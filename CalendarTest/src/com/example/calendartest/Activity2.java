package com.example.calendartest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends MainActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity2);
		final TextView tv = (TextView) findViewById(R.id.textView1);

		Date resultdate = new Date(MainActivity.date);
		String timestamp = new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(resultdate);
		tv.setText(timestamp);
	}


}
