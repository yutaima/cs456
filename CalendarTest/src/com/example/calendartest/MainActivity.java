package com.example.calendartest;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CalendarView;

public class MainActivity extends Activity {
	
	static long date = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final CalendarView cv = (CalendarView) findViewById(R.id.calendarView1);
		Button btn = (Button) findViewById(R.id.button);
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				date = cv.getDate();
        		Intent myIntent = new Intent(view.getContext(), Activity2.class);
        		startActivityForResult(myIntent, 0);  
			}
		});
	}


}
