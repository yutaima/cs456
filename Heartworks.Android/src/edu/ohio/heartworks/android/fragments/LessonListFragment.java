package edu.ohio.heartworks.android.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.activities.LessonDetailActivity;
import edu.ohio.heartworks.android.models.LessonModel;
import roboguice.fragment.RoboListFragment;

public class LessonListFragment extends RoboListFragment {

	@Inject private Db4oReadModelFactory readModelFactory;
	
	private List<LessonModel> lessons;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Db4oReadModel<LessonModel> readModel = readModelFactory.openSession(LessonModel.class);
		
		this.lessons = readModel.findAll();
		
		readModel.close();
		
		List<String> lessonNames = new ArrayList<String>();
		
		for (LessonModel lesson : lessons) {
			lessonNames.add(lesson.lessonName);
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, lessonNames);
		
		setListAdapter(adapter);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
	    Context context = getActivity();
		
		context.startActivity(LessonDetailActivity.createIntent(context, lessons.get(position).id));
	}
}
