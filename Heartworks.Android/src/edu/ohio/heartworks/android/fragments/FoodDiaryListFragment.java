package edu.ohio.heartworks.android.fragments;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import roboguice.fragment.RoboListFragment;
import android.app.AlertDialog;
import android.R.id;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.MainModule.UserData;
import edu.ohio.heartworks.android.R;
import edu.ohio.heartworks.android.activities.FoodDiaryDetailActivity;
import edu.ohio.heartworks.android.adapters.FoodDiaryModelAdapter;
import edu.ohio.heartworks.android.models.FoodDiaryModel;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.DeleteFoodDiary;
import edu.ohio.heartworks.infrastructure.Command;
import edu.ohio.heartworks.infrastructure.CommandSender;

public class FoodDiaryListFragment extends RoboListFragment {
	@Inject private Db4oReadModelFactory readModelFactory;
	@Inject private CommandSender commandBus;
	@Inject private @UserData File userDataRoot;
	
	private List<FoodDiaryModel> diaries;
	private List<Command> pendingCommands;
	private FoodDiaryModelAdapter adapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		pendingCommands = new ArrayList<Command>();
		return inflater.inflate(R.layout.fragment_fooddiary_list, container, false);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
		
		this.diaries = readModel.findAll();
				
		readModel.close();
		
		showHelp();
		
		adapter = new FoodDiaryModelAdapter(getActivity(), diaries, userDataRoot);
		
		setListAdapter(adapter);
		
		setHasOptionsMenu(true);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
	    Context context = getActivity();
        context.startActivity(FoodDiaryDetailActivity.createIntent(context,diaries.get(position).id));
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
	    super.onCreateOptionsMenu(menu, inflater);
	    inflater.inflate(R.menu.fragment_fooddiary_list, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_fooddiary_add:

	        Context context = getActivity();
	        context.startActivity(FoodDiaryDetailActivity.createIntent(context, null));
			return true;
			
		case R.id.menu_fooddiary_delete:
			showDeleteConfirmation();
			return true;
		}
		return false;
	}
	
	public void showHelp() {		
		Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
		int size = readModel.findAll().size();	
		readModel.close();
		
		TextView empty = (TextView) this.getActivity().findViewById(R.id.addDiaryText);
		if(size > 4) {
			empty.setVisibility(View.INVISIBLE);
		} else {
			empty.setVisibility(View.VISIBLE);
		}
	}
	
	private void showDeleteConfirmation() {
		        new AlertDialog.Builder(getActivity())
		                .setMessage("Are you sure you want to delete this entry?")
		                .setCancelable(false)
		                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		                    public void onClick(DialogInterface dialog, int id) {
		                        doDelete();
		                        //TODO:refresh the list
		                        
		                    }
		                }).setNegativeButton("No", null).show();
		    }
		    
			
		    private void doDelete() {
		    	for(FoodDiaryModel diary : diaries){
		    		
		    		if(diary.isChecked){
				        DeleteFoodDiary command = new DeleteFoodDiary();
				        command.aggregateId = diary.id;
				        command.expectedVersion = diary.version;
				        pendingCommands.add(command);
		    		}
			        
		    	}
		    	
		    	for (Command command : pendingCommands) {
					commandBus.send(command);
				}
		    	
		    	pendingCommands.clear();		    			
		    	Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);		    			
		    	this.diaries = readModel.findAll();		    			
		    	readModel.close();		    			
		    	adapter = new FoodDiaryModelAdapter(getActivity(), diaries, userDataRoot);		    			
		    	setListAdapter(adapter);		    			
		    	setHasOptionsMenu(true);
		    	
		    	showHelp();
		    }
}
