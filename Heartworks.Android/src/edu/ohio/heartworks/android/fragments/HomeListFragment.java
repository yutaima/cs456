package edu.ohio.heartworks.android.fragments;

import java.util.Arrays;

import roboguice.fragment.RoboListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.google.inject.Inject;

import de.greenrobot.event.EventBus;
import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.adapters.Adaptable;
import edu.ohio.heartworks.android.adapters.GenericArrayAdapter;
import edu.ohio.heartworks.android.adapters.NavigationHeaderAdaptable;
import edu.ohio.heartworks.android.adapters.NavigationItemAdaptable;
import edu.ohio.heartworks.android.models.FoodDiaryModel;

public class HomeListFragment extends RoboListFragment {
	private Object[] eventArray;
	
	@Inject private Db4oReadModelFactory readModelFactory;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
		
		int numFoodDiaries = readModel.findAll().size();
		
		readModel.close();
		
		Adaptable[] adaptableArray = new Adaptable[] {
				new NavigationHeaderAdaptable("My Wellness"),
				new NavigationItemAdaptable("Tasks", null),
				new NavigationItemAdaptable(String.format("Food Diary (%d)", numFoodDiaries), null),
				new NavigationItemAdaptable("Exercise Log", null),
				new NavigationHeaderAdaptable("Helpful Materials"),
				new NavigationItemAdaptable("Media", null),
				new NavigationItemAdaptable("Lessons", null)
		};
		
		eventArray = new Object[] {
				null,
				new TasksClicked(),
				new FoodDiariesClicked(),
				new ExerciseLogsClicked(),
				null,
				new MediaClicked(),
				new LessonsClicked()
		};
		
		setListAdapter(new GenericArrayAdapter(getActivity(), Arrays.asList(adaptableArray), 2));
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		if (eventArray[position] != null) {
			EventBus.getDefault().post(eventArray[position]);
			
			this.setSelection(position);
	        this.getListView().setItemChecked(position, true);
		}
	}

	public static class TasksClicked { }
	public static class FoodDiariesClicked { }
	public static class ExerciseLogsClicked { }
	public static class MediaClicked { }
	public static class LessonsClicked { }
}
