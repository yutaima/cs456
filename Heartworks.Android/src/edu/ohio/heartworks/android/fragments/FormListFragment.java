package edu.ohio.heartworks.android.fragments;

import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.activities.FormDetailActivity;
import edu.ohio.heartworks.android.adapters.FormModelAdapter;
import edu.ohio.heartworks.android.models.FormModel;
import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.R;
import roboguice.fragment.RoboListFragment;

public class FormListFragment extends RoboListFragment {
	@Inject private Db4oReadModelFactory readModelFactory;
	
	private List<FormModel> docs;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Db4oReadModel<FormModel> readModel = readModelFactory.openSession(FormModel.class);

		this.docs = readModel.findAll();
		
		readModel.close();
		
		FormModelAdapter adapter = new FormModelAdapter(getActivity(), docs);
		
		setListAdapter(adapter);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_form_list, container, false);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
	    Context context = getActivity();
	    
	    context.startActivity(FormDetailActivity.createIntent(context, docs.get(position).id));
	}
}
