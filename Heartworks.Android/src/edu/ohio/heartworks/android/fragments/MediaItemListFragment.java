package edu.ohio.heartworks.android.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.R;
import edu.ohio.heartworks.android.activities.MediaItemDetailActivity;
import edu.ohio.heartworks.android.models.MediaItemModel;

import roboguice.fragment.RoboListFragment;

public class MediaItemListFragment extends RoboListFragment {

	@Inject private Db4oReadModelFactory readModelFactory;
	private List<MediaItemModel> mediaItems;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Db4oReadModel<MediaItemModel> readModel = readModelFactory.openSession(MediaItemModel.class);

		this.mediaItems = readModel.findAll();
		
		readModel.close();
		
		List<String> itemNames = new ArrayList<String>();
		
		for (MediaItemModel mediaItem : mediaItems) {
			itemNames.add(mediaItem.fileName);
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, itemNames);
		
		setListAdapter(adapter);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
	    Context context = getActivity();
	    
	    context.startActivity(MediaItemDetailActivity.createIntent(context, mediaItems.get(position).id));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_form_list, container, false);
	}
}
