package edu.ohio.heartworks.android.services;

import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


//Why do we need deviceID?

// This DeviceID uniquely identifies a particular installation of the app. 
// It's not the device ID that actually uniquely identifies the physical device.
// Initially, the intention is to use this ID to identify which images were taken locally (versus images downloaded from the server).
// But now that mechanism is implemented differently.
// This code is currently not used.
//
// Images and text are uploaded separately because binaries don't go into JSON encoding directly.

public class DeviceId {
	private static UUID uniqueID = null;
	private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

	public synchronized static UUID get(Context context) {
	    if (uniqueID == null) {
	    	
	        SharedPreferences sharedPrefs = context.getSharedPreferences(PREF_UNIQUE_ID, Context.MODE_PRIVATE);
	        
	        String sUniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
	        
	        if (uniqueID != null) {
	        	uniqueID = UUID.fromString(sUniqueID);
	        } else {
	            uniqueID = UUID.randomUUID();
	            
	            Editor editor = sharedPrefs.edit();
	            editor.putString(PREF_UNIQUE_ID, uniqueID.toString());
	            editor.commit();
	        }
	    }
	    return uniqueID;
	}
}
