package edu.ohio.heartworks.android.services;

import java.util.List;
import java.util.Set;

import com.google.inject.Inject;

import de.greenrobot.event.EventBus;
import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.adapters.DataSetChanged;
import edu.ohio.heartworks.api.ServerAggregateDescriptor;
import edu.ohio.heartworks.infrastructure.EventHandler;
import edu.ohio.heartworks.infrastructure.QueueingCommandSender;
import edu.ohio.heartworks.infrastructure.RebuildableEventStore;

public class StateRebuilder {
	
	@Inject private Db4oReadModelFactory readModelFactory;
	@Inject private Set<EventHandler> eventHandlers;
	@Inject private RebuildableEventStore eventStore;
	@Inject private QueueingCommandSender commandQueue;
	
	public void clearAndRebuild() {
	    readModelFactory.openSession(ServerAggregateDescriptor.class).deleteAll().close();
	    rebuild();
	}
	
	public void rebuild() {
		
		commandQueue.popAll();
		eventStore.clear();
		
		for (EventHandler handler : eventHandlers) {
			handler.clear();
		}
		
		Db4oReadModel<ServerAggregateDescriptor> readModel = readModelFactory.openSession(ServerAggregateDescriptor.class);
		
		List<ServerAggregateDescriptor> descriptors = readModel.findAll();
		
		for (ServerAggregateDescriptor descriptor : descriptors) {
			descriptor.version = -1;

			readModel.store(descriptor);
		}
		
		readModel.close();
		
		EventBus.getDefault().post(new DataSetChanged());
	}
}
