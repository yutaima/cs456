package edu.ohio.heartworks.android.services;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import roboguice.service.RoboIntentService;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.SystemClock;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.R;
import edu.ohio.heartworks.android.MainModule.Server;
import edu.ohio.heartworks.android.MainModule.UserData;
import edu.ohio.heartworks.android.models.ResourceDownloadModel;
import edu.ohio.heartworks.android.models.ResourceUploadModel;
import edu.ohio.heartworks.api.AggregateEventList;
import edu.ohio.heartworks.api.ServerAggregateDescriptor;
import edu.ohio.heartworks.api.SyncRequest;
import edu.ohio.heartworks.infrastructure.Command;
import edu.ohio.heartworks.infrastructure.Event;
import edu.ohio.heartworks.infrastructure.EventStore;
import edu.ohio.heartworks.infrastructure.QueueingCommandSender;

public class SyncService extends RoboIntentService {
	
	// This is a "started" service. http://developer.android.com/guide/components/services.html
	// 
	// But one should not override this method "onStartCommand(Intent intent, int flags, int startId)" for IntentService.
	// Instead, override onHandleIntent(). http://developer.android.com/reference/android/app/IntentService.html
	
	// SyncService is invoked only from LifetimeService

    private static final long loopInterval = R.integer.sync_loop_interval * 1000;  
    	// how frequently SyncService should run again. Need to make it modifiable within the app.
    private static final String apiServiceEndpoint = "api";
    private static final String fileServiceEndpoint = "file";

    @Inject AlarmManager alarmManager;
    @Inject QueueingCommandSender commandQueue;
    @Inject EventStore eventStore;
    @Inject ObjectMapper objectMapper;
    @Inject ConnectivityManager connectivityManager;
    @Inject Db4oReadModelFactory readModelFactory;
    @Inject StateRebuilder stateRebuilder;
    @Inject @Server String serverUrl;
    @Inject @UserData File userDataRoot;

    public SyncService() {
        super(SyncService.class.getName());
    }

    private boolean isConnected() {

        // From Android documentation: "This may return null when no networks are available."
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onHandleIntent(Intent intent) {  

        /*
         * TODO handle these exceptions appropriately
         * 
         * JSON serialization / deserialization errors could be
         * candidates for runtime exceptions, because there isn't a sane
         * action to take if these occur.
         * 
         * HttpClient related exception data should probably be relayed
         * to the user. Commands should be re-queued.
         * 
         * SyncExceptions _should_ happen extremely rarely. If one does
         * occur, we should drop all of our data & rebuild from server
         * events. In this case, we may want to save the commands from
         * the sync, and attempt to update them & re-queue them.
         */

        if (isConnected()) {

            try {
                doUpload();
                doDownload();
            } catch (Exception e) {
                e.printStackTrace();
            }

            List<Command> commands = commandQueue.popAll();

            try {
                doSync(commands);
            } catch (JsonGenerationException e) {
                throw new RuntimeException(e);
            } catch (JsonMappingException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {

                // This just means that the server cannot be reached, so requeue our commands for later

                if (commands.size() > 0) {
                    commandQueue.pushAll(commands);
                }
            } catch (Exception e) {
                // This could be a SyncException or a ConcurrencyException, though in either case, we want to rebuild the client DB
                
            	// The next line was commented out to temporarily resolve a bug, with which all entries are gone in an hour or so, 
            	// presumably because of a changed server format causing client exception, or another bug of two different objects 
            	// with the same IDs. Either way, the client app throws an exception that comes here. The rebuild() would clean out
            	// all records, but if the rebuilding from server fails, no records are left.
            	//
                // stateRebuilder.rebuild();

                // Here, we would want to rebuild the event store & readmodels, and "merge" our commands back in the queue
            }
        }

        PendingIntent serviceIntent = PendingIntent.getService(this, 0, new Intent(this, this.getClass()), PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + loopInterval, serviceIntent);
    }

    private void doDownload() throws ClientProtocolException, IOException {
        Db4oReadModel<ResourceDownloadModel> resourceDownloadReadModel = readModelFactory.openSession(ResourceDownloadModel.class);

        List<ResourceDownloadModel> resDownloads = resourceDownloadReadModel.findAll();

        for (ResourceDownloadModel resDownload : resDownloads) {

            File directory = new File(userDataRoot, resDownload.aggregateId.toString());

            if (!directory.exists()) {
                directory.mkdirs();
            }

            File file = new File(directory, resDownload.fileName);

            if (file.exists()) {
                resourceDownloadReadModel.delete(resDownload);

                continue;
            }

            HttpClient client = new DefaultHttpClient();

            HttpGet get = new HttpGet(serverUrl + "fileServiceEndpoint" + "/" + resDownload.aggregateId.toString() + "/" + resDownload.fileName);

            HttpResponse response = client.execute(get);

            int statusCode = response.getStatusLine().getStatusCode();

            HttpEntity entity = response.getEntity();

            if (entity != null) { 

                InputStream inStream = entity.getContent();
                OutputStream outStream = new BufferedOutputStream(new FileOutputStream(file));

                int bufferSize = 1024;
                byte[] buffer = new byte[bufferSize];

                int len = 0;
                while ((len = inStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, len);
                }

                if (inStream != null) inStream.close();
                if (outStream != null) outStream.close();
            }

            if (statusCode >= 200 && statusCode <= 300) {
                resourceDownloadReadModel.delete(resDownload);
            }
        }

        resourceDownloadReadModel.close();
    }

    private void doUpload() throws ClientProtocolException, IOException {
        Db4oReadModel<ResourceUploadModel> resourceUploadReadModel = readModelFactory.openSession(ResourceUploadModel.class);

        List<ResourceUploadModel> resUploads = resourceUploadReadModel.findAll();

        for (ResourceUploadModel resUpload : resUploads) {

            File fileDir = new File(userDataRoot, resUpload.aggregateId.toString());
            File file = new File(fileDir, resUpload.fileName);

            // We may still be waiting for the user to take the photo
            if (!file.exists()) {
                continue;
            }

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl + fileServiceEndpoint + "/" + resUpload.aggregateId.toString());

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            entity.addPart("file", new FileBody(file));

            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode >= 200 && statusCode <= 300) {
                resourceUploadReadModel.delete(resUpload);
            }
        }

        resourceUploadReadModel.close();
    }

    private void doSync(List<Command> commands) throws JsonGenerationException, JsonMappingException, IOException, SyncException {

        Db4oReadModel<ServerAggregateDescriptor> serverAggregateReadModel = readModelFactory.openSession(ServerAggregateDescriptor.class);

        // Iterate through commands, find all that have expected version of -1
        for (Command command : commands) {
            if (command.expectedVersion == -1) {

                // We know that these aggregates _should_ exist on the server after the update
                serverAggregateReadModel.store(new ServerAggregateDescriptor(command.aggregateId, -1));
            }
        }

        List<ServerAggregateDescriptor> descriptors = serverAggregateReadModel.findAll();
        serverAggregateReadModel.close();

        // Put the commands & descriptors into a SyncRequest object and serialize it to JSON
        SyncRequest syncRequest = new SyncRequest(UUID.randomUUID(), descriptors, commands);
        String json = objectMapper.writeValueAsString(syncRequest);

        // Assemble our POST request
        HttpClient httpClient = new DefaultHttpClient();

        HttpPost post = new HttpPost(serverUrl + apiServiceEndpoint);
        post.setEntity(new StringEntity(json, HTTP.UTF_8));
        post.setHeader("Accept", "application/json");
        post.setHeader("Content-type", "application/json");

        // Execute the request, obtain the response as a string
        HttpResponse response = httpClient.execute(post);

        InputStream inputStream = response.getEntity().getContent();

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
        StringBuilder sb = new StringBuilder();

        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }

        String result = sb.toString();

        // Deserialize the JSON response
        if (result != null && !result.isEmpty()) {
            JsonNode root = objectMapper.readTree(result);

            // Only attempt to deserialize the response if it is not an empty JSON object
            if (root.size() > 0) {
                List<AggregateEventList> eventLists = objectMapper.readValue(root, new TypeReference<List<AggregateEventList>>(){});

                // Process the events that the server has returned
                processEvents(eventLists);
            }
        }
    }

    private void processEvents(List<AggregateEventList> eventLists) throws SyncException {

        Db4oReadModel<ServerAggregateDescriptor> serverAggregateReadModel = readModelFactory.openSession(ServerAggregateDescriptor.class);

        try {
            // For every aggregate Id / event list pair in the response
            for (AggregateEventList eventList : eventLists) {
                ServerAggregateDescriptor descriptor = serverAggregateReadModel.findById(eventList.id);
    
                // Get our sync descriptor to determine the last version of this aggregate that was synchronized with the server
                if (descriptor == null) {
                    // If we have no descriptor, then we don't know about this aggregate, so the version is -1
                    descriptor = new ServerAggregateDescriptor(eventList.id, -1);
                }
    
                // Get the events for this aggregate since the last synchronization
                List<Event> clientEvents = eventStore.getEventsForAggregateSince(eventList.id, descriptor.version);
                ListIterator<Event> clientEventIterator = clientEvents.listIterator();
    
                ListIterator<Event> serverEventIterator = eventList.events.listIterator();
    
                // Ensure that the server's events since the last version match our events since the last version
                while (clientEventIterator.hasNext() && serverEventIterator.hasNext()) {
    
                    Event clientEvent = clientEventIterator.next(); 
                    Event serverEvent = serverEventIterator.next();
    
                    if (!clientEvent.equals(serverEvent)) {
                        throw new SyncException();
                    }
    
                    // Increment our sync descriptor version
                    ++descriptor.version;
                }
    
                // If we have additional events, assume that they correspond to queued commands
    
                // If there are any additional events from the server
                if (serverEventIterator.hasNext()) {
    
                    // Then we don't know about them, so save/publish these events
                    List<Event> uncommittedServerEvents = eventList.events.subList(serverEventIterator.nextIndex(), eventList.events.size());
    
                    eventStore.save(eventList.id, uncommittedServerEvents, descriptor.version);
    
                    // Update our sync descriptor version
                    descriptor.version = uncommittedServerEvents.get(uncommittedServerEvents.size() - 1).version;
                }
    
                // Save the updated sync descriptor
                serverAggregateReadModel.store(descriptor);
            }
        } finally {
            serverAggregateReadModel.close();
        }
    }
}