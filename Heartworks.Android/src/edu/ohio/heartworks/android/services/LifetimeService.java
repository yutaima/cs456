package edu.ohio.heartworks.android.services;

import com.db4o.ObjectContainer;
import com.google.inject.Inject;

import edu.ohio.heartworks.android.MainModule.ForEventStore;
import edu.ohio.heartworks.android.MainModule.ForReadModel;

import android.content.Intent;
import android.os.IBinder;
import roboguice.service.RoboService;

public class LifetimeService extends RoboService {
	
	@Inject @ForEventStore ObjectContainer eventStoreContainer; 
	@Inject @ForReadModel ObjectContainer readModelContainer;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
        startService(new Intent(this, SyncService.class));
	}
	
	@Override
	public IBinder onBind(Intent intent) { // A bound service. http://developer.android.com/guide/components/services.html
		return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		eventStoreContainer.close();
		readModelContainer.close(); // What if the sync service is running at this time?
	}
}
