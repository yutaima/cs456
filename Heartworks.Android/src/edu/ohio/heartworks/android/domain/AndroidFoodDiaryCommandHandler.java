package edu.ohio.heartworks.android.domain;

import java.io.File;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.MainModule.UserData;
import edu.ohio.heartworks.android.models.ResourceUploadModel;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommandHandler;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.AddFoodPicture;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.ChangeDate;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.ChangeDescription;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.ChangeFoodType;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.CreateFoodDiary;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.DeleteFoodDiary;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.RemoveFoodPicture;
import edu.ohio.heartworks.infrastructure.Handler;
import edu.ohio.heartworks.infrastructure.RepositoryFactory;

public class AndroidFoodDiaryCommandHandler extends FoodDiaryCommandHandler {

	private Db4oReadModelFactory readModelFactory;
	private File userDataRoot;
	
	@Inject
	public AndroidFoodDiaryCommandHandler(RepositoryFactory factory, Db4oReadModelFactory readModelFactory, @UserData File userDataRoot) {
		super(factory);
		
		this.readModelFactory = readModelFactory;
		this.userDataRoot = userDataRoot;
	}

	@Override @Handler
	public void handle(CreateFoodDiary command) {
		super.handle(command);
	}

	@Override @Handler
	public void handle(ChangeDescription command) {
		super.handle(command);
	}

	@Override @Handler
	public void handle(ChangeFoodType command) {
		super.handle(command);
	}
	
	@Override @Handler
	public void handle(DeleteFoodDiary command) {
	    super.handle(command);
	}
	
	@Override @Handler
	public void handle(RemoveFoodPicture command) {
		super.handle(command);
	}

	@Override @Handler
	public void handle(AddFoodPicture command) {
		super.handle(command);
		
		File directory = new File(userDataRoot, command.aggregateId.toString());		
		File imageFile = new File(directory, command.fileName);
		
		if (imageFile.exists()) {
			readModelFactory.openSession(ResourceUploadModel.class).store(new ResourceUploadModel(command.aggregateId, command.fileName)).close();
		}
	}
	
	@Override @Handler
	public void handle(ChangeDate command) {
		super.handle(command);
	}
}