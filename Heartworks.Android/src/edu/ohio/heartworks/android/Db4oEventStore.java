package edu.ohio.heartworks.android;

import java.util.List;
import java.util.UUID;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

import edu.ohio.heartworks.infrastructure.AggregateDescriptor;
import edu.ohio.heartworks.infrastructure.ConcurrencyException;
import edu.ohio.heartworks.infrastructure.Event;
import edu.ohio.heartworks.infrastructure.EventPublisher;
import edu.ohio.heartworks.infrastructure.EventStore;
import edu.ohio.heartworks.infrastructure.RebuildableEventStore;

// EventStore is our database. It's shared by both the web server and the Android client, 
// although different database systems were used (mongoDB vs db49) on the two sides.
//
// https://bitbucket.org/liuc/cs456/wiki/doc/Dependency%20Injection

public class Db4oEventStore implements EventStore, RebuildableEventStore {
    private final ObjectContainer container;
    private final EventPublisher publisher;

    public Db4oEventStore(EventPublisher publisher, ObjectContainer container)
    {
        this.publisher = publisher;
        this.container = container;
    }

    public int getVersionForAggregate(final UUID aggregateId) {
        // Grab the db4oAggregate object for this aggregate, if it exists
        int aggregateVersion = -1;
        
        ObjectSet<AggregateDescriptor> aggregates = container.queryByExample(new AggregateDescriptor(aggregateId, null));
        
        // Assume db4o is configured to use unique indexes
        if (!aggregates.isEmpty()) {
            aggregateVersion = aggregates.get(0).version;
        }
        
        return aggregateVersion;
    }

    public List<Event> getEventsForAggregate(final UUID aggregateId) {
        return getEventsForAggregateSince(aggregateId, -1);
    }
    
    public List<Event> getEventsForAggregateSince(final UUID aggregateId, final int sinceVersion) {
        
        @SuppressWarnings("serial")
        List<Event> events = container.query(new Predicate<Event>() {
            @Override
            public boolean match(Event event) {
                
                // TODO Ensure index is on both version & aggregateId
                return event.version > sinceVersion && event.aggregateId.equals(aggregateId);
            }
        });

        if (events == null) {
            throw new RuntimeException("AggregateNotFoundException");
        }

        return events;
    }

    public void save(final UUID aggregateId, List<Event> events, final int expectedVersion) throws ConcurrencyException {
        
        // Grab the db4oAggregate object for this aggregate, if it exists
        AggregateDescriptor db4oAggregate = null;
        
        List<AggregateDescriptor> aggregates = container.queryByExample(new AggregateDescriptor(aggregateId, null));
        
        // Assume db4o is configured to use unique indexes
        if (aggregates.isEmpty()) {
            db4oAggregate = new AggregateDescriptor(aggregateId, -1);
        // Else, there is a single matching db4oAggregate, so use it
        } else {
            db4oAggregate = aggregates.get(0);
        }
        
        // If aggregate versions don't match up, there is a concurrency problem
        if (db4oAggregate.version != expectedVersion) {
            
            // This could be resolved here, if conflict resolution logic is defined
            throw new ConcurrencyException();
        }
        
        // Store events in the database
        int version = expectedVersion;
        
        for (Event event : events) {
            event.version = ++version;
            
            // TODO: May need to ensure that the correct update depth is being used here
            container.store(event);
            publisher.send(event);
        }
        
        db4oAggregate.version = version;
        
        // Note: db4o works with object references, so in the case that this db4oAggregate
            // already existed, this call will in fact update the object (rather than storing a new one)
        container.store(db4oAggregate);
        container.commit();
    }

	@Override
	public void republishAll() {
		ObjectSet<Event> events = container.query(Event.class);
		
		for (Event event : events) {
			publisher.send(event);
		}
	}

	@Override
	public void clear() {
		ObjectSet<Event> events = container.query(Event.class);
		
		for (Event event : events) {
			container.delete(event);
		}
		
		ObjectSet<AggregateDescriptor> descriptors = container.query(AggregateDescriptor.class);
		
		for (AggregateDescriptor descriptor : descriptors) {
			container.delete(descriptor);
		}
		
		container.commit();
	}
}