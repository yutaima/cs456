package edu.ohio.heartworks.android;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

import edu.ohio.heartworks.infrastructure.Command;
import edu.ohio.heartworks.infrastructure.QueueingCommandSender;
import edu.ohio.heartworks.infrastructure.ConcurrencyException;
import edu.ohio.heartworks.infrastructure.InProcessBus;
import edu.ohio.heartworks.infrastructure.Message;

public class Db4oPersistantCommandBus extends InProcessBus implements QueueingCommandSender {

    private ObjectContainer container;
    
    public Db4oPersistantCommandBus(ObjectContainer container) {
        this.container = container;
    }

    @Override
    public <T extends Message> void send(T command) throws ConcurrencyException {
        super.send(command);

        // If there were no exceptions, store the command
        container.store(command);
        container.commit();
    }
    
	@Override @SuppressWarnings("serial")
    public List<Command> popAll() {
    	ObjectSet<Command> commands = container.query(new Predicate<Command>() {
			@Override
			public boolean match(Command command) {
				return true;
			}
    	}, new Comparator<Command>() {
			@Override
			public int compare(Command lhs, Command rhs) {
				return Integer.valueOf(lhs.expectedVersion).compareTo(Integer.valueOf(rhs.expectedVersion));
			}
    	});
    	
        List<Command> commandList = new ArrayList<Command>(commands);
        
        for (Command command : commands) {
        	container.delete(command);
        }
        
        container.commit();
        
        return commandList;
    }

	@Override
	public void pushAll(List<Command> commands) {
		for (Command command : commands) {
			container.store(command);
		}
		
		container.commit();
	}

	@Override
	public int size() {
    	ObjectSet<Command> commands = container.query(Command.class);
    	return commands.size();
	}
}