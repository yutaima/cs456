package edu.ohio.heartworks.android.eventhandlers;

import com.google.inject.Inject;

import edu.ohio.heartworks.domain.form.FormEvents.*;
import edu.ohio.heartworks.infrastructure.EventHandler;
import edu.ohio.heartworks.infrastructure.Handler;
import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.models.*;

public class FormEventHandler implements EventHandler {

	@Inject private Db4oReadModelFactory readModelFactory;
	
	@Handler
	public void handle(final FormCreated event) {
		FormModel doc = new FormModel();
		doc.id = event.aggregateId;
		doc.name = event.name;
		doc.formItems = event.items;
		
		readModelFactory.openSession(FormModel.class)
			.store(doc)
			.close();
	}
	
	@Handler
	public void handle(final FormAssigned event) {
		Db4oReadModel<FormModel> readModel = readModelFactory.openSession(FormModel.class);
		
		FormModel doc = readModel.findById(event.aggregateId);
		doc.cronExpression = event.cronExpression;
		
		readModel.store(doc).close();
	}

	@Override
	public void clear() {
		readModelFactory.openSession(FormModel.class)
			.deleteAll()
			.close();
	}
}