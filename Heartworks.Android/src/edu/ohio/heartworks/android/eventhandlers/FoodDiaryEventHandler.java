package edu.ohio.heartworks.android.eventhandlers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.MainModule.UserData;
import edu.ohio.heartworks.android.models.FoodDiaryModel;
import edu.ohio.heartworks.android.models.ResourceDownloadModel;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.DateChanged;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.DescriptionChanged;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodDiaryCreated;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodDiaryDeleted;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodPictureAdded;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodPictureRemoved;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodTypeChanged;
import edu.ohio.heartworks.infrastructure.EventHandler;
import edu.ohio.heartworks.infrastructure.Handler;

public class FoodDiaryEventHandler implements EventHandler {
	
	@Inject private Db4oReadModelFactory readModelFactory;
	@Inject private @UserData File userDataRoot;
	
	@Handler
	public void handle(final FoodDiaryCreated event) {
		
		FoodDiaryModel diary = new FoodDiaryModel();
		diary.id = event.aggregateId;
		diary.dateCreated = event.dateCreated;
		
		readModelFactory.openSession(FoodDiaryModel.class)
			.store(diary)
			.close();
	}
	
	@Handler
	public void handle(DescriptionChanged event) {
		Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
		
		FoodDiaryModel diary = readModel.findById(event.aggregateId);
		
		diary.description = event.description;
		diary.version++;

		readModel.store(diary).close();
	}
	
	@Handler
	public void handle(FoodPictureAdded event) {
		Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
		
		FoodDiaryModel diary = readModel.findById(event.aggregateId);
		
		if (diary.imageFiles == null) {
			diary.imageFiles = new ArrayList<String>();
		}
		
		diary.imageFiles.add(event.fileName);
		diary.version++;
		
		readModel.store(diary).close();
		
		File directory = new File(userDataRoot, event.aggregateId.toString());
		File imageFile = new File(directory, event.fileName);
		
		if (!imageFile.exists()) {

			readModelFactory.openSession(ResourceDownloadModel.class)
				.store(new ResourceDownloadModel(event.aggregateId, event.fileName))
				.close();
		}
	}
	
	@Handler
	public void handle(FoodTypeChanged event) {
		Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
		
		FoodDiaryModel diary = readModel.findById(event.aggregateId);
		
		diary.foodType = event.foodType;
		diary.version++;
		
		readModel.store(diary).close();
	}
	
	@Handler
	public void handle(FoodPictureRemoved event) {
		Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
		
		FoodDiaryModel diary = readModel.findById(event.aggregateId);
		
		diary.imageFiles.remove(event.fileName);
		diary.version++;

		File imageFile = new File(userDataRoot, event.fileName);
		
		if (imageFile.exists()) {
			imageFile.delete();
		}
		
		readModel.store(diary).close();
	}
	
	@Handler
	public void handle(FoodDiaryDeleted event) {
		Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
		
		FoodDiaryModel diary = readModel.findById(event.aggregateId);
		
		if (diary.imageFiles != null) {
	        for (String imageFileName : diary.imageFiles) {
	            File imageFile = new File(new File(userDataRoot, event.aggregateId.toString()), imageFileName);
	            
	            if (imageFile.exists()) {
	                imageFile.delete();
	            }
	        }
		}
		
		readModel.delete(diary).close();
	}
	
	@Handler
	public void handle(DateChanged event) {
		Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
		
		FoodDiaryModel diary = readModel.findById(event.aggregateId);
		
		diary.dateCreated = event.newDate;
		diary.version++;
		
		readModel.store(diary).close();
	}

	@Override
	public void clear() {
		Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
		
		List<FoodDiaryModel> diaries = readModel.findAll();
		
		for (FoodDiaryModel diary : diaries) {
			if (diary.imageFiles != null) {                        
				for (String imageFileName : diary.imageFiles) {				    
					File imageFile = new File(new File(userDataRoot, diary.id.toString()), imageFileName);
					
					imageFile.delete();
				}
			}
			
			readModel.delete(diary);
		}
		
		readModel.close();
		
		readModelFactory.openSession(ResourceDownloadModel.class)
			.deleteAll()
			.close();
	}
}
