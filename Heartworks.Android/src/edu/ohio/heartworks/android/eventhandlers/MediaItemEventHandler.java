package edu.ohio.heartworks.android.eventhandlers;

import java.io.File;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.MainModule.UserData;
import edu.ohio.heartworks.android.models.MediaItemModel;
import edu.ohio.heartworks.android.models.ResourceDownloadModel;
import edu.ohio.heartworks.domain.media.MediaItemEvents.MediaItemCreated;
import edu.ohio.heartworks.infrastructure.EventHandler;
import edu.ohio.heartworks.infrastructure.Handler;

public class MediaItemEventHandler implements EventHandler {
	
	@Inject private Db4oReadModelFactory readModelFactory;
	@Inject private @UserData File userDataRoot;
	
	@Handler
	public void handle(MediaItemCreated event) {
		MediaItemModel mediaItem = new MediaItemModel();
		mediaItem.id = event.aggregateId;
		mediaItem.name = event.name;
		mediaItem.fileName = event.fileName;
		mediaItem.mediaType = event.mediaType;
		
		readModelFactory.openSession(MediaItemModel.class)
			.store(mediaItem)
			.close();
		
		File directory = new File(userDataRoot, event.aggregateId.toString());
		File mediaFile = new File(directory, event.fileName);
		
		if (!mediaFile.exists()) {
			readModelFactory.openSession(ResourceDownloadModel.class)
				.store(new ResourceDownloadModel(event.aggregateId, event.fileName))
				.close();
		}
	}

	@Override
	public void clear() {
		readModelFactory.openSession(MediaItemModel.class)
			.deleteAll()
			.close();
	}
}