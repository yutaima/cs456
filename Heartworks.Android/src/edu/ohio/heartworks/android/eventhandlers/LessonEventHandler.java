package edu.ohio.heartworks.android.eventhandlers;

import java.io.File;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.MainModule.UserData;
import edu.ohio.heartworks.android.models.LessonModel;
import edu.ohio.heartworks.android.models.ResourceDownloadModel;
import edu.ohio.heartworks.domain.lesson.LessonEvents.LessonCreated;
import edu.ohio.heartworks.infrastructure.EventHandler;
import edu.ohio.heartworks.infrastructure.Handler;

public class LessonEventHandler implements EventHandler {

	@Inject private Db4oReadModelFactory readModelFactory;
	@Inject private @UserData File userDataRoot;
	
	@Handler
	public void handle(LessonCreated event) {
		LessonModel model = new LessonModel();
		model.id = event.aggregateId;
		model.lessonName = event.name;
		model.htmlContent = event.htmlContent;
		model.files = event.files;
		
		readModelFactory.openSession(LessonModel.class).store(model).close();
		
		File directory = new File(userDataRoot, event.aggregateId.toString());
		
		if (event.files != null && event.files.size() > 0) {
			Db4oReadModel<ResourceDownloadModel> downloadReadModel = readModelFactory.openSession(ResourceDownloadModel.class);
			
			for (String fileName : event.files) {
				File file = new File(directory, fileName);
				
				if (!file.exists()) {
					downloadReadModel.store(new ResourceDownloadModel(event.aggregateId, fileName));
				}
			}
			
			downloadReadModel.close();
		}
	}

	@Override
	public void clear() {
		readModelFactory.openSession(LessonModel.class).deleteAll().close();
	}
}
