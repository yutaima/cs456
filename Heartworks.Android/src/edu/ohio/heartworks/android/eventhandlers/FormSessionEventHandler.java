package edu.ohio.heartworks.android.eventhandlers;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.models.FormSessionModel;
import edu.ohio.heartworks.domain.formsession.FormSessionEvents.*;
import edu.ohio.heartworks.infrastructure.EventHandler;
import edu.ohio.heartworks.infrastructure.Handler;

public class FormSessionEventHandler implements EventHandler {
	
	@Inject private Db4oReadModelFactory readModelFactory;
	
	@Handler
	public void handle(FormSessionStarted event) {
		FormSessionModel docSession = new FormSessionModel();
		
		docSession.id = event.aggregateId;
		docSession.formId = event.formId;
		docSession.sessionAnswers = event.answers;
		docSession.dateStarted = event.dateStarted;
		
		readModelFactory.openSession(FormSessionModel.class)
			.store(docSession)
			.close();
	}
	
	@Handler
	public void handle(FormSessionAnswerSubmitted event) {
		
		Db4oReadModel<FormSessionModel> readModel = readModelFactory.openSession(FormSessionModel.class);

		FormSessionModel docSession = readModel.findById(event.aggregateId);
			
		for (int i = 0; i < docSession.sessionAnswers.size(); i++) {
			if (docSession.sessionAnswers.get(i).questionId.equals(event.answer.questionId)) {
				docSession.sessionAnswers.set(i, event.answer);
				
				break;
			}
		}
		
		docSession.version++;
		
		readModel.store(docSession).close();
	}

	@Override
	public void clear() {
		readModelFactory.openSession(FormSessionModel.class)
			.deleteAll()
			.close();
	}
}
