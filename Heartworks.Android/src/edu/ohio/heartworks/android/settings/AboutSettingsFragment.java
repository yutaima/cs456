package edu.ohio.heartworks.android.settings;

import roboguice.RoboGuice;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.R;
import edu.ohio.heartworks.android.models.FoodDiaryModel;
import edu.ohio.heartworks.infrastructure.QueueingCommandSender;

public class AboutSettingsFragment extends PreferenceFragment {

    @Inject QueueingCommandSender commandQueue;
    @Inject Db4oReadModelFactory readModelFactory;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        RoboGuice.getInjector(getActivity()).injectMembersWithoutViews(this); 
        
        addPreferencesFromResource(R.xml.settings_about);

        Preference buildNumberPreference = (Preference) findPreference("build_number");

        Integer versionCode = null;
        String versionName = "";
        
        try {
            versionCode = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode;
            versionName = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            versionCode = 0;
        }
        
        buildNumberPreference.setSummary(versionName+": (Build "+versionCode.toString()+")");

        Preference serverUrlPreference = (Preference) findPreference("server_url");
        serverUrlPreference.setSummary(getString(R.string.default_server_url));

        
        Preference creditsPreference = (Preference) findPreference("credits");

        creditsPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                preference.setSummary(getString(R.string.credits_summary_with_names));
                
                return true;
            }
        });

        Preference syncStatusPreference = (Preference) findPreference("sync_status");

        syncStatusPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
            	
            	Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
            	
            	int numEntries = readModel.findAll().size();
            	
            	readModel.close();
            	
                preference.setSummary("There are " + Integer.valueOf(commandQueue.size()).toString() + " transactions not yet sync'ed.\n"  
                		+ "There are a total of "+ numEntries + " entries in the local Db4o food diary database.");
                return true;
            }
        });

    }
}