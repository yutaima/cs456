package edu.ohio.heartworks.android.settings;

import com.google.inject.Inject;

import roboguice.RoboGuice;
import edu.ohio.heartworks.android.R;
import edu.ohio.heartworks.android.services.StateRebuilder;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;

public class DeveloperSettingsFragment extends PreferenceFragment {
    
    @Inject private StateRebuilder stateRebuilder;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        RoboGuice.getInjector(getActivity()).injectMembersWithoutViews(this);
        
        addPreferencesFromResource(R.xml.settings_developer);
        
        Preference rebuildPreference = (Preference) findPreference("rebuild");
        
        rebuildPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                stateRebuilder.rebuild();
                
                // TODO: update preference summary with result?
                return true;
            }
        });
        
        Preference clearPreference = (Preference) findPreference("clear");
        
        clearPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                stateRebuilder.clearAndRebuild();
                
                // TODO: update preference summary with result?
                return true;
            }
        });

        Preference syncLoopIntervalPreference = (Preference) findPreference("sync_loop_interval");
        syncLoopIntervalPreference.setSummary(getString(R.integer.sync_loop_interval));
        
        syncLoopIntervalPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
               // TODO: consider changing this to a few check boxes
               // 
               preference.setSummary(getString(R.integer.sync_loop_interval));
               return true;
            }
        });

        Preference unittest1Preference = (Preference) findPreference("unittest1");
        
        unittest1Preference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
            	// invoke the test here
                preference.setSummary(getString(R.string.unittest1_done));
                
                return true;
            }
        });

        Preference unittest2Preference = (Preference) findPreference("unittest2");
        
        unittest2Preference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
            	// invoke the test here
                preference.setSummary(getString(R.string.unittest2_done));
                
                return true;
            }
        });

    }
}
