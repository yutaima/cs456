package edu.ohio.heartworks.android.settings;

// The setting activity and fragments are organized in a separate folder 
// because they are specified in XML files and are implemented differently from other activities and fragments.

import java.util.List;

import edu.ohio.heartworks.android.R;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;
import roboguice.activity.RoboPreferenceActivity;

public class SettingsActivity extends RoboPreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        

        ActionBar actionBar = getActionBar();
        
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.settings_headers, target);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home: // android.R.id.home is explained in http://developer.android.com/guide/topics/ui/actionbar.html
            finish();
            break;
        }
        
        return super.onOptionsItemSelected(item);
    }

}
