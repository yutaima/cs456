package edu.ohio.heartworks.android.adapters;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import edu.ohio.heartworks.android.R;
import edu.ohio.heartworks.android.models.FoodDiaryModel;

public class FoodDiaryModelAdapter extends ArrayAdapter<FoodDiaryModel> {
	private final LayoutInflater inflater;
	private final List<FoodDiaryModel> values;
	private File userDataRoot;
	
	private static class ViewHolder {
		public TextView description;
		public TextView date;
		public LinearLayout thumbnails;
		public CheckBox deleteBox;
	}
	
	public FoodDiaryModelAdapter(Context context, List<FoodDiaryModel> values, File userDataRoot) {
		// textViewResourceId is not necessary with getView being overridden
		super(context, 0, values);
		
		this.inflater = LayoutInflater.from(context);
		this.values = values;
		this.userDataRoot = userDataRoot;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.fooddiary_listitem, parent, false);
			
			holder = new ViewHolder();
			holder.description = (TextView) convertView.findViewById(R.id.fooddiary_listitem_description);
			holder.date = (TextView) convertView.findViewById(R.id.fooddiary_listitem_date);
			holder.thumbnails = (LinearLayout) convertView.findViewById(R.id.fooddiary_listitem_thumbnails);
			holder.deleteBox = (CheckBox) convertView.findViewById(R.id.fooddiary_listitem_checkbox);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		final FoodDiaryModel diary = values.get(position);
		
		//This adds info to the list display
		holder.description.setText(diary.description);
		
		String dateText = new SimpleDateFormat("MM/dd hh:mm a", Locale.US).format(diary.dateCreated);
		
		// It is currently possible for the diary food type to be null (maybe this should be changed)
		if (diary.foodType != null) {
			dateText += " " + diary.foodType.toString();
		}
		
		holder.date.setText(dateText);
		
		holder.deleteBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(isChecked) diary.isChecked = true;
			else diary.isChecked = false;
			}
		});
		
		if (diary.imageFiles != null && !diary.imageFiles.isEmpty()) {
			holder.thumbnails.removeAllViews();
			for(String fileName : diary.imageFiles){
											
				File directory = new File(userDataRoot, diary.id.toString());
				File imageFile = new File(directory, fileName);
			
				if (imageFile.exists()) {
	
					// Get the dimensions of the View
					int targetW = 100; //holder.image.getWidth();
					int targetH = 100; //holder.image.getHeight();
		    
					//Get the dimensions of the bitmap
					BitmapFactory.Options bmOptions = new BitmapFactory.Options();
					bmOptions.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);
					int photoW = bmOptions.outWidth;
					int photoH = bmOptions.outHeight;
		    
					// Determine how much to scale down the image
					int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
		    
					// Decode the image file into a Bitmap sized to fill the View
					bmOptions.inJustDecodeBounds = false;
					bmOptions.inSampleSize = scaleFactor;
					bmOptions.inPurgeable = true;
			
					Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);
		    
					ImageView diaryPic = new ImageView(getContext());
					diaryPic.setImageBitmap(bitmap);
					
					holder.thumbnails.addView(diaryPic);
				}
			}
		}	
		
		return convertView;
	}
}