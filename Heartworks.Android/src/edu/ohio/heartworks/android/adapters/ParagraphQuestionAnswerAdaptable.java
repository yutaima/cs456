package edu.ohio.heartworks.android.adapters;

import de.greenrobot.event.EventBus;
import edu.ohio.heartworks.domain.formsession.ParagraphQuestionAnswer;
import edu.ohio.heartworks.android.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class ParagraphQuestionAnswerAdaptable implements Adaptable {

	private final ParagraphQuestionAnswer answer;
	
	public static class ParagraphQuestionAnswered {
		public ParagraphQuestionAnswer answer;
	}
	
	// See http://www.youtube.com/watch?v=N6YdwzAvwOA for details on this
	private static class ViewHolder {
		private TextView questionPrompt;
		public EditText questionAnswer;
	}

	public ParagraphQuestionAnswerAdaptable(ParagraphQuestionAnswer answer) {
		this.answer = answer;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent, LayoutInflater inflater) {
		final ViewHolder holder;
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.formitem_paragraphquestion, parent, false);

			holder = new ViewHolder();
			holder.questionPrompt = (TextView) convertView.findViewById(R.id.paragraphquestion_prompt);
			holder.questionAnswer = (EditText) convertView.findViewById(R.id.paragraphquestion_answer);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.questionPrompt.setText(answer.question.getQuestionText());
		holder.questionAnswer.setText(answer.answerText);
		
		holder.questionAnswer.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String savedAnswerText = answer.answerText;
					String newAnswerText = holder.questionAnswer.getText().toString();
					
					if (newAnswerText != null && !newAnswerText.trim().isEmpty() && !newAnswerText.trim().equals(savedAnswerText)) {
						answer.answerText = holder.questionAnswer.getText().toString().trim();
						
						ParagraphQuestionAnswered event = new ParagraphQuestionAnswered();
						event.answer = answer;
						EventBus.getDefault().post(event);
					}
				}
			}
		});
		
		return convertView;
	}

	@Override
	public int getItemViewType(int position) {
		return 1;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

}
