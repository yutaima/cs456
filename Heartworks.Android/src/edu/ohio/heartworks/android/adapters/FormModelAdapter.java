package edu.ohio.heartworks.android.adapters;

import it.sauronsoftware.cron4j.Predictor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import edu.ohio.heartworks.android.models.FormModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class FormModelAdapter extends ArrayAdapter<FormModel> {
	private final LayoutInflater inflater;
	private final List<FormModel> values;
	
	private static class ViewHolder {
		public TextView title;
		public TextView subTitle;
	}
	
	public FormModelAdapter(Context context, List<FormModel> values) {
		// textViewResourceId is not necessary with getView being overridden
		super(context, 0, values);
		
		this.inflater = LayoutInflater.from(context);
		this.values = values;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = inflater.inflate(android.R.layout.simple_list_item_activated_2, parent, false);
			
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(android.R.id.text1);
			holder.subTitle = (TextView) convertView.findViewById(android.R.id.text2);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
				
		holder.title.setText(values.get(position).name);
		
		Predictor p = new Predictor(values.get(position).cronExpression);
		Date nextMatchingDate = p.nextMatchingDate();
		
		holder.subTitle.setText(new SimpleDateFormat("MM/dd hh:mm a", Locale.US).format(nextMatchingDate));

		return convertView;
	}
}
