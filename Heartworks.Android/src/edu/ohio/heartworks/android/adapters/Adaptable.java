package edu.ohio.heartworks.android.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public interface Adaptable {
	int getItemViewType(int position);
	boolean isEnabled(int position);
	View getView(int position, View convertView, ViewGroup parent, LayoutInflater inflater);
}
