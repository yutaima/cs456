package edu.ohio.heartworks.android.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NavigationHeaderAdaptable implements Adaptable {
	
	private String headerText;

	public NavigationHeaderAdaptable(String headerText) {
		this.headerText = headerText;
	}
	
	private static class ViewHolder {
		public TextView header;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent, LayoutInflater inflater) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = inflater.inflate(android.R.layout.preference_category, parent, false);
			
			holder = new ViewHolder();
			holder.header = (TextView) convertView.findViewById(android.R.id.title);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		if (headerText != null) holder.header.setText(headerText);
		
		return convertView;
	}

	@Override
	public int getItemViewType(int position) {
		return 1;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}
}
