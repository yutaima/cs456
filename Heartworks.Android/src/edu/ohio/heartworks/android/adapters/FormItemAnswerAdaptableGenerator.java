package edu.ohio.heartworks.android.adapters;

import java.util.ArrayList;
import java.util.List;

import edu.ohio.heartworks.domain.formsession.FormItemAnswer;
import edu.ohio.heartworks.domain.formsession.FormItemAnswerVisitor;
import edu.ohio.heartworks.domain.formsession.ParagraphQuestionAnswer;
import edu.ohio.heartworks.domain.formsession.RangedQuestionAnswer;

public class FormItemAnswerAdaptableGenerator implements FormItemAnswerVisitor {

	private final List<FormItemAnswer> items;
	private final List<Adaptable> adaptableItems;
	
	public FormItemAnswerAdaptableGenerator(List<FormItemAnswer> items) {
		this.items = items;
		this.adaptableItems = new ArrayList<Adaptable>();
	}
	
	public List<Adaptable> generate() {
		for (FormItemAnswer item : items) {
			item.accept(this);
		}
		
		return adaptableItems;
	}

	@Override
	public void visit(ParagraphQuestionAnswer answer) {
		adaptableItems.add(new ParagraphQuestionAnswerAdaptable(answer));
	}

	@Override
	public void visit(RangedQuestionAnswer answer) {
		adaptableItems.add(new RangedQuestionAnswerAdaptable(answer));
	}
}
