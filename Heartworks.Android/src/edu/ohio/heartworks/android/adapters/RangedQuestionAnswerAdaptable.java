package edu.ohio.heartworks.android.adapters;

import de.greenrobot.event.EventBus;
import edu.ohio.heartworks.domain.formsession.RangedQuestionAnswer;
import edu.ohio.heartworks.android.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class RangedQuestionAnswerAdaptable implements Adaptable {
	
	private final RangedQuestionAnswer answer;
	
	public static class RangedQuestionAnswered {
		public RangedQuestionAnswer answer;
	}
	
	// See http://www.youtube.com/watch?v=N6YdwzAvwOA for details on this
	private static class ViewHolder {
		private TextView questionPrompt;
		public Spinner questionAnswerSpinner;
	}

	public RangedQuestionAnswerAdaptable(RangedQuestionAnswer answer) {
		this.answer = answer;
	}
	
	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	
	@Override
	public View getView(final int questionPosition, View convertView, ViewGroup parent, LayoutInflater inflater) {
		final ViewHolder holder;
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.formitem_rangedquestion, parent, false);
			
			holder = new ViewHolder();
			holder.questionPrompt = (TextView) convertView.findViewById(R.id.rangedquestion_prompt);
			holder.questionAnswerSpinner = (Spinner) convertView.findViewById(R.id.rangedquestion_answers);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.questionPrompt.setText(answer.question.questionText);
		
        ArrayAdapter<String> answersAdapter = new ArrayAdapter<String>(inflater.getContext(), android.R.layout.simple_spinner_item, android.R.id.text1);
        answersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        answersAdapter.add("Select One");
        
        for (String answerOption : answer.question.answerList) {
        	answersAdapter.add(answerOption);
        }
		
		holder.questionAnswerSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		
			private boolean isInitialized = false;
			
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int answerPosition, long id) {
				if (!isInitialized) {
					isInitialized = true;
					return;
				}
				
				if (answerPosition == 0) return;
				
				answer.answerIndex = answerPosition - 1;
				
				RangedQuestionAnswered event = new RangedQuestionAnswered();
				event.answer = answer;
				
				EventBus.getDefault().post(event);
			}
	
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
		
		holder.questionAnswerSpinner.setAdapter(answersAdapter);
		holder.questionAnswerSpinner.setSelection(answer.answerIndex);
		
		return convertView;
	}
}
