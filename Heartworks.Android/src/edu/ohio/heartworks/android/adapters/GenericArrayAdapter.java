package edu.ohio.heartworks.android.adapters;

import java.util.List;

import android.content.Context;
import android.view.*;
import android.widget.*;

public class GenericArrayAdapter extends ArrayAdapter<Adaptable> {
	private final LayoutInflater inflater;
	private final List<Adaptable> values;
	private final int viewTypeCount;

	public GenericArrayAdapter(Context context, List<Adaptable> values, int viewTypeCount) {
		// textViewResourceId is not necessary with getView being overridden
		super(context, 0, values);
		
		this.inflater = LayoutInflater.from(context);
		this.values = values;
		this.viewTypeCount = viewTypeCount;
	}
	
	/*
	 * getItemViewType() and getViewTypeCount() are to ensure that row recycling
	 * works. Android will maintain separate object pools and will only hand you
	 * a row back to recycle that is of the correct type.
	 */

	// Must return an int between 0 and getViewTypeCount() - 1
	@Override
	public int getItemViewType(int position) {
		int viewType = values.get(position).getItemViewType(position);
		return viewType;
	}

	@Override
	public int getViewTypeCount() {
		return viewTypeCount;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return values.get(position).getView(position, convertView, parent, inflater);
	}

	@Override
    public boolean isEnabled(int position) {
		return values.get(position).isEnabled(position);
    }
}