package edu.ohio.heartworks.android.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NavigationItemAdaptable implements Adaptable {
	
	private String titleText;
	private String subTitleText;

	public NavigationItemAdaptable(String titleText, String subTitleText) {
		this.titleText = titleText;
		this.subTitleText = subTitleText;
	}
	
	private static class ViewHolder {
		public TextView text1;
		public TextView text2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent, LayoutInflater inflater) {
		ViewHolder holder = null;
		
		if (convertView == null) {
			
			holder = new ViewHolder();
			
			// note simple_list_item_activated_2 exists as well
			if (subTitleText != null) {
				convertView = inflater.inflate(android.R.layout.simple_list_item_activated_2, parent, false);
				holder.text2 = (TextView) convertView.findViewById(android.R.id.text2);
			} else {
				convertView = inflater.inflate(android.R.layout.simple_list_item_activated_1, parent, false);
			}
			
			holder.text1 = (TextView) convertView.findViewById(android.R.id.text1);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		if (titleText != null) holder.text1.setText(titleText);
		if (subTitleText != null) holder.text2.setText(subTitleText);
		
		return convertView;
	}

	@Override
	public int getItemViewType(int position) {
		return subTitleText == null ? 0 : 1;
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}
}
