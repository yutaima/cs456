package edu.ohio.heartworks.android.models;

import java.util.List;
import java.util.UUID;

import edu.ohio.heartworks.infrastructure.UniqueObject;

public class LessonModel implements UniqueObject {
	
	public UUID id;
	public String lessonName;
	public String htmlContent;
	public List<String> files;

	@Override
	public UUID getId() {
		return id;
	}
}
