package edu.ohio.heartworks.android.models;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import edu.ohio.heartworks.domain.fooddiary.FoodTypes.FoodType;
import edu.ohio.heartworks.infrastructure.UniqueObject;

public class FoodDiaryModel implements UniqueObject {
	public UUID id;
	public Date dateCreated;
	public Date dateCreatedStatic;  
		// This date cannot be modified by the user. This is visible to doctors and nurses so that they can compare this to dataCreated.
	public int version;
	
	public String description;
	public List<String> imageFiles;
	public FoodType foodType;
	public boolean isChecked;
	
	
	@Override
	public UUID getId() {
		return this.id;
	}
}
