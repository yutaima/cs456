package edu.ohio.heartworks.android.models;

import java.util.UUID;

import edu.ohio.heartworks.infrastructure.UniqueObject;

public class ResourceUploadModel implements UniqueObject {
	public final UUID aggregateId;
	public final String fileName;
	
	public ResourceUploadModel(UUID aggregateId, String fileName) {
		this.aggregateId = aggregateId;
		this.fileName = fileName;
	}

	@Override
	public UUID getId() {
		return this.aggregateId;
	}
}
