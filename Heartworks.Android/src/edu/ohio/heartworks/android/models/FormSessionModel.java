package edu.ohio.heartworks.android.models;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import edu.ohio.heartworks.domain.formsession.FormItemAnswer;
import edu.ohio.heartworks.infrastructure.UniqueObject;

public class FormSessionModel implements UniqueObject {
	public UUID id;
	public UUID formId;
	public String formName;
	public Date dateStarted;
	public List<FormItemAnswer> sessionAnswers;
	public int version;

	public UUID getId() {
		return id;
	}
}
