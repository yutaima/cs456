package edu.ohio.heartworks.android.models;

import java.util.List;
import java.util.UUID;

import edu.ohio.heartworks.domain.form.FormItem;
import edu.ohio.heartworks.infrastructure.UniqueObject;

public class FormModel implements UniqueObject {
	
	public UUID id;
	public String name;
	public String cronExpression;
	public List<FormItem> formItems;
    
	@Override
	public UUID getId() {
		return id;
	}
}