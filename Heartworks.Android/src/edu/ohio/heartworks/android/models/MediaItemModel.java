package edu.ohio.heartworks.android.models;

import java.util.UUID;

import edu.ohio.heartworks.domain.media.MediaTypes.MediaType;
import edu.ohio.heartworks.infrastructure.UniqueObject;

public class MediaItemModel implements UniqueObject {
	
	public UUID id;
	public String name;
	public String fileName;
	public MediaType mediaType;

	@Override
	public UUID getId() {
		return id;
	}
}