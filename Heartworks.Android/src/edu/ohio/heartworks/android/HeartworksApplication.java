package edu.ohio.heartworks.android;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import edu.ohio.heartworks.android.services.LifetimeService;
import edu.ohio.heartworks.infrastructure.BusBootstrapper;

@ReportsCrashes(formKey = "dEFZd3k0WDNON3V1RWlsNHZqUkxoNUE6MQ")
public class HeartworksApplication extends Application implements ActivityLifecycleCallbacks { 
	// http://developer.android.com/reference/android/app/Application.html
	// http://developer.android.com/guide/components/fundamentals.html
	
	private static final String TAG = HeartworksApplication.class.getName();
	
	private int runningActivities = 0;
			
    @Override
    public void onCreate() {
        super.onCreate();
                
        RoboGuice.setBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE,  RoboGuice.newDefaultRoboModule(this), new MainModule(this));
		
        RoboInjector injector = RoboGuice.getInjector(this);
        
        BusBootstrapper busBootstrapper = injector.getInstance(BusBootstrapper.class);
        busBootstrapper.Bootstrap();
        
        startService(new Intent(this, LifetimeService.class));
        registerActivityLifecycleCallbacks(this);
        
        ACRA.init(this);
    }

	@Override
	public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
		Log.d(TAG, activity.getComponentName().getShortClassName() + ":onActivityCreated");
	}

	@Override
	public void onActivityStarted(Activity activity) {
		Log.d(TAG, activity.getComponentName().getShortClassName() + ":onActivityStarted");
	}

	@Override
	public void onActivityResumed(Activity activity) {
		Log.d(TAG, activity.getComponentName().getShortClassName() + ":onActivityResumed");
		runningActivities++;
	}

	@Override
	public void onActivityPaused(Activity activity) {
		Log.d(TAG, activity.getComponentName().getShortClassName() + ":onActivityPaused");
	}

	@Override
	public void onActivityStopped(Activity activity) {
		Log.d(TAG, activity.getComponentName().getShortClassName() + ":onActivityStopped");
		runningActivities--;
		
		if (runningActivities == 0) {
	        Log.d(TAG, "Application is being backgrounded");
		}
	}

	@Override
	public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
		Log.d(TAG, activity.getComponentName().getShortClassName() + ":onActivitySaveInstanceState");
	}

	@Override
	public void onActivityDestroyed(Activity activity) {
		Log.d(TAG, activity.getComponentName().getShortClassName() + ":onActivityDestroyed");
	}
}