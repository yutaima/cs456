package edu.ohio.heartworks.android;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.map.ObjectMapper;

import android.content.Context;
import android.os.Environment;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.config.UuidSupport;
import com.db4o.constraints.UniqueFieldValueConstraint;
import com.db4o.ta.TransparentActivationSupport;
import com.google.inject.AbstractModule;
import com.google.inject.BindingAnnotation;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;

import edu.ohio.heartworks.android.domain.AndroidFoodDiaryCommandHandler;
import edu.ohio.heartworks.android.eventhandlers.FoodDiaryEventHandler;
import edu.ohio.heartworks.android.eventhandlers.FormEventHandler;
import edu.ohio.heartworks.android.eventhandlers.FormSessionEventHandler;
import edu.ohio.heartworks.android.eventhandlers.LessonEventHandler;
import edu.ohio.heartworks.android.eventhandlers.MediaItemEventHandler;
import edu.ohio.heartworks.android.models.FoodDiaryModel;
import edu.ohio.heartworks.android.models.FormModel;
import edu.ohio.heartworks.android.models.FormSessionModel;
import edu.ohio.heartworks.android.models.ResourceDownloadModel;
import edu.ohio.heartworks.android.models.ResourceUploadModel;
import edu.ohio.heartworks.android.services.StateRebuilder;
import edu.ohio.heartworks.android.share.FacebookSharePlatform;
import edu.ohio.heartworks.android.share.SharePlatform;
import edu.ohio.heartworks.api.HeartWorksMessages;
import edu.ohio.heartworks.api.ServerAggregateDescriptor;
import edu.ohio.heartworks.domain.form.FormCommandHandler;
import edu.ohio.heartworks.domain.formsession.FormSessionCommandHandler;
import edu.ohio.heartworks.domain.media.MediaItemCommandHandler;
import edu.ohio.heartworks.infrastructure.AggregateRoot;
import edu.ohio.heartworks.infrastructure.BusBootstrapper;
import edu.ohio.heartworks.infrastructure.CommandHandler;
import edu.ohio.heartworks.infrastructure.CommandSender;
import edu.ohio.heartworks.infrastructure.DefaultRepository;
import edu.ohio.heartworks.infrastructure.EventHandler;
import edu.ohio.heartworks.infrastructure.EventPublisher;
import edu.ohio.heartworks.infrastructure.EventStore;
import edu.ohio.heartworks.infrastructure.InProcessBus;
import edu.ohio.heartworks.infrastructure.QueueingCommandSender;
import edu.ohio.heartworks.infrastructure.RebuildableEventStore;
import edu.ohio.heartworks.infrastructure.Repository;
import edu.ohio.heartworks.infrastructure.RepositoryFactory;
import edu.ohio.heartworks.infrastructure.UniqueObject;


// MainModule is in a way the configuration file for Guice. Typical Android apps not using Guice do not need this module.

public class MainModule extends AbstractModule { // http://google-guice.googlecode.com/svn/trunk/javadoc/com/google/inject/AbstractModule.html
    private Context context;

    public MainModule(Context context) {
        this.context = context;
    }

    @Override
    protected void configure() {
        bind(File.class).annotatedWith(ApplicationData.class).toInstance(context.getFilesDir());

        bind(File.class).annotatedWith(UserData.class)
            .toInstance(new File(Environment.getExternalStorageDirectory(), getClass().getPackage().getName()));

        bind(String.class).annotatedWith(Server.class).toInstance(context.getString(R.string.default_server_url));

        bind(EventPublisher.class).to(InProcessBus.class).in(Singleton.class);

        bind(StateRebuilder.class);

        // Because these event handlers are specific to Android, it's OK for them to have IOC annotations
        Multibinder<EventHandler> eventBinder = Multibinder.newSetBinder(binder(), EventHandler.class);
        eventBinder.addBinding().to(FormEventHandler.class);
        eventBinder.addBinding().to(FormSessionEventHandler.class);
        eventBinder.addBinding().to(FoodDiaryEventHandler.class);
        eventBinder.addBinding().to(MediaItemEventHandler.class);
        eventBinder.addBinding().to(LessonEventHandler.class);
    }
    
    @Provides
    SharePlatform provideSharePlatform(@UserData File userDataRoot) {
        return new FacebookSharePlatform(userDataRoot);
    }

    // Command handlers are part of the domain "shared" code, so keep them clean of 3rd-party library dependencies (eg. IOC)
    @Provides @Singleton
    Set<CommandHandler> provideCommandHandlers(RepositoryFactory repositoryFactory, Db4oReadModelFactory readModelFactory, @UserData File userDataRoot) {
        Set<CommandHandler> handlers = new HashSet<CommandHandler>();
        handlers.add(new FormCommandHandler(repositoryFactory));
        handlers.add(new FormSessionCommandHandler(repositoryFactory));
        handlers.add(new MediaItemCommandHandler(repositoryFactory));
        handlers.add(new AndroidFoodDiaryCommandHandler(repositoryFactory, readModelFactory, userDataRoot));

        return handlers;
    }

    @Provides @Singleton @ForEventStore
    ObjectContainer provideEventStoreServer(@ApplicationData File root) {
        File dbFile = new File(root, "eventStore.db4o");

        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().add(new TransparentActivationSupport());
        config.common().add(new UuidSupport());

        return Db4oEmbedded.openFile(config, dbFile.getAbsolutePath());
    }

    @Provides @Singleton @ForReadModel
    ObjectContainer provideReadModelServer(@ApplicationData File root) {
        File dbFile = new File(root, "readModel.db4o");

        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().add(new TransparentActivationSupport());
        config.common().add(new UuidSupport());

        config.common().objectClass(FormModel.class).updateDepth(4);

        config.common().objectClass(FoodDiaryModel.class).updateDepth(2);
        config.common().objectClass(FoodDiaryModel.class).cascadeOnDelete(true);

        config.common().objectClass(FormSessionModel.class).updateDepth(4);

        config.common().objectClass(FormModel.class).objectField("id").indexed(true);				
        config.common().objectClass(FoodDiaryModel.class).objectField("id").indexed(true);				
        config.common().objectClass(FormSessionModel.class).objectField("id").indexed(true);				
        config.common().objectClass(ResourceDownloadModel.class).objectField("id").indexed(true);				
        config.common().objectClass(ResourceUploadModel.class).objectField("id").indexed(true);
        config.common().objectClass(ServerAggregateDescriptor.class).objectField("id").indexed(true);

        config.common().add(new UniqueFieldValueConstraint(FormModel.class, "id"));
        config.common().add(new UniqueFieldValueConstraint(FoodDiaryModel.class, "id"));
        config.common().add(new UniqueFieldValueConstraint(FormSessionModel.class, "id"));
        config.common().add(new UniqueFieldValueConstraint(ServerAggregateDescriptor.class, "id"));

        return Db4oEmbedded.openFile(config, dbFile.getAbsolutePath());
    }

    @Provides
    ObjectMapper provideObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new HeartWorksMessages());

        return mapper;
    }

    @Provides @Singleton
    RebuildableEventStore provideRebuildableEventStore(EventPublisher publisher, @ForEventStore ObjectContainer container) {
        return new Db4oEventStore(publisher, container);
    }

    @Provides
    EventStore provideEventStore(RebuildableEventStore eventStore) {
        return eventStore;
    }

    @Provides @Singleton
    QueueingCommandSender provideCommandQueue(@ForReadModel ObjectContainer container) {
        return new Db4oPersistantCommandBus(container);
    }

    @Provides
    CommandSender provideCommandSender(QueueingCommandSender commandSender) {
        return commandSender;
    }

    @Provides
    Db4oReadModelFactory provideReadModelFactory(final @ForReadModel ObjectContainer container) {

        return new Db4oReadModelFactory() {			
            @Override
            public <T extends UniqueObject> Db4oReadModel<T> openSession(Class<T> modelType) {
                return new Db4oReadModel<T>(modelType, container.ext().openSession());
            }
        };
    }

    @Provides
    RepositoryFactory provideRepositoryFactory(final EventStore storage) {
        return new RepositoryFactory() {
            @Override
            public <T extends AggregateRoot> Repository<T> getRepository(Class<T> type) {
                return new DefaultRepository<T>(type, storage);
            }
        };
    }

    @Provides
    BusBootstrapper provideBusBootstrapper(
            Set<CommandHandler> commandHandlers,
            Set<EventHandler> eventHandlers,
            CommandSender commandBus,
            EventPublisher eventBus) {

        return new BusBootstrapper(commandHandlers, eventHandlers, commandBus, eventBus);
    }

    @Retention(RUNTIME) @Target({ FIELD, PARAMETER, METHOD }) @BindingAnnotation
    public @interface ForEventStore {}

    @Retention(RUNTIME) @Target({ FIELD, PARAMETER, METHOD }) @BindingAnnotation
    public @interface ForReadModel {}

    @Retention(RUNTIME) @Target({ FIELD, PARAMETER, METHOD }) @BindingAnnotation
    public @interface Server {}

    @Retention(RUNTIME) @Target({ FIELD, PARAMETER, METHOD }) @BindingAnnotation
    public @interface ApplicationData {}

    @Retention(RUNTIME) @Target({ FIELD, PARAMETER, METHOD }) @BindingAnnotation
    public @interface UserData {}
}