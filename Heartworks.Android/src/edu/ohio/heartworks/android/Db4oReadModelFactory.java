package edu.ohio.heartworks.android;

import edu.ohio.heartworks.infrastructure.UniqueObject;

public abstract class Db4oReadModelFactory {
	public abstract <T extends UniqueObject> Db4oReadModel<T> openSession(Class<T> modelType);
}