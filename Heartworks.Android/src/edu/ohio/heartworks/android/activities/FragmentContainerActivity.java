package edu.ohio.heartworks.android.activities;

import roboguice.activity.RoboFragmentActivity;
import edu.ohio.heartworks.android.R;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class FragmentContainerActivity<F extends Fragment> extends RoboFragmentActivity {
    protected F fragment;

    public FragmentContainerActivity(F fragment) {
        this.fragment = fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic_container);
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.generic_container, fragment).commit();
        }
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpTo(this, new Intent(this, HomeListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
