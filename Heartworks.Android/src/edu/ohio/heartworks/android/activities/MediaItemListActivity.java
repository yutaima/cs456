package edu.ohio.heartworks.android.activities;

import edu.ohio.heartworks.android.fragments.MediaItemListFragment;

public class MediaItemListActivity extends FragmentContainerActivity<MediaItemListFragment> {
    
    public MediaItemListActivity() {
        super(new MediaItemListFragment());
    }
}
