package edu.ohio.heartworks.android.activities;

import java.util.Arrays;

import android.content.Intent;

import com.facebook.Session;
import com.facebook.widget.UserSettingsFragment;

public class FacebookLoginActivity extends FragmentContainerActivity<UserSettingsFragment> {
    public FacebookLoginActivity() {        
        super(new UserSettingsFragment());
        
        super.fragment.setPublishPermissions(Arrays.asList("publish_actions"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }
}