package edu.ohio.heartworks.android.activities;

import java.util.List;
import java.util.UUID;

import com.db4o.query.Predicate;
import com.google.inject.Inject;

import de.greenrobot.event.EventBus;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.InjectView;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.ListView;
import edu.ohio.heartworks.android.adapters.Adaptable;
import edu.ohio.heartworks.android.adapters.FormItemAnswerAdaptableGenerator;
import edu.ohio.heartworks.android.adapters.GenericArrayAdapter;
import edu.ohio.heartworks.android.adapters.ParagraphQuestionAnswerAdaptable.ParagraphQuestionAnswered;
import edu.ohio.heartworks.android.adapters.RangedQuestionAnswerAdaptable.RangedQuestionAnswered;
import edu.ohio.heartworks.android.models.FormModel;
import edu.ohio.heartworks.android.models.FormSessionModel;
import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.R;
import edu.ohio.heartworks.domain.formsession.FormSessionCommands.SubmitFormSessionAnswer;
import edu.ohio.heartworks.domain.formsession.FormSessionGenerator;
import edu.ohio.heartworks.domain.formsession.FormSessionCommands.*;
import edu.ohio.heartworks.infrastructure.CommandSender;

public class FormDetailActivity extends RoboFragmentActivity {

	public static final String ARG_FORM_ID = "form_id";
	
	@Inject Db4oReadModelFactory readModelFactory;
	@Inject CommandSender commandBus;
	
	@InjectView(android.R.id.list) ListView listView;
	
	private GenericArrayAdapter adapter;
	private FormSessionModel session;
	
	public static Intent createIntent(Context context, UUID formId) {
		return new Intent(context, FormDetailActivity.class)
			.putExtra(FormDetailActivity.ARG_FORM_ID, formId);
	}

	@Override @SuppressWarnings("serial")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_form_detail);
		
		Bundle extras = getIntent().getExtras();
		
		if (extras == null) {
			setResult(RESULT_CANCELED);
			finish();
			return;
		}
		
		final UUID formId = (UUID) extras.get(ARG_FORM_ID);
		
		if (formId == null) {
			setResult(RESULT_CANCELED);
			finish();
			return;
		}

		Db4oReadModel<FormSessionModel> formSessionReadModel = readModelFactory.openSession(FormSessionModel.class);
		
		// Here, we're just storing a local copy of the session. Changes made to this object (even if saved)
		// will *not* actually change the session in the DB (that must be done by commands)
		this.session = formSessionReadModel.findByQuery(new Predicate<FormSessionModel>() {
			@Override public boolean match(FormSessionModel session) {
				return session.formId.equals(formId);
			}
		});
		
		formSessionReadModel.close();
		
		if (session == null) {
			Db4oReadModel<FormModel> formReadModel = readModelFactory.openSession(FormModel.class);
			
			FormModel form = formReadModel.findById(formId);
			
			formReadModel.close();
			
			if (form != null) {
				
				FormSessionGenerator generator = new FormSessionGenerator(form.formItems);
				
				session = new FormSessionModel();
				session.id = UUID.randomUUID();
				session.formId = form.id;
				session.version = 0;
				session.sessionAnswers = generator.generate();
				
				StartFormSession command = new StartFormSession();
				command.aggregateId = session.id;
				command.formId = session.formId;
				command.expectedVersion = session.version;
				command.answers = session.sessionAnswers;
				
				commandBus.send(command);
			}
		}
		
		FormItemAnswerAdaptableGenerator itemGenerator = new FormItemAnswerAdaptableGenerator(session.sessionAnswers);
		
		List<Adaptable> adaptables = itemGenerator.generate();
		
		this.adapter = new GenericArrayAdapter(this, adaptables, 4);
		
		this.listView.setAdapter(adapter);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		EventBus.getDefault().register(this);
	}
	
	public void onEvent(final RangedQuestionAnswered event) {
		SubmitFormSessionAnswer command = new SubmitFormSessionAnswer();
		command.aggregateId = session.id;
		command.answer = event.answer;
		command.expectedVersion = session.version;
		
		commandBus.send(command);
		
		session.version++;
	}
	
	public void onEvent(final ParagraphQuestionAnswered event) {
		SubmitFormSessionAnswer command = new SubmitFormSessionAnswer();
		command.aggregateId = session.id;
		command.answer = event.answer;
		command.expectedVersion = session.version;
		
		commandBus.send(command);
		
		session.version++;
	}
	
	@Override
	protected void onPause() {
		EventBus.getDefault().unregister(this);
		
		super.onPause();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpTo(this, new Intent(this, HomeListActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
