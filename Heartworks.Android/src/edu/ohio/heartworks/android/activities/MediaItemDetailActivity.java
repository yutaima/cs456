package edu.ohio.heartworks.android.activities;

import java.io.File;
import java.util.UUID;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.MainModule.UserData;
import edu.ohio.heartworks.android.R;
import edu.ohio.heartworks.android.models.MediaItemModel;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.MediaController;
import roboguice.activity.RoboFragmentActivity;

public class MediaItemDetailActivity extends RoboFragmentActivity implements OnPreparedListener, MediaController.MediaPlayerControl {
	
	public static final String ARG_MEDIA_ID = "media_id";
	
	@Inject private Db4oReadModelFactory readModelFactory;
	@Inject private @UserData File userDataRoot;
	
	private Integer currentPosition;
	private Boolean isLooping;
	private Boolean isPlaying;

	private Handler handler = new Handler();

	private MediaPlayer mediaPlayer;
	private MediaController mediaController;
	
	public static Intent createIntent(Context context, UUID mediaItemId) {
		return new Intent(context, MediaItemDetailActivity.class).putExtra(ARG_MEDIA_ID, mediaItemId);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_mediaitem_detail);
		
		Bundle extras = getIntent().getExtras();
		
		UUID mediaId = null;
		
		if (extras != null) {
			mediaId = (UUID) extras.get(ARG_MEDIA_ID);
		}
		
		if (mediaId == null) {
			setResult(RESULT_CANCELED);
			finish();
			return;
		}
				
		if (savedInstanceState != null) {
			this.currentPosition = savedInstanceState.getInt("currentPosition");
			this.isPlaying = savedInstanceState.getBoolean("isPlaying");
			this.isLooping = savedInstanceState.getBoolean("isLooping");
		}
		
		if (currentPosition == null) currentPosition = 0;
		if (isLooping == null) isLooping = false;
		if (isPlaying == null) isPlaying = false;
		
		Db4oReadModel<MediaItemModel> readModel = readModelFactory.openSession(MediaItemModel.class);
		MediaItemModel mediaItem = readModel.findById(mediaId);
		
		readModel.close();
		
		if (mediaItem == null) {
			setResult(RESULT_CANCELED);
			finish();
			return;
		}
		
		File directory =  new File(userDataRoot, mediaItem.id.toString());
		File mediaFile = new File(directory, mediaItem.fileName);

		if (!mediaFile.exists()) {
			setResult(RESULT_CANCELED);
			finish();
			return;
		}
		
		// http://developer.android.com/reference/android/media/MediaPlayer.html
		mediaPlayer = new MediaPlayer();
		
		try {
			mediaPlayer.setDataSource(mediaFile.getAbsolutePath());
			
			mediaPlayer.setOnPreparedListener(this);
			mediaPlayer.prepareAsync();
		} catch (Exception e) {
			e.printStackTrace();
			
			setResult(RESULT_CANCELED);
			finish();
			return;
		}
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setTitle(mediaItem.name);
	}

	@Override
	public void onPrepared(final MediaPlayer mediaPlayer) {
		mediaPlayer.setLooping(isLooping);
		
		if (isPlaying) {
			mediaPlayer.start();
		}

		mediaPlayer.seekTo(currentPosition);
		
		mediaController = new MediaController(this);
		mediaController.setMediaPlayer(this);
	    mediaController.setAnchorView(findViewById(R.id.media_audio_container));

		handler.post(new Runnable() {
			public void run() {
				mediaController.setEnabled(true);
				mediaController.show();
			}
		});
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mediaController.show();
		return false;
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (mediaController != null) mediaController.hide();
		
		if (mediaPlayer != null) {
			mediaPlayer.stop();
			mediaPlayer.release();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpTo(this, new Intent(this, HomeListActivity.class));
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
    public void onSaveInstanceState (Bundle outState) {
    	super.onSaveInstanceState(outState);
    	
    	outState.putInt("currentPosition", mediaPlayer.getCurrentPosition());
    	outState.putBoolean("isPlaying", mediaPlayer.isPlaying());
    	outState.putBoolean("isLooping", mediaPlayer.isLooping());
    }

	@Override
	public void start() {
		isPlaying = true;
		
		mediaPlayer.start();
	}

	@Override
	public void pause() {
		mediaPlayer.pause();
	}

	@Override
	public int getDuration() {
		return mediaPlayer.getDuration();
	}

	@Override
	public int getCurrentPosition() {
		return mediaPlayer.getCurrentPosition();
	}

	@Override
	public void seekTo(int pos) {
		mediaPlayer.seekTo(pos);
	}

	@Override
	public boolean isPlaying() {
		return mediaPlayer.isPlaying();
	}

	@Override
	public int getBufferPercentage() {
		return 0;
	}

	@Override
	public boolean canPause() {
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		return true;
	}

	@Override
	public boolean canSeekForward() {
		return true;
	}
}
