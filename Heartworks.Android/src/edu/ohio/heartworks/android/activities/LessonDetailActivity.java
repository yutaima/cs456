package edu.ohio.heartworks.android.activities;

import java.io.File;
import java.util.UUID;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.webkit.WebView;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.MainModule.UserData;
import edu.ohio.heartworks.android.R;
import edu.ohio.heartworks.android.models.LessonModel;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.InjectView;

public class LessonDetailActivity extends RoboFragmentActivity {

	public static final String ARG_LESSON_ID = "lesson_id";
	
	@Inject private Db4oReadModelFactory readModelFactory;
	@Inject private @UserData File userDataRoot;
	
	@InjectView(R.id.lesson_webview) private WebView webView;
	
	public static Intent createIntent(Context context, UUID lessonId) {
		return new Intent(context, LessonDetailActivity.class)
			.putExtra(ARG_LESSON_ID, lessonId);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_lesson_detail);
		
		Bundle extras = getIntent().getExtras();
		
		UUID lessonId = null;
		
		if (extras != null) {
			lessonId = (UUID) extras.get(ARG_LESSON_ID);
		}
		
		if (lessonId == null) {
			setResult(RESULT_CANCELED);
			finish();
			return;
		}
		
		Db4oReadModel<LessonModel> readModel = readModelFactory.openSession(LessonModel.class);
		LessonModel lesson = readModel.findById(lessonId);
		
		readModel.close();
		
		if (lesson == null) {
			setResult(RESULT_CANCELED);
			finish();
			return;
		}
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle(lesson.lessonName);
		
		String baseUrl = "file://" + new File(userDataRoot, lessonId.toString()).getAbsolutePath() + "/";
		
		webView.loadDataWithBaseURL(baseUrl, lesson.htmlContent, "text/html", "utf-8", null);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpTo(this, new Intent(this, HomeListActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
