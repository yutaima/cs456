package edu.ohio.heartworks.android.activities;

import edu.ohio.heartworks.android.fragments.LessonListFragment;

public class LessonListActivity extends FragmentContainerActivity<LessonListFragment> {

    public LessonListActivity() {
        super(new LessonListFragment());
    }
}
