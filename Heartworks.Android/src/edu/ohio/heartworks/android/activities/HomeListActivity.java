package edu.ohio.heartworks.android.activities;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.InjectResource;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import de.greenrobot.event.EventBus; //http://www.slideshare.net/greenrobot/eventbus-for-android-15314813
import edu.ohio.heartworks.android.R;
import edu.ohio.heartworks.android.adapters.DataSetChanged;
import edu.ohio.heartworks.android.fragments.FoodDiaryListFragment;
import edu.ohio.heartworks.android.fragments.FormListFragment;
import edu.ohio.heartworks.android.fragments.HomeListFragment.ExerciseLogsClicked;
import edu.ohio.heartworks.android.fragments.HomeListFragment.FoodDiariesClicked;
import edu.ohio.heartworks.android.fragments.HomeListFragment.LessonsClicked;
import edu.ohio.heartworks.android.fragments.HomeListFragment.MediaClicked;
import edu.ohio.heartworks.android.fragments.HomeListFragment.TasksClicked;
import edu.ohio.heartworks.android.fragments.LessonListFragment;
import edu.ohio.heartworks.android.fragments.MediaItemListFragment;
import edu.ohio.heartworks.android.settings.SettingsActivity;

public class HomeListActivity extends RoboFragmentActivity {

	@InjectResource(R.bool.use_two_pane) private boolean isTwoPane;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_list);
	}
	
	@Override
	public void onResume() {
		super.onResume();

		EventBus.getDefault().register(this);
	}
	
	@Override
	public void onPause() {
		EventBus.getDefault().unregister(this);
		
		super.onPause();
	}
	
	public void onEvent(DataSetChanged event) {
		this.recreate();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_settings:
		    startActivity(new Intent(this, SettingsActivity.class));
		    
			return true;
		}
		
		return false;
	}
	
	public void onEvent(TasksClicked event) {
		Toast.makeText(this, "The Tasks section is incomplete. Please do not use for now.", Toast.LENGTH_SHORT).show();
		// http://developer.android.com/guide/topics/ui/notifiers/toasts.html

		launchFragmentOrActivity(new FormListFragment(), new Intent(this, FormListActivity.class));
	}
	
	public void onEvent(FoodDiariesClicked event) {
		launchFragmentOrActivity(new FoodDiaryListFragment(), new Intent(this, FoodDiaryListActivity.class));
	}
	
	public void onEvent(ExerciseLogsClicked event) {
		Toast.makeText(this, "Not implemented", Toast.LENGTH_SHORT).show();
	}

	public void onEvent(MediaClicked event) {
		Toast.makeText(this, "The Media section has not been implemented.", Toast.LENGTH_SHORT).show();
		launchFragmentOrActivity(new MediaItemListFragment(), new Intent(this, MediaItemListActivity.class));
	}
	
	public void onEvent(LessonsClicked event) {
		Toast.makeText(this, "The Lessons section is incomplete. Please do not use for now.", Toast.LENGTH_SHORT).show();
		launchFragmentOrActivity(new LessonListFragment(), new Intent(this, LessonListActivity.class));
	}
	
	private <F extends Fragment> void launchFragmentOrActivity(F fragment, Intent intent) {
		if (this.isTwoPane) {
			getSupportFragmentManager().beginTransaction().replace(R.id.home_detail_container, fragment).commit();
		} else {
			startActivity(intent);
		}
	}
}
