package edu.ohio.heartworks.android.activities;

import edu.ohio.heartworks.android.fragments.FormListFragment;

public class FormListActivity extends FragmentContainerActivity<FormListFragment> {
    
    public FormListActivity() {
        super(new FormListFragment());
    }
}