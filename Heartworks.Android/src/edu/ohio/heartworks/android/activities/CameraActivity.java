package edu.ohio.heartworks.android.activities;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.io.FileUtils;

import roboguice.activity.RoboFragmentActivity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Toast;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.MainModule.UserData;

// This is just a simple activity (no view) that wraps Android's Intent(MediaStore.ACTION_IMAGE_CAPTURE)
// to ensure that a URI to the captured image is returned in the result intent
public class CameraActivity extends RoboFragmentActivity {
	
	public static final String ARG_ID = "id";
	public static final String ARG_FILENAME = "fileName";
    public static final String ARG_ISGALLERY = "isGallery";
	
	private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_PICK_PHOTO = 2;
    private static final int CROP_PIC = 3;
    private Uri fileUri;
		
	@Inject private @UserData File userDataRoot;
	private UUID aggregateId;
	private String fileName;
	private boolean isGallery;
	
	public static Intent createIntent(Context context, UUID aggregateId, String fileName, boolean isGallery) {		
		return new Intent(context, CameraActivity.class)
			.putExtra(ARG_ID, aggregateId)
			.putExtra(ARG_FILENAME, fileName)
			.putExtra(ARG_ISGALLERY, isGallery);
	}
	
	public static class CameraActivityResult {
		
		private Intent intent;

		public CameraActivityResult(Intent intent) {
			this.intent = intent;
		}
		
		public String getFileName() {
			return intent.getExtras().getString(ARG_FILENAME);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle extras = getIntent().getExtras();
		
		if (extras == null) {
			setResult(RESULT_CANCELED);
			finish();
			return;
		}
		
		try {
			this.aggregateId = (UUID) extras.get(ARG_ID);
			this.fileName = extras.getString(ARG_FILENAME);
			this.isGallery = extras.getBoolean(ARG_ISGALLERY);
		} catch (Exception e) {
			// This is implicitly dealt with in the next block
			e.printStackTrace();
		}
		
		if (aggregateId == null || fileName == null || fileName.isEmpty()) {
			setResult(RESULT_CANCELED);
			finish();
			return;
		}
		
		File directory = new File(userDataRoot, aggregateId.toString());
		
		if (!directory.exists()) {
			directory.mkdir();
		}
		
		if (isGallery) {
            Intent pickingIntent = new Intent();
            
            pickingIntent.setType("image/*");
            pickingIntent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(pickingIntent, "Complete action using"), REQUEST_PICK_PHOTO);
		} else {
    		 fileUri = Uri.fromFile(new File(directory, fileName));
    		
    		final Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    		//cameraIntent.putExtra("crop", "true");
    		
    		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
    		startActivityForResult(cameraIntent, REQUEST_TAKE_PHOTO);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode != RESULT_OK) {
		    setResult(RESULT_CANCELED);
		    finish();
		    return;
		}
		
		Intent resultIntent = new Intent().putExtra(ARG_ID, aggregateId);
        resultIntent.putExtra(ARG_FILENAME, fileName);
        
        setResult(resultCode, resultIntent);

        // File selected from gallery
        if (requestCode == REQUEST_PICK_PHOTO) {
            File galleryPicture = new File(getPath(data.getData()));

            File diaryDirectory = new File(userDataRoot, aggregateId.toString());
            
            if (!diaryDirectory.exists()) {
                diaryDirectory.mkdir();
            }

            // New file created in userDataRoot folder
            File copiedPicture = new File(diaryDirectory, this.fileName);

            try {
                // Copies gallery picture file into new file
                FileUtils.copyFile(galleryPicture, copiedPicture);
            } catch (IOException e) {
                setResult(RESULT_CANCELED);
            }
        }else if (requestCode == REQUEST_TAKE_PHOTO){
        	performCrop();
        }
		
		finish();
	}
	
    public String getPath(Uri uri) {
        String path = null;

        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null,  null);

        if (cursor != null) {
            int columnIndex = cursor.getColumnIndex(projection[0]);
            cursor.moveToFirst();
            path = cursor.getString(columnIndex);
            cursor.close();
        }

        if (path == null) {
            path = uri.getPath();
        }

        return path;
    }
	
    private void performCrop(){
    	//take care of exceptions
    	try {
    		//call the standard crop action intent (the user device may not support it)
	    	Intent cropIntent = new Intent("com.android.camera.action.CROP"); 
	    	//indicate image type and Uri
	    	cropIntent.setDataAndType( fileUri, "image/*");
	    	//set crop properties
	    	cropIntent.putExtra("crop", "true");
	    	//indicate aspect of desired crop
	    	cropIntent.putExtra("aspectX", 1);
	    	cropIntent.putExtra("aspectY", 1);
	    	//indicate output X and Y
	    	cropIntent.putExtra("outputX", 256);
	    	cropIntent.putExtra("outputY", 256);
	    	
	    	//saving the cropped image to the fileUri 
	    	cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);            
	    	cropIntent.putExtra("output", fileUri);
	    	//retrieve data on return
	    	cropIntent.putExtra("return-data", false);
	    	//start the activity - we handle returning in onActivityResult
	        startActivityForResult(cropIntent, CROP_PIC);  
    	}
    	//respond to users whose devices do not support the crop action
    	catch(ActivityNotFoundException anfe){
    		//display an error message
    		String errorMessage = "Whoops - your device doesn't support the crop action!";
    		Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
    		toast.show();
    	}
    }
}
