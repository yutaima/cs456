package edu.ohio.heartworks.android.activities;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.view.View.OnLongClickListener;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.inject.Inject;

import edu.ohio.heartworks.android.Db4oReadModel;
import edu.ohio.heartworks.android.Db4oReadModelFactory;
import edu.ohio.heartworks.android.MainModule.UserData;
import edu.ohio.heartworks.android.R;
import edu.ohio.heartworks.android.activities.CameraActivity.CameraActivityResult;
import edu.ohio.heartworks.android.models.FoodDiaryModel;
import edu.ohio.heartworks.android.share.SharePlatform;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.AddFoodPicture;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.ChangeDate;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.ChangeDescription;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.ChangeFoodType;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.CreateFoodDiary;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.DeleteFoodDiary;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.RemoveFoodPicture;
import edu.ohio.heartworks.domain.fooddiary.FoodTypes.Breakfast;
import edu.ohio.heartworks.domain.fooddiary.FoodTypes.Dinner;
import edu.ohio.heartworks.domain.fooddiary.FoodTypes.FoodType;
import edu.ohio.heartworks.domain.fooddiary.FoodTypes.Lunch;
import edu.ohio.heartworks.domain.fooddiary.FoodTypes.Snack;
import edu.ohio.heartworks.infrastructure.Command;
import edu.ohio.heartworks.infrastructure.CommandSender;

public class FoodDiaryDetailActivity extends RoboFragmentActivity implements DatePickerDialog.OnDateSetListener {

	public static final String ARG_DIARY_ID = "diary_id";
	private static final int ACTION_GET_PHOTO = 1;
	
	//private static final int CROP_FROM_CAMERA = 2;
 
	@Inject private Context context;
	@Inject private CommandSender commandBus;
	@Inject private Db4oReadModelFactory readModelFactory;
	@Inject private @UserData File userDataRoot;
	@Inject private SharePlatform sharePlatform;
	
	@InjectView(R.id.fooddiary_description) private EditText foodDescription;
	@InjectView(R.id.fooddiary_foodtype) private Spinner foodType;
	@InjectView(R.id.fooddiary_image_container) private LinearLayout imageContainer;
	@InjectView(R.id.fooddiary_image_button) private ImageButton foodImageButton;
	@InjectView(R.id.fooddiary_change_date_button) private ImageButton changeDateButton;
	@InjectView(R.id.fooddiary_browse_button) private ImageButton browseImageButton;
	private ImageView largeView;
	
	@InjectResource(R.anim.alpha) private Animation alphaAnimation;
    
    private MenuItem saveButton;
	
	private List<Command> pendingCommands;
	private FoodType[] foodTypes;
	private FoodDiaryModel diary;
		
	public static Intent createIntent(Context context, UUID foodDiaryId) {
		Intent intent = new Intent(context, FoodDiaryDetailActivity.class);
		intent.putExtra(ARG_DIARY_ID, foodDiaryId);
		
		return intent;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_fooddiary_detail);
		
        UUID diaryId = null;
        pendingCommands = new ArrayList<Command>();

		// Show the Up button in the action bar.
		if (getActionBar() != null) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
		
		Bundle extras = getIntent().getExtras();
		
		if (extras != null) {
		    diaryId = (UUID) extras.get(ARG_DIARY_ID);
		}
		
		if (diaryId == null) {
	        diaryId = UUID.randomUUID();
	        
	        CreateFoodDiary command = new CreateFoodDiary();
	        command.aggregateId = diaryId;
	        command.dateCreated = new Date();
	        
	        pendingCommands.add(command);
	        
	        diary = new FoodDiaryModel();
	        diary.id = command.aggregateId;
	        diary.dateCreated = command.dateCreated;
	        diary.version = -1;
		} else {
	        Db4oReadModel<FoodDiaryModel> readModel = readModelFactory.openSession(FoodDiaryModel.class);
	        
	        diary = readModel.findById(diaryId);
	        
	        readModel.close();
		}
		
		if (diary.imageFiles == null) diary.imageFiles = new ArrayList<String>();
		
		if (diary.description != null) foodDescription.setText(diary.description);
		
		loadImages();
		
		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		foodTypes = new FoodType[] { new Breakfast(), new Lunch(), new Dinner(), new Snack() };
		
		// The diary food type is set when re-opening a diary that has previously been saved
		if (diary.foodType == null) {
			
			int predictedIndex;
			int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			
            if (hour > 5 && hour < 10) predictedIndex = 0; // 5am - 10am Breakfast
            else if (hour > 11 && hour < 15) predictedIndex = 1; // 11am - 3pm Lunch
            else if (hour > 17 && hour < 20) predictedIndex = 2; // 5pm - 8pm Dinner
            else predictedIndex = 3; // any other time Snack
			
			//set to breakfast as default
			diary.foodType = foodTypes[predictedIndex];
			
			ChangeFoodType command = new ChangeFoodType();
			command.aggregateId = diary.id;
			command.foodType = foodTypes[predictedIndex];
			
			pendingCommands.add(command);
		}
		
		Integer selectedIndex = null;
		
		for (int i = 0; i < foodTypes.length; i++) {
			spinnerAdapter.add(foodTypes[i].toString());
			
			if (foodTypes[i].equals(diary.foodType)) {
				selectedIndex = i;
			}
		}
		
		foodType.setAdapter(spinnerAdapter);
		
		if (selectedIndex != null) {
			foodType.setSelection(selectedIndex);
		}
		
		foodType.setOnItemSelectedListener(new OnItemSelectedListener() {
			
			private boolean isInitialized = false;
			
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (!isInitialized) {
					isInitialized = true;
					return;
				}

				ChangeFoodType command = new ChangeFoodType();
				command.aggregateId = diary.id;
				command.foodType = foodTypes[position];
								
				pendingCommands.add(command);
				
				doSave(true);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
		
		foodImageButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				v.startAnimation(alphaAnimation);
				
				String fileName = UUID.randomUUID().toString() + ".jpg";
				startActivityForResult(CameraActivity.createIntent(context, diary.id, fileName, false), ACTION_GET_PHOTO);
			}
		});
		
		browseImageButton.setOnClickListener(new OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        v.startAnimation(alphaAnimation);

		        String fileName = UUID.randomUUID().toString() + ".jpg";
		        startActivityForResult(CameraActivity.createIntent(context, diary.id, fileName, true), ACTION_GET_PHOTO);
		    }
		});
               
		changeDateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				v.startAnimation(alphaAnimation);
		        
			    DialogFragment newFragment = new DatePickerFragment();
			    newFragment.show(getSupportFragmentManager(), "datePicker");
			}
		});
		
		foodDescription.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void afterTextChanged(Editable s) {
				updateSaveButton();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) { }
		});
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {         
            getMenuInflater().inflate(R.menu.activity_fooddiary_detail, menu);
            saveButton = menu.findItem(R.id.menu_fooddiary_save);
            
            updateSaveButton();
    
            return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpTo(this, new Intent(this, FoodDiaryListActivity.class));
            return true;
            
        case R.id.menu_fooddiary_save:
            doSave(false);
            return true;
            
        case R.id.menu_fooddiary_share:
            doShare();
            return true;
            
        case R.id.menu_fooddiary_delete:
            showDeleteConfirmation();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDeleteConfirmation() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to delete this entry?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        FoodDiaryDetailActivity.this.doDelete();
                    }
                }).setNegativeButton("No", null).show();
    }
    
    private void doDelete() {
        // onPause() will still be called, clear pending commands
        pendingCommands.clear();
        
        // reset food diary description (so we don't generate a ChangeDescription command... ugly)
        foodDescription.setText(diary.description);
        
        if (diary.version > -1) {
            DeleteFoodDiary command = new DeleteFoodDiary();
            command.aggregateId = diary.id;
            command.expectedVersion = diary.version;
            
            pendingCommands.add(command);
        }
        
        finish();
    }

    @Override
	protected void onPause() {
		doSave(true);
		
		invalidateOptionsMenu();

		super.onPause();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (resultCode != RESULT_OK) return;
	    
		switch (requestCode) {
		case ACTION_GET_PHOTO:
			CameraActivityResult result = new CameraActivity.CameraActivityResult(data);
			String fileName = result.getFileName();
			
			AddFoodPicture command = new AddFoodPicture();
			command.aggregateId = diary.id;
			command.fileName = fileName;
			
			pendingCommands.add(command);

			diary.imageFiles.add(fileName);
			//performCrop();
			loadImages();
			
			doSave(true);
			break;
		}
	}
	
	private boolean didTextChange() {
		String editText = foodDescription.getText().toString(); // Can not be null
		String diaryText = diary.description != null ? diary.description : ""; // Can be null
		
		if (!editText.equals(diaryText)) {
			return true;
		}
		
		return false;
	}
	
	private void updateSaveButton() {
		if (saveButton == null) return;
		
		if (didTextChange() || !pendingCommands.isEmpty()) {
	    	saveButton.setTitle(R.string.save);
	    	saveButton.setEnabled(true);	
		} else {
	    	saveButton.setTitle(R.string.saved);
	    	saveButton.setEnabled(false);	
		}
	}
	
	private void doSave(boolean isAutoSave) {
		
		if(!diary.imageFiles.isEmpty() || didTextChange()) {
		
			if (didTextChange()) {
				ChangeDescription command = new ChangeDescription();
				command.aggregateId = diary.id;
				command.description = foodDescription.getText().toString();
				
				pendingCommands.add(command);
				
				diary.description = foodDescription.getText().toString();
			}
			
			if (this.pendingCommands.isEmpty()) {
				return;
			}
			
			
			for (Command command : pendingCommands) {
				command.expectedVersion = diary.version;
				
				commandBus.send(command);
				
				diary.version++;
			}
			
			pendingCommands.clear();
			
			Toast.makeText(this, isAutoSave ? "Autosaved" : "Saved", Toast.LENGTH_SHORT).show();
			
			updateSaveButton();
		}
	}
	
    private void doShare() {
        doSave(true);
        
        sharePlatform.shareFoodDiary(this, diary);
    }

	private void loadImages() {
		imageContainer.removeAllViews();
			
		for (final String fileName : diary.imageFiles) {
			File directory = new File(userDataRoot, diary.id.toString());
			final File imageFile = new File(directory, fileName);
			
			if (!imageFile.exists()) continue;

			// Get the dimensions of the View
		    int targetW = 100; //holder.image.getWidth();
		    int targetH = 100; //holder.image.getHeight();
		    
		    // Get the dimensions of the bitmap
		    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		    bmOptions.inJustDecodeBounds = true;
		    BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);
		    int photoW = bmOptions.outWidth;
		    int photoH = bmOptions.outHeight;
		    
		    // Determine how much to scale down the image
		    int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
		    
		    // Decode the image file into a Bitmap sized to fill the View
		    bmOptions.inJustDecodeBounds = false;
		    bmOptions.inSampleSize = scaleFactor;
		    bmOptions.inPurgeable = true;
			
		    final Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);
		    /**************************************************************************************/
            // Get the dimensions of the View
        int targetW1 = 250; //holder.image.getWidth();
        int targetH1 = 250; //holder.image.getHeight();
        
        // Get the dimensions of the bitmap
        final BitmapFactory.Options bmOptions1 = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions1);
        int photoW1 = bmOptions1.outWidth;
        int photoH1 = bmOptions1.outHeight;
        
        // Determine how much to scale down the image
        int scaleFactor1 = Math.min(photoW1/targetW1, photoH1/targetH1);
        
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions1.inJustDecodeBounds = false;
        bmOptions1.inSampleSize = scaleFactor1;
        bmOptions1.inPurgeable = true;
            
        /**************************************************************************************/
		    
		    final ImageButton imageButton = new ImageButton(this);
		    imageButton.setImageBitmap(bitmap);
		    
		    int px = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
		    imageButton.setPadding(0, 0, px, 0);
		    
		    imageContainer.addView(imageButton);
		    
		    imageButton.setOnLongClickListener(new OnLongClickListener() {
		    			    	
		    			        public boolean onLongClick(final View v) {
		    			        	promptDeletion(fileName); 	
		    			        	return false;
		    			        }
		    			    });
		    
		    imageButton.setOnClickListener(new View.OnClickListener() {
                
                public void onClick(View v){
                	final Dialog enlargedPicture = new Dialog(context);
                	enlargedPicture.setContentView(R.layout.expanded_image);
                	largeView = (ImageView) enlargedPicture.findViewById(R.id.imageView1);
                	enlargedPicture.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
                	Button dismiss = (Button) enlargedPicture.findViewById(R.id.dismiss);
                	dismiss.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View arg0) {
							enlargedPicture.dismiss();
						}
					});
                	
                    Bitmap bitmap1 = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions1);
                	bitmap1 = Bitmap.createScaledBitmap(bitmap1, 700, 1000, true);
                	largeView.setImageBitmap(bitmap1);
                	
    		        enlargedPicture.show();
                    //TODO: enlarge picture for zooming	
                }
            });
		    
		}
	}
	
	public void promptDeletion(final String picID){
				
				AlertDialog.Builder deletePicture = new AlertDialog.Builder(this);
		        deletePicture.setMessage("Are you sure you want to delete this image?");
		        deletePicture.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		
		            	// Reflect the changes locally (this does nothing to actually persist the change)
		            	diary.imageFiles.remove(picID);
		            	
		            	// Send a RemoveFoodPicture command to actually persist the change
		            	RemoveFoodPicture command = new RemoveFoodPicture();
		            	command.aggregateId = diary.id;
		            	command.fileName = picID; 
		            	
		            	pendingCommands.add(command);
		            	updateSaveButton();
		            	
		            	// Reload all of the images (could be more efficient about this)
		            	loadImages();
		            	
		            }
		        });
		        deletePicture.setNegativeButton("No", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		                //Do nothing
		            }
		        });
		        
		        deletePicture.show();
			}
	
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, monthOfYear, dayOfMonth);
        
        diary.dateCreated = cal.getTime();
        
        ChangeDate command = new ChangeDate();
        command.aggregateId = diary.id;
        command.newDate = diary.dateCreated;
        
        pendingCommands.add(command);
        
        updateSaveButton();
    }
    
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        
        private boolean hasFired = false;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // Prevent callback from being invoked twice -- this is an Android SDK bug
            if (hasFired) return;
            
            ((DatePickerDialog.OnDateSetListener)getActivity()).onDateSet(view, year, monthOfYear, dayOfMonth);
            
            hasFired = true;
        }
    }
	
}