package edu.ohio.heartworks.android.activities;

import edu.ohio.heartworks.android.fragments.FoodDiaryListFragment;

public class FoodDiaryListActivity  extends FragmentContainerActivity<FoodDiaryListFragment> {

    public FoodDiaryListActivity() {
        super(new FoodDiaryListFragment());
    }
}
