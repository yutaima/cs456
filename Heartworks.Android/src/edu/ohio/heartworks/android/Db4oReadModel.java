package edu.ohio.heartworks.android;

import java.util.List;
import java.util.UUID;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.ext.ExtObjectContainer;
import com.db4o.query.Predicate;

import edu.ohio.heartworks.infrastructure.ReadModel;
import edu.ohio.heartworks.infrastructure.UniqueObject;

public class Db4oReadModel<T extends UniqueObject> implements ReadModel<T> {
	
	private Class<T> modelType;
	private ObjectContainer container;
	
	public Db4oReadModel(Class<T> modelType, ObjectContainer container) {
		this.modelType = modelType;
		this.container = container;
	}

	@Override @SuppressWarnings("serial")
	public T findById(final UUID id) {
		ObjectSet<T> objects = container.query(new Predicate<T>(modelType) {
			@Override
			public boolean match(T object) {
				return object.getId().equals(id);
			}
		});
				
		if (objects.size() == 0) {
			return null;
		}
		
		if (objects.size() == 1) {
			return objects.get(0);
		}
		
		throw new RuntimeException("Object ID is not unique. Configure a Db4o to ensure uniqueness on this field");
	}

	@Override
	public List<T> findAll() {
		return container.query(modelType);
	}

	@Override
	public Db4oReadModel<T> store(T object) {
		container.store(object);
		return this;
	}

	@Override
	public Db4oReadModel<T> deleteByID(UUID id) {
		T object = this.findById(id);
		
		container.delete(object);
		return this;
	}

	@Override
	public Db4oReadModel<T> delete(T object) {
		container.delete(object);
		
		return this;
	}

	@Override
	public Db4oReadModel<T> deleteAll() {
		ObjectSet<T> objects = container.query(modelType);
		
		for (T object : objects) {
			container.delete(object);
		}
		
		return this;
	}
	
	public T findByQuery(Predicate<T> predicate) {
		
		ObjectSet<T> objects = container.query(predicate);
		
		if (objects.size() == 0) {
			return null;
		}
		
		if (objects.size() == 1) {
			return objects.get(0);
		}
		
		throw new RuntimeException("Query returns more than one object");
	}
	
	public ObjectContainer getContainer() {
		return this.container;
	}
	
	public ExtObjectContainer getExtContainer() {
		return this.container.ext();
	}
	
	public void close() {
		this.container.close();
	}
}
