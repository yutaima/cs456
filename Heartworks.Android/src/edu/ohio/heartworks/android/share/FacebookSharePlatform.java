package edu.ohio.heartworks.android.share;

import java.io.File;
import java.io.FileNotFoundException;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;

import edu.ohio.heartworks.android.activities.FacebookLoginActivity;
import edu.ohio.heartworks.android.models.FoodDiaryModel;

public class FacebookSharePlatform implements SharePlatform {
    
    private File userDataRoot;
    
    public FacebookSharePlatform(File userDataRoot) {
        this.userDataRoot = userDataRoot;
    }
    
    @Override
    public void shareFoodDiary(final Context context, FoodDiaryModel diary) {
        Session session = Session.getActiveSession();
        
        if (session == null || !session.isOpened()) {
            context.startActivity(new Intent(context, FacebookLoginActivity.class));
        }
        
        // TODO re-authenticate, try share again on resume
        if (session == null) return;
        
        if (diary.imageFiles == null || diary.imageFiles.isEmpty()) {
            Toast.makeText(context, "Could not share, there aren't any images", Toast.LENGTH_SHORT).show();
            return;
        }
        
        /*
         * TODO upload all images. Possibly like so:
         * 
         * RequestBatch requestBatch = new RequestBatch();
         * requestBatch.add(Request.newUploadPhotoRequest(...);
         * 
         * The only problem with this is that it will generate multiple wall
         * posts for one food diary image
         */
        
        File directory = new File(userDataRoot, diary.id.toString());
        File imageFile = new File(directory, diary.imageFiles.get(0));
        
        Request request;
        
        try {
            request = Request.newUploadPhotoRequest(session, imageFile, new Request.Callback() {
                @Override
                public void onCompleted(Response response) {
                    if (response.getError() != null) {
                        Toast.makeText(context, "Error sharing food diary", Toast.LENGTH_SHORT).show();
                        
                        return;
                    }
                    
                    Toast.makeText(context, "Food diary shared", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (FileNotFoundException e) {
            Toast.makeText(context, "Error sharing diary", Toast.LENGTH_SHORT).show();
            
            return;
        }
        
        // Set the caption of the image to be the food diary description
        Bundle parameters = request.getParameters();
        parameters.putString("name", diary.description);
        
        request.setParameters(parameters);
        
        request.executeAsync(); 
    }
}
