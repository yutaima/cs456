package edu.ohio.heartworks.android.share;

import android.content.Context;
import edu.ohio.heartworks.android.models.FoodDiaryModel;

public interface SharePlatform {
    void shareFoodDiary(Context context, FoodDiaryModel foodDiary);
}