# Facebook Graph API
To compile Heartworks.Android, first import the "facebook" subfolder into Eclipse as an existing Eclipse project (not an Android project).


# Db4o OME
http://www.db4o.com/DownloadNow.aspx, install Eclipse plugin from OME subfolder

# Dual pane (tablet) / single pane (phone) 
http://i.imgur.com/gICmail5uWC.png

# Image cropping
https://github.com/lvillani/android-cropimage

# Date picker
http://developer.android.com/guide/topics/ui/controls/pickers.html#DatePicker

# CQRS 

The event model is based on the CQRS pattern.
http://martinfowler.com/bliki/CQRS.html

Only events are saved in the database, not raw diary entries.

# EventBus

http://www.slideshare.net/greenrobot/eventbus-for-android-15314813

# Test user #1

## Email address
auncfit_thurnson_1363050129@tfbnw.net

## Password
cs456