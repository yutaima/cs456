package edu.ohio.cs4560_5560.Bobcat;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SyncStateContract.Constants;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import edu.ohio.cs4560_5560.Bobcat.usage.usage;
import edu.ohio.cs4560_5560.Bobcat.diary.diaryEntry;
import edu.ohio.cs4560_5560.Bobcat.diary.diaryLoadHelper;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class DailyDiaryEntryIntroScreen extends Activity  implements SurfaceHolder.Callback, Camera.ShutterCallback, Camera.PictureCallback{
	 private static final String DEBUG_TAG = null;
	Camera mCamera;
	    SurfaceView mPreview;
	    Context context;
	    private Spinner spinner1;

	    @Override
	    public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.dailydiary);

	        //addItemsOnSpinner2();
	    	//addListenerOnButton();
	    	addListenerOnSpinnerItemSelection();
	        mPreview = (SurfaceView)findViewById(R.id.preview);
	        mPreview.getHolder().addCallback(this);
	        mPreview.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

	        mCamera = Camera.open();

	        ImageView statistics_back_button = (ImageView)findViewById(R.id.headerBackButton);

	        statistics_back_button.setOnClickListener(new OnClickListener(){

	 			@Override
	 			public void onClick(View v) {
	 				Intent launchHwMain = new Intent(DailyDiaryEntryIntroScreen.this, DailySection.class);
	 				startActivity(launchHwMain);

	 			}

	         });
	    }

	    @Override
	    public void onPause() {
	        super.onPause();
	        mCamera.stopPreview();
	    }

	    @Override
	    public void onDestroy() {
	        super.onDestroy();
	        mCamera.release();
	        Log.d("CAMERA","Destroy");
	    }

	    public void onCancelClick(View v) {
	        finish();
	    }

	    public void onSnapClick(View v) {
	        mCamera.takePicture(this, null, null, this);
	    }

	    @Override
	    public void onShutter() {
	        Toast.makeText(this, "Click!", Toast.LENGTH_SHORT).show();
	    }

	    @Override
	    public void onPictureTaken(byte[] data, Camera camera) {

	        File pictureFileDir = getDir();
	    	context =  getApplicationContext();

	        if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {

	          Log.d(DEBUG_TAG, "Can't create directory to save image.");
	          Toast.makeText(context, "Can't create directory to save image.",
	              Toast.LENGTH_LONG).show();
	          return;

	        }

	    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
	        String date = dateFormat.format(new Date());
	        String photoFile = "Picture_" + date + ".jpg";

	       String filename = getDir().toString() + "/"+ photoFile;
	       // String filename = pictureFileDir.getPath() + photoFile;

	        File pictureFile = new File(filename);

	        try {
	          FileOutputStream fos = new FileOutputStream(pictureFile);
	          fos.write(data);
	          fos.close();
	          Toast.makeText(context, "New Image saved:" + photoFile,
	              Toast.LENGTH_LONG).show();
	          		HeartWorksMobileAppActivity hWorks = new HeartWorksMobileAppActivity ();
	          				hWorks.getNotificationManager().cancel(1);
	          				hWorks.setNutrition(false);
	        } catch (Exception error) {
	          Log.d(DEBUG_TAG, "File" + filename + "not saved: "
	              + error.getMessage());
	          Toast.makeText(context, "Image could not be saved.",
	              Toast.LENGTH_LONG).show();
	        }

	    }

	    private File getDir() {
	        File sdDir = Environment
	          .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
	        return new File(sdDir, "foodDiary");
	      }

	    @Override
	    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	        Camera.Parameters params = mCamera.getParameters();
	        List<Camera.Size> sizes = params.getSupportedPreviewSizes();
	        Camera.Size selected = sizes.get(0);
	        params.setPreviewSize(selected.width,selected.height);
	        mCamera.setParameters(params);

	        mCamera.setDisplayOrientation(0);
	        mCamera.startPreview();
	    }

	    @Override
	    public void surfaceCreated(SurfaceHolder holder) {
	        try {
	            mCamera.setPreviewDisplay(mPreview.getHolder());
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	    @Override
	    public void surfaceDestroyed(SurfaceHolder holder) {
	        Log.i("PREVIEW","surfaceDestroyed");
	    }


	    public void addListenerOnSpinnerItemSelection() {
	    	spinner1 = (Spinner) findViewById(R.id.spinner1);
	    	//spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
	      }

	    public void onFeedbackClick(View v) {
	    	Intent feedback = new Intent(DailyDiaryEntryIntroScreen.this, FoodDiaryFeedback.class);
			startActivity(feedback);
	    }

}



