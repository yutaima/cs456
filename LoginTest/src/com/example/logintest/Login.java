package com.example.logintest;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class Login extends Activity {
	
	TextView tv;
	EditText et;
	Button btn;
	CheckBox cb;
	boolean rememberMe;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		tv = (TextView) findViewById(R.id.textView1);
		et = (EditText) findViewById(R.id.editText1);
		btn = (Button) findViewById(R.id.button1);
		cb = (CheckBox) findViewById(R.id.checkBox1);
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String s = et.getText().toString();
				tv.setText(s);
				
			    rememberMe = cb.isChecked();
			    System.out.println(rememberMe);

			}
		});
	}
}
