package edu.ohio.heartworks.domain.lesson;

import java.util.List;

import edu.ohio.heartworks.infrastructure.Event;

public class LessonEvents {
	
	public static class LessonCreated extends Event {
		public String name;
		public String htmlContent;
		public List<String> files;
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((files == null) ? 0 : files.hashCode());
			result = prime * result
					+ ((htmlContent == null) ? 0 : htmlContent.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			LessonCreated other = (LessonCreated) obj;
			if (files == null) {
				if (other.files != null)
					return false;
			} else if (!files.equals(other.files))
				return false;
			if (htmlContent == null) {
				if (other.htmlContent != null)
					return false;
			} else if (!htmlContent.equals(other.htmlContent))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}
	}
}
