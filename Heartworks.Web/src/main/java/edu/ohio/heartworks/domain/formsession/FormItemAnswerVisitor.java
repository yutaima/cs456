package edu.ohio.heartworks.domain.formsession;

public interface FormItemAnswerVisitor {
	void visit(ParagraphQuestionAnswer paragraphQuestionAnswer);
	void visit(RangedQuestionAnswer rangedQuestionAnswer);
}
