package edu.ohio.heartworks.domain.fooddiary;

import java.util.Date;
import java.util.UUID;

import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.DateChanged;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.DescriptionChanged;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodDiaryCreated;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodDiaryDeleted;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodPictureAdded;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodPictureRemoved;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodTypeChanged;
import edu.ohio.heartworks.domain.fooddiary.FoodTypes.FoodType;
import edu.ohio.heartworks.infrastructure.AggregateRoot;

public class FoodDiary extends AggregateRoot {
	public FoodDiary() { }
	
	public FoodDiary(UUID id, Date dateCreated) {
		applyChange(new FoodDiaryCreated(id, dateCreated));
	}
	
	public void apply(FoodDiaryCreated event) {
		this.aggregateId = event.aggregateId;
	}
	
	public void changeDescription(String description) {
		
		// Make any necessary data validations here. 
		// For example, make sure it's not blank
		
		applyChange(new DescriptionChanged(this.aggregateId, description));
	}
	
	public void apply(DescriptionChanged event) {
		
	}
	
	public void changeFoodType(FoodType foodType) {
		applyChange(new FoodTypeChanged(this.aggregateId, foodType));
	}
	
	public void apply(FoodTypeChanged event) {
		
	}
	
	public void addImage(String fileName) {
		FoodPictureAdded event = new FoodPictureAdded();
		event.fileName = fileName;
		
		applyChange(event);
	}
	
	public void apply(FoodPictureAdded event) {
		
	}
	
	public void removeImage(String fileName) {
		FoodPictureRemoved event = new FoodPictureRemoved();
		event.fileName = fileName;
		
		applyChange(event);
	}
	
	public void apply(FoodPictureRemoved event) {
		
	}
	
	public void delete() {
		applyChange(new FoodDiaryDeleted());
	}
	
	public void apply(FoodDiaryDeleted event) {
		
	}

	public void changeDate(Date d) {
		DateChanged event = new DateChanged();
		event.newDate = d;
		
		applyChange(event);
	}
	
	public void apply(DateChanged event) {
		
	}
}
