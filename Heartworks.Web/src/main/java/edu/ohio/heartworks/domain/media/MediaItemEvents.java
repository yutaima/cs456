package edu.ohio.heartworks.domain.media;

import edu.ohio.heartworks.domain.media.MediaTypes.MediaType;
import edu.ohio.heartworks.infrastructure.Event;

public class MediaItemEvents {
	
	public static class MediaItemCreated extends Event {
		public String name;
		public String fileName;
		public MediaType mediaType;
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((fileName == null) ? 0 : fileName.hashCode());
			result = prime * result
					+ ((mediaType == null) ? 0 : mediaType.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			MediaItemCreated other = (MediaItemCreated) obj;
			if (fileName == null) {
				if (other.fileName != null)
					return false;
			} else if (!fileName.equals(other.fileName))
				return false;
			if (mediaType == null) {
				if (other.mediaType != null)
					return false;
			} else if (!mediaType.equals(other.mediaType))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}
	}
}
