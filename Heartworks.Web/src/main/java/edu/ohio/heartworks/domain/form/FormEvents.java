package edu.ohio.heartworks.domain.form;

import java.util.List;
import java.util.UUID;

import edu.ohio.heartworks.infrastructure.Event;

public class FormEvents {
	
	public static class FormCreated extends Event {

		public String name;
		public List<FormItem> items;
		
		public FormCreated() { }

		public FormCreated(UUID aggregateId, String name, List<FormItem> items) {
			super(aggregateId);
			
			this.name = name;
			this.items = items;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((items == null) ? 0 : items.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FormCreated other = (FormCreated) obj;
			if (items == null) {
				if (other.items != null)
					return false;
			} else if (!items.equals(other.items))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}
	}
	
	public static class FormAssigned extends Event {

		public String cronExpression;
		
		public FormAssigned() { }

		public FormAssigned(UUID aggregateId, String cronExpression) {
			super(aggregateId);
			
			this.cronExpression = cronExpression;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime
					* result
					+ ((cronExpression == null) ? 0 : cronExpression.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FormAssigned other = (FormAssigned) obj;
			if (cronExpression == null) {
				if (other.cronExpression != null)
					return false;
			} else if (!cronExpression.equals(other.cronExpression))
				return false;
			return true;
		}
	}
}
