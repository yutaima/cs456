package edu.ohio.heartworks.domain.media;

public class MediaTypes {

	public static abstract class MediaType {
	}
	
	public static class Audio extends MediaType {

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			return true;
		}
	}
	
	public static class Video extends MediaType {

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			return true;
		}
	}
}
