package edu.ohio.heartworks.domain.formsession;

import java.util.UUID;

public abstract class FormItemAnswer {
	public UUID questionId;
	
	public FormItemAnswer() { }
	
	public FormItemAnswer(UUID id) {
		this.questionId = id;
	}
	
	public abstract void accept(FormItemAnswerVisitor visitor);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((questionId == null) ? 0 : questionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormItemAnswer other = (FormItemAnswer) obj;
		if (questionId == null) {
			if (other.questionId != null)
				return false;
		} else if (!questionId.equals(other.questionId))
			return false;
		return true;
	}
}
