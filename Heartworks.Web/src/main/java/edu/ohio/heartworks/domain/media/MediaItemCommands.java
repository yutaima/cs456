package edu.ohio.heartworks.domain.media;

import edu.ohio.heartworks.domain.media.MediaTypes.MediaType;
import edu.ohio.heartworks.infrastructure.Command;

public class MediaItemCommands {

	public static class CreateMediaItem extends Command {
		public String name;
		public String fileName;
		public String fileExtension;
		public MediaType mediaType;
	}
	
}
