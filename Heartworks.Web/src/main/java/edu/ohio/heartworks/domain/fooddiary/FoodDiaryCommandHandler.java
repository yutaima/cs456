package edu.ohio.heartworks.domain.fooddiary;

import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommands.*;
import edu.ohio.heartworks.infrastructure.CommandHandler;
import edu.ohio.heartworks.infrastructure.Handler;
import edu.ohio.heartworks.infrastructure.Repository;
import edu.ohio.heartworks.infrastructure.RepositoryFactory;

public class FoodDiaryCommandHandler implements CommandHandler {

    protected final Repository<FoodDiary> repository;

    public FoodDiaryCommandHandler(RepositoryFactory factory) {
        this.repository = factory.getRepository(FoodDiary.class);
    }

    @Handler
    public void handle(CreateFoodDiary command) {
        FoodDiary diary = new FoodDiary(command.aggregateId,
                command.dateCreated);

        repository.save(diary, -1);
    }

    @Handler
    public void handle(ChangeDescription command) {
        FoodDiary diary = repository.getById(command.aggregateId);

        diary.changeDescription(command.description);

        repository.save(diary, command.expectedVersion);
    }

    @Handler
    public void handle(AddFoodPicture command) {
        FoodDiary diary = repository.getById(command.aggregateId);

        diary.addImage(command.fileName);

        repository.save(diary, command.expectedVersion);
    }

    @Handler
    public void handle(ChangeFoodType command) {
        FoodDiary diary = repository.getById(command.aggregateId);

        diary.changeFoodType(command.foodType);

        repository.save(diary, command.expectedVersion);
    }

    @Handler
    public void handle(RemoveFoodPicture command) {
        FoodDiary diary = repository.getById(command.aggregateId);

        diary.removeImage(command.fileName);

        repository.save(diary, command.expectedVersion);
    }
    
    @Handler
    public void handle(ChangeDate command) {
        FoodDiary diary = repository.getById(command.aggregateId);

        diary.changeDate(command.newDate);

        repository.save(diary, command.expectedVersion);
    }

    @Handler
    public void handle(DeleteFoodDiary command) {
        FoodDiary diary = repository.getById(command.aggregateId);

        diary.delete();

        repository.save(diary, command.expectedVersion);
    }
}
