package edu.ohio.heartworks.domain.media;

import edu.ohio.heartworks.domain.media.MediaItemCommands.CreateMediaItem;
import edu.ohio.heartworks.infrastructure.CommandHandler;
import edu.ohio.heartworks.infrastructure.Handler;
import edu.ohio.heartworks.infrastructure.Repository;
import edu.ohio.heartworks.infrastructure.RepositoryFactory;

public class MediaItemCommandHandler implements CommandHandler {
	
	protected final Repository<MediaItem> repository;
	
	public MediaItemCommandHandler(RepositoryFactory factory) {
		this.repository = factory.getRepository(MediaItem.class);
	}
	
	@Handler
	public void handle(CreateMediaItem command) {
		MediaItem item = new MediaItem(command.aggregateId, command.name, command.fileName, command.mediaType);
		
		repository.save(item, -1);
	}
}
