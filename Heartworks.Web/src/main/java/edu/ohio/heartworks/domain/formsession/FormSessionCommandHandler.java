package edu.ohio.heartworks.domain.formsession;

import edu.ohio.heartworks.domain.formsession.FormSessionCommands.*;
import edu.ohio.heartworks.infrastructure.CommandHandler;
import edu.ohio.heartworks.infrastructure.Handler;
import edu.ohio.heartworks.infrastructure.Repository;
import edu.ohio.heartworks.infrastructure.RepositoryFactory;

public class FormSessionCommandHandler implements CommandHandler {
	
	protected final Repository<FormSession> repository;

	public FormSessionCommandHandler(RepositoryFactory factory) {
		this.repository = factory.getRepository(FormSession.class);
	}

	@Handler
	public void handle(StartFormSession command) {
		FormSession doc = new FormSession(command.aggregateId, command.formId, command.answers);
		
		repository.save(doc, -1);
	}
	
	@Handler
	public void handle(SubmitFormSessionAnswer command) {
		FormSession doc = repository.getById(command.aggregateId);
		doc.submitAnswer(command.answer);
		
		repository.save(doc, command.expectedVersion);
	}
}