package edu.ohio.heartworks.domain.lesson;

import java.util.List;
import java.util.UUID;

import edu.ohio.heartworks.domain.lesson.LessonEvents.LessonCreated;
import edu.ohio.heartworks.infrastructure.AggregateRoot;

public class Lesson extends AggregateRoot {
	
	public Lesson() { }
	
	public Lesson(UUID id, String name, String htmlContent, List<String> files) {
		LessonCreated event = new LessonCreated();
		event.aggregateId = id;
		event.name = name;
		event.htmlContent = htmlContent;
		event.files = files;
		
		applyChange(event);
	}
	
	public void apply(LessonCreated event) {
		this.aggregateId = event.aggregateId;
	}
}
