package edu.ohio.heartworks.domain.fooddiary;

import java.util.Date;
import java.util.UUID;

import edu.ohio.heartworks.domain.fooddiary.FoodTypes.FoodType;
import edu.ohio.heartworks.infrastructure.Event;

public class FoodDiaryEvents {
	
	public static class FoodDiaryCreated extends Event {

		public Date dateCreated;
		
		public FoodDiaryCreated() { }

		public FoodDiaryCreated(UUID aggregateId, Date dateCreated) {
			super(aggregateId);
			
			this.dateCreated = dateCreated;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((dateCreated == null) ? 0 : dateCreated.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FoodDiaryCreated other = (FoodDiaryCreated) obj;
			if (dateCreated == null) {
				if (other.dateCreated != null)
					return false;
			} else if (!dateCreated.equals(other.dateCreated))
				return false;
			return true;
		}
	}
	
	public static class DescriptionChanged extends Event {

		public String description;
		
		public DescriptionChanged() { }

		public DescriptionChanged(UUID aggregateId, String description) {
			super(aggregateId);
			
			this.description = description;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((description == null) ? 0 : description.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DescriptionChanged other = (DescriptionChanged) obj;
			if (description == null) {
				if (other.description != null)
					return false;
			} else if (!description.equals(other.description))
				return false;
			return true;
		}
	}
	
	public static class FoodPictureAdded extends Event {

		public String fileName;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((fileName == null) ? 0 : fileName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FoodPictureAdded other = (FoodPictureAdded) obj;
			if (fileName == null) {
				if (other.fileName != null)
					return false;
			} else if (!fileName.equals(other.fileName))
				return false;
			return true;
		}
	}
	
	public static class FoodTypeChanged extends Event {
		
		public FoodType foodType;

		public FoodTypeChanged() { }

		public FoodTypeChanged(UUID aggregateId, FoodType foodType) {
			super(aggregateId);
			
			this.foodType = foodType;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((foodType == null) ? 0 : foodType.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FoodTypeChanged other = (FoodTypeChanged) obj;
			if (foodType == null) {
				if (other.foodType != null)
					return false;
			} else if (!foodType.equals(other.foodType))
				return false;
			return true;
		}
	}
	
	public static class FoodPictureRemoved extends Event {
		public String fileName;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((fileName == null) ? 0 : fileName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FoodPictureRemoved other = (FoodPictureRemoved) obj;
			if (fileName == null) {
				if (other.fileName != null)
					return false;
			} else if (!fileName.equals(other.fileName))
				return false;
			return true;
		}
	}
	
	public static class FoodDiaryDeleted extends Event {
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			return true;
		}
	}

	public static class DateChanged extends Event {
		public Date newDate;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((newDate == null) ? 0 : newDate.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DateChanged other = (DateChanged) obj;
			if (newDate == null) {
				if (other.newDate != null)
					return false;
			} else if (!newDate.equals(other.newDate))
				return false;
			return true;
		}
	}
}
