package edu.ohio.heartworks.domain.media;

import java.util.UUID;

import edu.ohio.heartworks.domain.media.MediaItemEvents.MediaItemCreated;
import edu.ohio.heartworks.domain.media.MediaTypes.MediaType;
import edu.ohio.heartworks.infrastructure.AggregateRoot;

public class MediaItem extends AggregateRoot {
	
	public MediaItem() { }
	
	public MediaItem(UUID id, String name, String fileName, MediaType mediaType) {
		MediaItemCreated event = new MediaItemCreated();
		event.aggregateId = id;
		event.name = name;
		event.fileName = fileName;
		event.mediaType = mediaType;
		
		applyChange(event);
	}
	
	public void apply(MediaItemCreated event) {
		this.aggregateId = event.aggregateId;
	}
}
