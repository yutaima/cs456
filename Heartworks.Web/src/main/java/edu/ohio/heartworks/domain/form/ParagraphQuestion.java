package edu.ohio.heartworks.domain.form;

import java.util.UUID;

public class ParagraphQuestion extends FormItem {
	private String questionText;
	
	public ParagraphQuestion() { }

	public ParagraphQuestion(UUID id, String questionText) {
		super(id);
		
		this.questionText = questionText;
	}
	
	@Override
	public void accept(FormItemVisitor visitor) {
		visitor.visit(this);		
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((questionText == null) ? 0 : questionText.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParagraphQuestion other = (ParagraphQuestion) obj;
		if (questionText == null) {
			if (other.questionText != null)
				return false;
		} else if (!questionText.equals(other.questionText))
			return false;
		return true;
	}
}