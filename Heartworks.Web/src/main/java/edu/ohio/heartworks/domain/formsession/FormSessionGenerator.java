package edu.ohio.heartworks.domain.formsession;

import java.util.ArrayList;
import java.util.List;

import edu.ohio.heartworks.domain.form.FormItem;
import edu.ohio.heartworks.domain.form.FormItemVisitor;
import edu.ohio.heartworks.domain.form.ParagraphQuestion;
import edu.ohio.heartworks.domain.form.RangedQuestion;
public class FormSessionGenerator implements FormItemVisitor {
	
	private final List<FormItem> items;
	private final List<FormItemAnswer> answers;
	
	public FormSessionGenerator(List<FormItem> items) {
		this.items = items;
		this.answers = new ArrayList<FormItemAnswer>();
	}
	
	public List<FormItemAnswer> generate() {
		for (FormItem item : items) {
			item.accept(this);
		}
		
		return answers;
	}

	@Override
	public void visit(ParagraphQuestion question) {
		answers.add(new ParagraphQuestionAnswer(question));
	}

	@Override
	public void visit(RangedQuestion question) {
		answers.add(new RangedQuestionAnswer(question));
	}
}
