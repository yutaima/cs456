package edu.ohio.heartworks.domain.fooddiary;

import java.util.Date;

import edu.ohio.heartworks.domain.fooddiary.FoodTypes.FoodType;
import edu.ohio.heartworks.infrastructure.Command;

public class FoodDiaryCommands {
	
	public static class CreateFoodDiary extends Command {
		public Date dateCreated;
	}

	public static class ChangeDescription extends Command {
		public String description;
	}
	
	public static class AddFoodPicture extends Command {
		public String fileName;
	}
	
	public static class ChangeFoodType extends Command {
		public FoodType foodType;
	}
	
	public static class ChangeDate extends Command {
		public Date newDate;
	}
	
	public static class RemoveFoodPicture extends Command {
		public String fileName;
	}
	
	public static class DeleteFoodDiary extends Command {
	}
}
