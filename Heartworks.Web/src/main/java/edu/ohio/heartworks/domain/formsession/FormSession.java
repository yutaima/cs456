package edu.ohio.heartworks.domain.formsession;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import edu.ohio.heartworks.domain.formsession.FormSessionEvents.*;
import edu.ohio.heartworks.infrastructure.AggregateRoot;

public class FormSession extends AggregateRoot {
	
	private List<FormItemAnswer> answers;
	private UUID formId;

	public FormSession() { }
	
	public FormSession(UUID id, UUID formId, List<FormItemAnswer> answers) {
		FormSessionStarted event = new FormSessionStarted();
		event.aggregateId = id;
		event.formId = formId;
		event.answers = answers;
		event.dateStarted = new Date();
		
		applyChange(event);
	}
	
	public void apply(FormSessionStarted event) {
		this.aggregateId = event.aggregateId;
		this.formId = event.formId;
		this.answers = event.answers;
	}
	
	private class TypeChecker implements FormItemAnswerVisitor {
		
		private final FormItemAnswer checkedType;
		private boolean isSameType;

		public TypeChecker(FormItemAnswer checkedType) {
			this.checkedType = checkedType;
			this.isSameType = false;
		}
		
		public boolean isSameType() {
			return isSameType;
		}
		
		@Override
		public void visit(ParagraphQuestionAnswer paragraphQuestionAnswer) {
			if (ParagraphQuestionAnswer.class.isAssignableFrom(checkedType.getClass())) {
				this.isSameType = true;
			}
		}

		@Override
		public void visit(RangedQuestionAnswer rangedQuestionAnswer) {
			if (RangedQuestionAnswer.class.isAssignableFrom(checkedType.getClass())) {
				this.isSameType = true;
			}
		}
	}
	
	// Have a separate kind for every type?
	public void submitAnswer(final FormItemAnswer answer) {
		for (FormItemAnswer savedAnswer : answers) {
			if (savedAnswer.questionId.equals(answer.questionId)) {
				TypeChecker checker = new TypeChecker(answer);
				answer.accept(checker);
				
				if (!checker.isSameType()) {
					// TODO: throw exception
					return;
				}
				break;
			}
 		}
		
		applyChange(new FormSessionAnswerSubmitted(aggregateId, formId, answer));
	}
	
	public void apply(FormSessionAnswerSubmitted event) {
		// Do nothing, actual answer not needed for later validation.
	}
}

