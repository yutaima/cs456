package edu.ohio.heartworks.domain.formsession;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import edu.ohio.heartworks.infrastructure.Event;

public class FormSessionEvents {
	public static class FormSessionStarted extends Event {

		public UUID formId;
		public List<FormItemAnswer> answers;
		public Date dateStarted;
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((answers == null) ? 0 : answers.hashCode());
			result = prime * result
					+ ((dateStarted == null) ? 0 : dateStarted.hashCode());
			result = prime * result
					+ ((formId == null) ? 0 : formId.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FormSessionStarted other = (FormSessionStarted) obj;
			if (answers == null) {
				if (other.answers != null)
					return false;
			} else if (!answers.equals(other.answers))
				return false;
			if (dateStarted == null) {
				if (other.dateStarted != null)
					return false;
			} else if (!dateStarted.equals(other.dateStarted))
				return false;
			if (formId == null) {
				if (other.formId != null)
					return false;
			} else if (!formId.equals(other.formId))
				return false;
			return true;
		}
	}

	public static class FormSessionAnswerSubmitted extends Event {

		public UUID formId;
		public FormItemAnswer answer;
		
		public FormSessionAnswerSubmitted() { }

		public FormSessionAnswerSubmitted(UUID aggregateId, UUID formId, FormItemAnswer answer) {
			super(aggregateId);
			
			this.formId = formId;
			this.answer = answer;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((answer == null) ? 0 : answer.hashCode());
			result = prime * result
					+ ((formId == null) ? 0 : formId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FormSessionAnswerSubmitted other = (FormSessionAnswerSubmitted) obj;
			if (answer == null) {
				if (other.answer != null)
					return false;
			} else if (!answer.equals(other.answer))
				return false;
			if (formId == null) {
				if (other.formId != null)
					return false;
			} else if (!formId.equals(other.formId))
				return false;
			return true;
		}
	}
}
