package edu.ohio.heartworks.domain.form;

import java.util.List;
import java.util.UUID;

import edu.ohio.heartworks.domain.form.FormEvents.*;
import edu.ohio.heartworks.infrastructure.AggregateRoot;

public class Form extends AggregateRoot {
	
	public Form() { }
	
	public Form(UUID id, String name, List<FormItem> items) {
		applyChange(new FormCreated(id, name, items));
	}
	
	public void apply(FormCreated event) {
		this.aggregateId = event.aggregateId;
	}
	
	public void assign(String cronExpression) {
		// TODO: validate expression
		
		applyChange(new FormAssigned(this.aggregateId, cronExpression));
	}
	
	public void apply(FormAssigned event) {
		// Do nothing for now
	}
}
