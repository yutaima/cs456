package edu.ohio.heartworks.domain.formsession;

import java.util.List;
import java.util.UUID;

import edu.ohio.heartworks.infrastructure.Command;

public class FormSessionCommands {
	public static class StartFormSession extends Command {
		public UUID formId;
		public List<FormItemAnswer> answers;
	}
	
	public static class SubmitFormSessionAnswer extends Command {
		public FormItemAnswer answer;
	}
}
