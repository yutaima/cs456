package edu.ohio.heartworks.domain.lesson;

import java.util.List;

import edu.ohio.heartworks.infrastructure.Command;

public class LessonCommands {
	
	public static class CreateLesson extends Command {
		public String lessonName;
		public String htmlContent;
		public List<String> files;
	}
}
