package edu.ohio.heartworks.domain.form;

import java.util.UUID;

public abstract class FormItem {
	public UUID id;
	
	public FormItem() { }
	
	public FormItem(UUID id) {
		this.id = id;
	}
	
	public abstract void accept(FormItemVisitor visitor);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormItem other = (FormItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
