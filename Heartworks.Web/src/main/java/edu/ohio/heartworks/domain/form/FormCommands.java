package edu.ohio.heartworks.domain.form;

import java.util.List;

import edu.ohio.heartworks.infrastructure.Command;

public class FormCommands {

	public static class CreateForm extends Command {
		public String name;
		public List<FormItem> items;
	}

	public static class AssignForm extends Command {
		public String cronExpression;
	}

}
