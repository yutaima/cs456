package edu.ohio.heartworks.domain.form;


public interface FormItemVisitor {
	void visit(ParagraphQuestion question);
	void visit(RangedQuestion question);
}