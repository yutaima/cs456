package edu.ohio.heartworks.domain.formsession;

import edu.ohio.heartworks.domain.form.RangedQuestion;

public class RangedQuestionAnswer extends FormItemAnswer {
	public RangedQuestion question;
	public int answerIndex;
	
	public RangedQuestionAnswer() { }
	
	public RangedQuestionAnswer(RangedQuestion question) {
		super(question.id);
		this.question = question;
	}

	@Override
	public void accept(FormItemAnswerVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + answerIndex;
		result = prime * result
				+ ((question == null) ? 0 : question.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RangedQuestionAnswer other = (RangedQuestionAnswer) obj;
		if (answerIndex != other.answerIndex)
			return false;
		if (question == null) {
			if (other.question != null)
				return false;
		} else if (!question.equals(other.question))
			return false;
		return true;
	}
}
