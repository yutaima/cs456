package edu.ohio.heartworks.domain.fooddiary;

public class FoodTypes {
	public static abstract class FoodType {
		@Override
		public abstract String toString();
	}
	
	public static class Breakfast extends FoodType {

		private final String stringName = "Breakfast";
		
		@Override
		public String toString() {
			return stringName;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((stringName == null) ? 0 : stringName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Breakfast other = (Breakfast) obj;
			if (stringName == null) {
				if (other.stringName != null)
					return false;
			} else if (!stringName.equals(other.stringName))
				return false;
			return true;
		}
	}
	
	public static class Lunch extends FoodType {

		private final String stringName = "Lunch";
		
		@Override
		public String toString() {
			return stringName;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((stringName == null) ? 0 : stringName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Lunch other = (Lunch) obj;
			if (stringName == null) {
				if (other.stringName != null)
					return false;
			} else if (!stringName.equals(other.stringName))
				return false;
			return true;
		}
	}
	
	public static class Dinner extends FoodType {

		private final String stringName = "Dinner";
		
		@Override
		public String toString() {
			return stringName;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((stringName == null) ? 0 : stringName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Dinner other = (Dinner) obj;
			if (stringName == null) {
				if (other.stringName != null)
					return false;
			} else if (!stringName.equals(other.stringName))
				return false;
			return true;
		}
	}
	
	public static class Snack extends FoodType {

		private final String stringName = "Snack";
		
		@Override
		public String toString() {
			return stringName;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((stringName == null) ? 0 : stringName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Snack other = (Snack) obj;
			if (stringName == null) {
				if (other.stringName != null)
					return false;
			} else if (!stringName.equals(other.stringName))
				return false;
			return true;
		}
	}
}
