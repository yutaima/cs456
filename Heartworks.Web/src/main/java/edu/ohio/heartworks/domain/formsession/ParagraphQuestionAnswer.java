package edu.ohio.heartworks.domain.formsession;

import edu.ohio.heartworks.domain.form.ParagraphQuestion;

public class ParagraphQuestionAnswer extends FormItemAnswer {
	
	public String answerText;
	public ParagraphQuestion question;
	
	public ParagraphQuestionAnswer() { }

	public ParagraphQuestionAnswer(ParagraphQuestion question) {
		super(question.id);
		
		this.question = question;
	}

	@Override
	public void accept(FormItemAnswerVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((answerText == null) ? 0 : answerText.hashCode());
		result = prime * result
				+ ((question == null) ? 0 : question.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParagraphQuestionAnswer other = (ParagraphQuestionAnswer) obj;
		if (answerText == null) {
			if (other.answerText != null)
				return false;
		} else if (!answerText.equals(other.answerText))
			return false;
		if (question == null) {
			if (other.question != null)
				return false;
		} else if (!question.equals(other.question))
			return false;
		return true;
	}
}
