package edu.ohio.heartworks.domain.lesson;

import edu.ohio.heartworks.domain.lesson.LessonCommands.CreateLesson;
import edu.ohio.heartworks.infrastructure.CommandHandler;
import edu.ohio.heartworks.infrastructure.Handler;
import edu.ohio.heartworks.infrastructure.Repository;
import edu.ohio.heartworks.infrastructure.RepositoryFactory;

public class LessonCommandHandler implements CommandHandler {
	
	public final Repository<Lesson> repository;
	
	public LessonCommandHandler(RepositoryFactory factory) {
		this.repository = factory.getRepository(Lesson.class);
	}
	
	@Handler
	public void handle(CreateLesson command) {
		Lesson lesson = new Lesson(command.aggregateId, command.lessonName, command.htmlContent, command.files);
		
		repository.save(lesson, -1);
	}
}