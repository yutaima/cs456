package edu.ohio.heartworks.domain.form;

import java.util.List;
import java.util.UUID;

public class RangedQuestion extends FormItem {
	public String questionText;
	public List<String> answerList;
	
	public RangedQuestion() { }

	public RangedQuestion(UUID id, String questionText, List<String> answerList) {
		super(id);
		
		this.questionText = questionText;
		this.answerList = answerList;
	}
	
	@Override
	public void accept(FormItemVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((answerList == null) ? 0 : answerList.hashCode());
		result = prime * result
				+ ((questionText == null) ? 0 : questionText.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RangedQuestion other = (RangedQuestion) obj;
		if (answerList == null) {
			if (other.answerList != null)
				return false;
		} else if (!answerList.equals(other.answerList))
			return false;
		if (questionText == null) {
			if (other.questionText != null)
				return false;
		} else if (!questionText.equals(other.questionText))
			return false;
		return true;
	}
}
