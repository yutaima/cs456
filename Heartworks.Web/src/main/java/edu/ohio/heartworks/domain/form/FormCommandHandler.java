package edu.ohio.heartworks.domain.form;

import edu.ohio.heartworks.domain.form.FormCommands.*;
import edu.ohio.heartworks.infrastructure.*;

public class FormCommandHandler implements CommandHandler {
	
	private final Repository<Form> repository;
	
	public FormCommandHandler(RepositoryFactory factory) {		
		this.repository = factory.getRepository(Form.class);
	}

	@Handler
	public void handle(CreateForm command) {
		Form doc = new Form(command.aggregateId, command.name, command.items);
		
		repository.save(doc, -1);
	}
	
	@Handler
	public void handle(AssignForm command) {
		Form doc = repository.getById(command.aggregateId);
		doc.assign(command.cronExpression);
		
		repository.save(doc, doc.getVersion());
	}
}