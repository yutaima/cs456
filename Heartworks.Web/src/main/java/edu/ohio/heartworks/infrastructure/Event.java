package edu.ohio.heartworks.infrastructure;

import java.util.UUID;

public abstract class Event implements Message {
	public UUID aggregateId;
	public int version;
	
	public Event() { }
	
    public Event(UUID aggregateId) {
    	this.aggregateId = aggregateId;
	}
}