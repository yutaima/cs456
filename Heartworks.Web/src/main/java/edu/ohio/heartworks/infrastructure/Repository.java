package edu.ohio.heartworks.infrastructure;

import java.util.UUID;

public interface Repository<T extends AggregateRoot> {
	public T getById(UUID id);

    public T getById(UUID id, int version);
	
	public void save(T aggregate, int expectedVersion) throws ConcurrencyException;
}
