package edu.ohio.heartworks.infrastructure;

import java.util.List;
import java.util.UUID;

public interface ReadModel<T extends UniqueObject> {
	T findById(final UUID id);
	List<T> findAll();
	
	// Return self to allow method chaining
	ReadModel<T> store(final T object);
	ReadModel<T> deleteByID(final UUID id);
	ReadModel<T> delete(T object);
	ReadModel<T> deleteAll();
}