package edu.ohio.heartworks.infrastructure;

import java.util.UUID;

public interface UniqueObject {
    UUID getId();
}