package edu.ohio.heartworks.infrastructure;

public abstract class ReadModelFactory {
	public abstract <T extends UniqueObject> ReadModel<T> getReadModel(Class<T> modelType);
	public void commit() { }
}