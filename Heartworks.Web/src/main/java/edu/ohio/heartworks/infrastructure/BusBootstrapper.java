package edu.ohio.heartworks.infrastructure;

import java.lang.reflect.Method;
import java.util.Set;

public class BusBootstrapper {
	
	private final Set<CommandHandler> commandHandlers;
	private final Set<EventHandler> eventHandlers;
	private final CommandSender commandBus;
	private final EventPublisher eventBus;

	public BusBootstrapper(
			Set<CommandHandler> commandHandlers,
			Set<EventHandler> eventHandlers,
			CommandSender commandBus,
			EventPublisher eventBus) {

		this.commandHandlers = commandHandlers;
		this.eventHandlers = eventHandlers;
		this.commandBus = commandBus;
		this.eventBus = eventBus;
	}
	
	public void Bootstrap() {
		registerHandlers(commandHandlers, commandBus);
		registerHandlers(eventHandlers, eventBus);
	}
	
	@SuppressWarnings("unchecked")
	private <T extends Object> void registerHandlers(Set<T> handlers, Bus bus) {
		for (Object handler : handlers) {
			
			Method[] methods = handler.getClass().getDeclaredMethods();
			
			for (int i = 0; i < methods.length; i++) {
				Handler handlerAnnotation = methods[i].getAnnotation(Handler.class);
				
				Class<?>[] parameters = methods[i].getParameterTypes();
				
				if (handlerAnnotation != null) {
					if (parameters.length != 1) {
						throw new RuntimeException("handlers should only take a single message as a parameter");
					}
					
					if (!Message.class.isAssignableFrom(parameters[0]) ) {
						throw new RuntimeException("parameter is not a command or event");
					}
					
					bus.registerHandler((Class<? extends Message>)parameters[0], handler);
				}
			}
		}
	}
}
