package edu.ohio.heartworks.infrastructure;

import java.util.List;
import java.util.UUID;

public interface EventStore {
    void save(final UUID aggregateId, final List<Event> events, final int expectedVersion) throws ConcurrencyException;
    int getVersionForAggregate(final UUID aggregateId);
    List<Event> getEventsForAggregateSince(final UUID aggregateId, final int sinceVersion);
    List<Event> getEventsForAggregate(final UUID aggregateId);
    void republishAll();
}