package edu.ohio.heartworks.infrastructure;

public interface Bus {
	<T extends Message> void registerHandler(Class<T> type, Object handler);
}
