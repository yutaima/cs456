package edu.ohio.heartworks.infrastructure;

import java.util.UUID;

public class AggregateDescriptor
{
    public UUID id;
    public Integer version;
    
    public AggregateDescriptor() { }
    
    public AggregateDescriptor(UUID id, Integer version) {
        this.id = id;
        this.version = version;
    }
}