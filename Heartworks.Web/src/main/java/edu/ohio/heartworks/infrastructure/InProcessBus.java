package edu.ohio.heartworks.infrastructure;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InProcessBus implements CommandSender, EventPublisher {
    private final HashMap<Class<? extends Message>, List<Object>> routes
    	= new HashMap<Class<? extends Message>, List<Object>>();

	@Override
	public <T extends Message> void send(T message) {
        List<Object> handlers = routes.get(message.getClass()); 

        if (handlers == null) return;
        
        for(Object handler : handlers) {
            try {
				Method m = handler.getClass().getMethod("handle", message.getClass());
				
				m.invoke(handler, message);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
        }
    }

	@Override
	public <T extends Message> void registerHandler(Class<T> type, Object handler) {
        List<Object> handlers = routes.get(type);
        
        if (handlers == null) {
            handlers = new ArrayList<Object>();
            handlers.add(handler);
            routes.put(type, handlers);
        } else {
            handlers.add(handler);
        }
	}
}
