package edu.ohio.heartworks.infrastructure;

public interface RebuildableEventStore extends EventStore {
	void clear();
}
