package edu.ohio.heartworks.infrastructure;

import java.util.List;

public interface QueueingCommandSender extends CommandSender {
    void pushAll(List<Command> commands);
    List<Command> popAll();
    int size();
}