package edu.ohio.heartworks.infrastructure;

public interface CommandSender extends Bus {
	<T extends Message> void send(T command) throws ConcurrencyException;
}