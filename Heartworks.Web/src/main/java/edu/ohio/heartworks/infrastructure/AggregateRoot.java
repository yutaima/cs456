package edu.ohio.heartworks.infrastructure;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class AggregateRoot {
	private final List<Event> changes = new ArrayList<Event>();

	protected UUID aggregateId;
	protected int version;
	
	public UUID getId() {
	    return aggregateId;
	}
	
	public int getVersion() {
	    return version;
	}

	public List<Event> getUncommittedChanges() {
		return changes;
	}

	public void markChangesAsCommitted() {
		changes.clear();
	}

	public void loadsFromHistory(List<Event> history) {
		for (Event e : history)
			applyChange(e, false);
	}
	
    public void loadsFromHistory(List<Event> history, Integer version) {
        for (Event e : history) {
            
            if (e.version > version) break;
            
            applyChange(e, false);
        }
    }

	protected void applyChange(Event event) {	    
		applyChange(event, true);
				
        event.aggregateId = aggregateId;
	}

	private void applyChange(Event event, boolean isNew) {
		try {
			Method m = getClass().getDeclaredMethod("apply", event.getClass());
			m.invoke(this, event);
			
			if (isNew)
				changes.add(event);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
