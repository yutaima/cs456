package edu.ohio.heartworks.infrastructure;

public interface RepositoryFactory {
	<T extends AggregateRoot> Repository<T> getRepository(Class<T> type);
}
