package edu.ohio.heartworks.infrastructure;

import java.util.UUID;

public abstract class Command implements Message {
    public UUID aggregateId;
    
    // Integer, because we allow this to be null (for "create" commands)
    public int expectedVersion = -1;
}