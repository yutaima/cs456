package edu.ohio.heartworks.infrastructure;

public interface EventPublisher extends Bus {
	<T extends Message> void send(T event);
}