package edu.ohio.heartworks.infrastructure;

import java.util.List;
import java.util.UUID;

public class DefaultRepository<T extends AggregateRoot> implements Repository<T> {
	private final EventStore storage;
	private final Class<T> type;
	
	public DefaultRepository(Class<T> type, EventStore storage) {
		this.type = type;
		this.storage = storage;
	}

	@Override
	public void save(T aggregate, int expectedVersion) throws ConcurrencyException {
		storage.save(aggregate.getId(), aggregate.getUncommittedChanges(), expectedVersion);
	}

	@Override
	public T getById(UUID id) {
		try {
			T obj = type.newInstance();
			List<Event> history = storage.getEventsForAggregate(id);
			obj.loadsFromHistory(history);
			
			return obj;
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

    @Override
    public T getById(UUID id, int version) {
        try {
            T obj = type.newInstance();
            List<Event> history = storage.getEventsForAggregate(id);
            obj.loadsFromHistory(history);
            
            return obj;
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}