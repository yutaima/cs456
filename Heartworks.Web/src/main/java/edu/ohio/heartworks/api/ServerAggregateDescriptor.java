package edu.ohio.heartworks.api;

import java.util.UUID;

import edu.ohio.heartworks.infrastructure.UniqueObject;

public class ServerAggregateDescriptor implements UniqueObject {
    private UUID id;
    public Integer version;
    
    public ServerAggregateDescriptor() { }
    
    public ServerAggregateDescriptor(UUID id, Integer version) {
        this.id = id;
        this.version = version;
    }

	@Override
	public UUID getId() {
		return this.id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServerAggregateDescriptor other = (ServerAggregateDescriptor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}
}
