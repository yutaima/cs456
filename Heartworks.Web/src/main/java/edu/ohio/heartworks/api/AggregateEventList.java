package edu.ohio.heartworks.api;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import edu.ohio.heartworks.infrastructure.Event;

public class AggregateEventList {

	public UUID id;
	public List<Event> events;
	
	public AggregateEventList() {
		this.events = new ArrayList<Event>();
	}
	
	public AggregateEventList(UUID id, List<Event> events) {
		this.id = id;
		this.events = events;
	}
}
