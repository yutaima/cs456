package edu.ohio.heartworks.api;

import java.util.ArrayList;

import edu.ohio.heartworks.infrastructure.Event;

public class EventList extends ArrayList<Event> {
	private static final long serialVersionUID = 6593758042457292537L;
}