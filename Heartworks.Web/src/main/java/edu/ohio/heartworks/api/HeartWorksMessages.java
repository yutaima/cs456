package edu.ohio.heartworks.api;

import org.codehaus.jackson.Version;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.module.SimpleModule;

import edu.ohio.heartworks.domain.fooddiary.FoodTypes.FoodType;
import edu.ohio.heartworks.domain.form.FormItem;
import edu.ohio.heartworks.domain.formsession.FormItemAnswer;
import edu.ohio.heartworks.domain.media.MediaTypes;
import edu.ohio.heartworks.infrastructure.Command;
import edu.ohio.heartworks.infrastructure.Event;

public class HeartWorksMessages extends SimpleModule {
    public HeartWorksMessages() {
        super("HeartworksMessages", Version.unknownVersion());
    }

    @Override
    public void setupModule(SetupContext context) {
        context.setMixInAnnotations(Event.class, MinimalTypeInfoMixin.class);
        context.setMixInAnnotations(Command.class, MinimalTypeInfoMixin.class);
        context.setMixInAnnotations(FormItem.class, MinimalTypeInfoMixin.class);
        context.setMixInAnnotations(FormItemAnswer.class, MinimalTypeInfoMixin.class);
        context.setMixInAnnotations(FoodType.class, MinimalTypeInfoMixin.class);
        context.setMixInAnnotations(MediaTypes.MediaType.class, MinimalTypeInfoMixin.class);
        
        super.setupModule(context);
    }
    
    @JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "type")
    public abstract class MinimalTypeInfoMixin { }
}