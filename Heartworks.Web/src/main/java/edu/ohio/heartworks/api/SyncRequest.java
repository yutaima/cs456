package edu.ohio.heartworks.api;

// Code in the api package contains utility classes for serialization purposes, for example. 
// Plain Java serialization does not know how to restore the type of objects from JSON. These utility classes help.

import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.map.annotate.JsonRootName;

import edu.ohio.heartworks.infrastructure.Command;

@JsonRootName(value = "syncRequest")
public class SyncRequest {
    private UUID clientId;    
    private List<ServerAggregateDescriptor> aggregateDescriptors;
    private List<Command> commands;

	public SyncRequest() { }
    
    public SyncRequest(UUID clientId, List<ServerAggregateDescriptor> aggregateDescriptors, List<Command> commands) {
        this.clientId = clientId;
        this.aggregateDescriptors = aggregateDescriptors;
        this.commands = commands;
    }
    
    public UUID getClientId() {
		return clientId;
	}

	public void setClientId(UUID clientId) {
		this.clientId = clientId;
	}

	public List<ServerAggregateDescriptor> getAggregateDescriptors() {
		return aggregateDescriptors;
	}

	public void setAggregateDescriptors(
			List<ServerAggregateDescriptor> aggregateDescriptors) {
		this.aggregateDescriptors = aggregateDescriptors;
	}

	public List<Command> getCommands() {
		return commands;
	}

	public void setCommands(List<Command> commands) {
		this.commands = commands;
	}
}
