package edu.ohio.heartworks.web.models;

import java.util.UUID;

import net.vz.mongodb.jackson.ObjectId;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import edu.ohio.heartworks.infrastructure.UniqueObject;

public class PublicAggregateModel implements UniqueObject {
	
	@ObjectId @JsonProperty("_id")
	public String aggregateId;

	@Override @JsonIgnore
	public UUID getId() {
		return UUID.fromString(this.aggregateId);
	}
}