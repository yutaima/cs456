package edu.ohio.heartworks.web;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.vz.mongodb.jackson.DBCursor;
import net.vz.mongodb.jackson.JacksonDBCollection;

import edu.ohio.heartworks.infrastructure.ReadModel;
import edu.ohio.heartworks.infrastructure.UniqueObject;

public class MongoDBReadModel<T extends UniqueObject> implements ReadModel<T> {
	
	JacksonDBCollection<T, String> collection;

	public MongoDBReadModel(JacksonDBCollection<T, String> collection) {
		this.collection = collection;
	}
	
	@Override
	public T findById(UUID id) {
		return collection.findOneById(id.toString());
	}

	@Override
	public List<T> findAll() {
		DBCursor<T> cursor = collection.find();
		
		List<T> list = new ArrayList<T>();
		
		while (cursor.hasNext()) {
			list.add(cursor.next());
		}
		
		cursor.close();
		
		return list;
	}

	@Override
	public ReadModel<T> store(T object) {
		collection.save(object);
		
		return this;
	}

	@Override
	public ReadModel<T> deleteByID(UUID id) {
		collection.removeById(id.toString());
		
		return this;
	}

	@Override
	public ReadModel<T> delete(T object) {
		collection.remove(object);
		
		return this;
	}

	@Override
	public ReadModel<T> deleteAll() {
		collection.drop();
		
		return this;
	}
}
