package edu.ohio.heartworks.web;

import java.util.ArrayList;
import java.util.UUID;
import java.util.List;

import edu.ohio.heartworks.domain.form.FormCommands.AssignForm;
import edu.ohio.heartworks.domain.form.FormCommands.CreateForm;
import edu.ohio.heartworks.domain.form.FormItem;
import edu.ohio.heartworks.domain.form.ParagraphQuestion;
import edu.ohio.heartworks.domain.form.RangedQuestion;
import edu.ohio.heartworks.domain.lesson.LessonCommands.CreateLesson;
import edu.ohio.heartworks.infrastructure.CommandSender;
import edu.ohio.heartworks.infrastructure.EventStore;

public class DatabaseBootstrapper {
	
	private CommandSender commandBus;
	private EventStore eventStore;

	public DatabaseBootstrapper(CommandSender commandBus, EventStore eventStore) {
		this.commandBus = commandBus;
		this.eventStore = eventStore;
	}

	public void Bootstrap() {
		bootstrapDocs();
		bootstrapLessons();
	}
	
	private void bootstrapDocs() {
		UUID docId = UUID.fromString("5f31fd00-75b8-11e2-bcfd-0800200c9a66");
		
		if (eventStore.getVersionForAggregate(docId) > -1) return;
		
		List<FormItem> formItems = new ArrayList<FormItem>();
		
		List<String> answerList1 = new ArrayList<String>();
		answerList1.add("Okay");
		answerList1.add("Good");
		answerList1.add("Great");
		
		formItems.add(new RangedQuestion(UUID.randomUUID(), "How are you?", answerList1));
		formItems.add(new ParagraphQuestion(UUID.randomUUID(), "How do you feel?"));
		
		{
			CreateForm command = new CreateForm();
			command.aggregateId = docId;
			command.items = formItems;
			command.name = "Daily Questionnaire";
			command.expectedVersion = -1;
			
			commandBus.send(command);
		}
		
		{
			AssignForm command = new AssignForm();
			command.aggregateId = docId;
			command.cronExpression = "0 0 * * *";
			command.expectedVersion = 0;
			
			commandBus.send(command);
		}
	}
	
	private void bootstrapLessons() {
		UUID lessonId = UUID.fromString("82161ae0-785b-11e2-b92a-0800200c9a66");
		
		if (eventStore.getVersionForAggregate(lessonId) > -1) return;
		
		List<String> files = new ArrayList<String>();
		files.add("brokenheart.png");
		files.add("neg_stress_cycle.png");
		files.add("weights.png");
		
		CreateLesson command = new CreateLesson();
		command.aggregateId = lessonId;
		command.lessonName = "Managing Stress";
		command.htmlContent = lessonContent;
		command.files = files;
		
		commandBus.send(command);
	}
	
	// Used http://www.freeformatter.com/java-dotnet-escape.html to escape
	private static final String lessonContent = "<!DOCTYPE html>\r\n<html>\r\n\t<body>\r\n\t\t<h1 style=\"text-align:center;\"; style=\"font-family:verdana\">Managing Stress for Your Health</h1>\r\n\t\t<p style=\"font-family:verdana\">\r\n\t\t\tStress is a natural part of our lives. We all confront physical, psychological,\r\n\t\t\tenvironmental, and social stress. We may think we know all there is to know\r\n\t\t\tabout stress, but do we really?\r\n\t\t</p>\r\n\r\n\r\n\t\t<br /><h4 style=\"font-family:verdana\">\"Stress\" is Difficult to Define</h4>\r\n\t\t<ul>\r\n\t\t\t<li style=\"font-family:verdana\">Stress is not just negative - it can be positive. Life would be boringwithout some stress!</li>\r\n\t\t\t<li style=\"font-family:verdana\">What creates stress for one person can be a challenge for another person.</li>\r\n\t\t\t<li style=\"font-family:verdana\">Stress can increase performance and efficiency (until levels become too high and both performance and efficiency begin to diminish).</li>\r\n\t\t</ul>\r\n\r\n\r\n\t\t<br /><h4 style=\"font-family:verdana\">Stress (or Distress) Defined</h4>\r\n\t\t<p style=\"font-family:verdana\">\r\n\t\t\tStress is the <i>perception</i> of a threat to one's physical or psychological well-being\r\n\t\t\tand the <i>perception</i> that one is unable to cope with the threat.\r\n\t\t</p>\r\n\r\n\r\n\t\t<br /><h4 style=\"font-family:verdana\">The Negative Stress Cycle</h4>\r\n\t\t<p style=\"text-align:center;\"><img src=\"neg_stress_cycle.png\" alt=\"Negative Stress Cycle\" width=\"500\" height=\"500\" align=\"center\"/></p>\r\n\r\n\r\n\t\t<br /><h4 style=\"font-family:verdana\">What is the Fight or Flight Response?</h4>\r\n\t\t<p style=\"font-family:verdana\">\r\n\t\t\tDuring times of stress, the body prepares itself for action - to either flee or enter\r\n\t\t\tinto battle. Our bodies developed this response centuries ago to help us survive in\r\n\t\t\thunter-gatherer society.\r\n\t\t</p>\r\n\r\n\r\n\t\t<br /><h4 style=\"font-family:verdana\">What happens during the Fight or Flight Response?</h4>\r\n\t\t<p style=\"font-family:verdana\">\r\n\t\t\tIncreased heart rate, blood pressure, muscle tension, breathing rate, metabolism.\r\n\t\t</p>\r\n\t\t<img src=\"brokenheart.png\" alt=\"Heart Doctor Image\" width=\"250\" height=\"250\" align=\"right\" align=\"middle\"/>\r\n\r\n\t\t<br /><h4 style=\"font-family:verdana\">The Problem:</h4>\t\t\t\r\n\t\t<p style=\"font-family:verdana\">\r\n\t\t\tThough we don't face the same dangers today, our body still\r\n\t\t\treacts the same way during a perceived threat. During real\r\n\t\t\temergencies, this response is helpful. But prolonged states\r\n\t\t\tof arousal is harmful to the body - particularly the heart. \r\n\t\t\t<br /><br /><br /><br /><br />\r\n\t\t</p>\r\n\t\t<table frame=\"border\">\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\"><b>Chronic stress can lead to physical symptoms, which may cause or worsen illness.</b></td>\r\n\t\t\t</tr> \r\n\t\t</table>\r\n\r\n\t\t<br /><h4 style=\"font-family:verdana\">Chronic Stress can affect how we think, feel, and act ...</h4>\r\n\t\t<ul>\r\n\t\t\t<li style=\"font-family:verdana\"><b>Think.</b> May be difficult to concentrate, make appropriate decisions.</li>\r\n\t\t\t<li style=\"font-family:verdana\"><b>Feel.</b> May feel anxious, helpless, overwhelmed, angry.</li>\r\n\t\t\t<li style=\"font-family:verdana\"><b>Act.</b> May change our behavior by eating, drinking, or smoking too much - \r\n\t\t\t\t\t\t\t\t\tor neglecting healthy habits like exercise.</li>\r\n\t\t</ul>\r\n\r\n\t\t\r\n\t\t<br /><h2 style=\"text-align:center;\";style=\"font-family:verdana\">Stress Warning Signs</h2>\r\n\t\t<h4 style=\"font-family:verdana\">Physical Symptoms</h4>\r\n\t\t<table cellspacing=\"15\">\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Headaches</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Indigestion</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Stomachaches</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Sweaty palms</td>\r\n\t\t\t</tr>\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Sleep difficulties</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Dizziness</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Back pain</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Tight shoulders</td>\r\n\t\t\t</tr>\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Racing heart</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Restlessness</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Tiredness</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Ringing in ears</td>\r\n\t\t\t</tr> \r\n\t\t</table>\r\n\t\t<br /><h4 style=\"font-family:verdana\">Behavioral Symptoms</h4>\r\n\t\t<table cellspacing=\"15\">\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Excess smoking</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Bossiness</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Compulsive eating</td>\r\n\t\t\t</tr>\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Overuse of alcohol</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Grinding teeth</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Critical attitude towards others</td>\r\n\t\t\t</tr>\r\n\t\t\t<tr>\r\n\t\t\t\t<td colspan=\"2\"; style=\"font-family:verdana\">Inability to get things done</td>\r\n\t\t\t</tr> \r\n\t\t</table>\r\n\t\t<br /><h4 style=\"font-family:verdana\">Emotional Symptoms</h4>\r\n\t\t<table cellspacing=\"15\">\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Crying</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Nervousness, anxiety</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Boredom - no meaning to things</td>\r\n\t\t\t</tr>\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Anger</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Loneliness</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Edginess - ready to explode</td>\r\n\t\t\t</tr>\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Easily upset</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Unhappiness</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Overwhelming sense of pressure</td>\r\n\t\t\t</tr> \r\n\t\t\t<tr>\r\n\t\t\t\t<td colspan=\"2\";  style=\"font-family:verdana\">Feeling powerless to change things</td>\r\n\t\t\t</tr> \r\n\t\t</table>\r\n\t\t<br /><h4 style=\"font-family:verdana\">Cognitive Symptoms</h4>\r\n\t\t<table cellspacing=\"15\">\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Trouble thinking clearly</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Inability to make decisions</td>\r\n\t\t\t</tr>\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Forgetfulness</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Thoughts of running away</td>\r\n\t\t\t</tr>\r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Memory loss</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Thoughts of running away</td>\r\n\t\t\t</tr> \r\n\t\t\t<tr>\r\n\t\t\t\t<td style=\"font-family:verdana\">Lack of creativity</td>\r\n\t\t\t\t<td style=\"font-family:verdana\">Constant worry</td>\r\n\t\t\t</tr> \r\n\t\t</table>\r\n\r\n\r\n\t\t<br /><h2 style=\"text-align:center;\";style=\"font-family:verdana\">The Stress Solution: The Relaxation Response</h2>\r\n\t\t<p style=\"font-family:verdana\">\r\n\t\t\tResearch has found that we can counter the fight-or-flight response through\r\n\t\t\trelaxation. During relaxation, we allow our bodies and minds to rest - so that our\r\n\t\t\theart doesn't have to work as hard.\r\n\t\t\t<br /><br />\r\n\t\t\t<ul>\r\n\t\t\t<li style=\"font-family:verdana\">sit quietly without worry, focusing your thoughts on your breath or some relaxing image</li>\r\n\t\t\t<li style=\"font-family:verdana\">takes slow breaths from the abdomen</li>\r\n\t\t\t<li style=\"font-family:verdana\">close your eyes, relax your muscles</li>\r\n\t\t\t</ul>\r\n\t\t</p>\r\n\r\n\r\n\t\t<br /><h4 style=\"text-align:left;\";style=\"font-family:verdana\">What happens during the Relaxation Response?</h4>\r\n\t\t<p style=\"font-family:verdana\">\r\n\t\t\tDecreased heart rate, blood pressure, muscle tension, breathing rate, metabolism.\r\n\t\t</p>\r\n\r\n\r\n\t\t<br /><h2 style=\"text-align:center;\";style=\"font-family:verdana\">Four-Step Approach to Stress Management</h2>\r\n\t\t<ul>\r\n\t\t\t<li style=\"font-family:verdana\">Stop</li>\r\n\t\t\t<li style=\"font-family:verdana\">Breathe</li>\r\n\t\t\t<li style=\"font-family:verdana\">Reflect</li>\r\n\t\t\t<li style=\"font-family:verdana\">Choose</li>\r\n\t\t</ul>\r\n\t\t<p style=\"font-family:verdana\">\r\n\t\t\t<b>Stop</b><br />\r\n\t\t\tEach time you encounter a stress, <b>STOP</b> - before your thoughts escalate and your\r\n\t\t\tmind and body become tense.\r\n\t\t\t<br /><br />\r\n\r\n\t\t\t<b>Breathe</b><br />\r\n\t\t\tAfter you stop, <b>BREATHE</b> deeply and release physical tension. Take in a deep\r\n\t\t\tbreath, using diaphragmatic breathing, and elicit the relaxation response. While\r\n\t\t\tyou concentrate on your breath, your mind is momentarily diverted from the\r\n\t\t\tstress. Even this momentary interruption can help to re-direct your attention and\r\n\t\t\thelp you to look at the stress in a different way.\r\n\t\t\t<br />\r\n\t\t\t<table frame=\"border\">\r\n\t\t\t\t<tr>\r\n\t\t\t\t\t<td style=\"font-family:verdana\">\r\n\t\t\t\t\t\tSo...\r\n\t\t\t\t\t\t<ol>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">Use diaphragmatic breathing to release physical tension</li>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">Redirect your attention to the stress.</li>\r\n\t\t\t\t\t\t</ol>\r\n\t\t\t\t\t</td>\r\n\t\t\t\t</tr> \r\n\t\t\t</table>\r\n\t\t\t<br /><br />\r\n\r\n\t\t\t<b>Reflect</b><br />\r\n\t\t\tOnce your body is less tense, you can focus your energy on the problem at hand\r\n\t\t\tand <b>REFLECT</b> on the cause of the stress. By identifying the stressor and your\r\n\t\t\timmediate reaction to the stress, you can think about whether this is a healthy way\r\n\t\t\tto deal with the stress.\r\n\t\t\t<br /><br />\r\n\t\t\t<table frame=\"border\">\r\n\t\t\t\t<tr>\r\n\t\t\t\t\t<td style=\"font-family:verdana\">\r\n\t\t\t\t\t\tSo...\r\n\t\t\t\t\t\t<ol>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">Consider the situation.</li>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">What is the concern?</li>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">What is your immediate reaction?</li>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">Is that reaction appropriate? (Are you distracted by some other problem?\r\n\t\t\t\t\t\t\t\t\t\t\t\tIs the problem exaggerated by some other stress?)</li>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">What needs to be dealt with now?</li>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">Are you or your loved ones threatened due to this problem?</li>\r\n\t\t\t\t\t\t</ol>\r\n\t\t\t\t\t</td>\r\n\t\t\t\t</tr> \r\n\t\t\t</table>\r\n\t\t\t<br /><br />\r\n\r\n\t\t\t<b>Choose</b><br />\r\n\t\t\tAfter you have stopped your automatic response to the stress, taken a breath to\r\n\t\t\tcalm your body & divert your attention, and reflected on the problem and its\r\n\t\t\tcause, it's now time to <b>CHOOSE</b> how to deal with the stress.\r\n\t\t\t<br /><br />\r\n\t\t\t<table frame=\"border\">\r\n\t\t\t\t<tr>\r\n\t\t\t\t\t<td style=\"font-family:verdana\">\r\n\t\t\t\t\t\tSo...\r\n\t\t\t\t\t\t<ol>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">What do I want?</li>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">What can I do to cope with this problem?</li>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">Do I have the time, skills, and energy to deal with the problem now?</li>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">Do I need to calm my emotional reaction before responding? Should I wait\r\n\t\t\t\t\t\t\t\t\t\t\t\tsome time before acting?</li>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">What is possible given my circumstances?</li>\r\n\t\t\t\t\t\t\t<li style=\"font-family:verdana\">What is my decision?</li>\r\n\t\t\t\t\t\t</ol>\r\n\t\t\t\t\t</td>\r\n\t\t\t\t</tr> \r\n\t\t\t</table>\r\n\t\t\t<br />\r\n\t\t\t<p style=\"text-align:center;\"><img src=\"weights.png\" alt=\"Weights\" width=\"500\" height=\"500\" align=\"center\"/></p>\r\n\t\t</p>\r\n\t</body>\r\n</html>";
}