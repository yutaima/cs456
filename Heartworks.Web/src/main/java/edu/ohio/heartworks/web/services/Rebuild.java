package edu.ohio.heartworks.web.services;

import com.google.inject.Inject;
import com.google.sitebricks.At;
import com.google.sitebricks.headless.Service;
import com.google.sitebricks.http.Post;

import edu.ohio.heartworks.web.StateRebuilder;

@Service @At("/rebuild")
public class Rebuild {

	@Inject private StateRebuilder stateRebuilder;
	
	@Post
	public void rebuild() {
		stateRebuilder.rebuild();
	}
}