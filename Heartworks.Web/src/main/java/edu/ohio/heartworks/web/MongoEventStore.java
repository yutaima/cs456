package edu.ohio.heartworks.web;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bson.types.ObjectId;
import org.codehaus.jackson.map.ObjectMapper;

import net.vz.mongodb.jackson.DBCursor;
import net.vz.mongodb.jackson.DBQuery;
import net.vz.mongodb.jackson.DBQuery.Query;
import net.vz.mongodb.jackson.JacksonDBCollection;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;

import edu.ohio.heartworks.infrastructure.ConcurrencyException;
import edu.ohio.heartworks.infrastructure.Event;
import edu.ohio.heartworks.infrastructure.EventPublisher;
import edu.ohio.heartworks.infrastructure.EventStore;

public class MongoEventStore implements EventStore {
	
	private EventPublisher publisher;
	private JacksonDBCollection<EventDescriptor, ObjectId> eventCollection;

	public MongoEventStore(DB database, EventPublisher publisher, ObjectMapper mapper) {
		this.publisher = publisher;
		
		DBCollection eventCollection = database.getCollection("Events");
		this.eventCollection = JacksonDBCollection.wrap(eventCollection, EventDescriptor.class, ObjectId.class, mapper);
	}

	@Override
	public void save(final UUID aggregateId, final List<Event> events, final int expectedVersion) throws ConcurrencyException {
		if (aggregateId == null) {
			throw new IllegalArgumentException("aggregate id is null");
		}
		
		if (getVersionForAggregate(aggregateId) != expectedVersion) {
			throw new ConcurrencyException();
		}
        
        int i = expectedVersion;
        for (Event event : events){
            i++;
            event.version = i;
            
            EventDescriptor eventDescriptor = new EventDescriptor();
            eventDescriptor.eventData = event;
            
            eventCollection.insert(eventDescriptor);
            publisher.send(event);
        }
	}

	@Override
	public int getVersionForAggregate(final UUID aggregateId) {
		DBCursor<EventDescriptor> cursor = eventCollection.find(DBQuery.is("eventData.aggregateId", aggregateId.toString())).sort(new BasicDBObject("eventData.version" , -1));
		
		EventDescriptor descriptor = null;
		
		if (cursor.hasNext()) {
			 descriptor = cursor.next();
		}
		
		cursor.close();
		
		if (descriptor == null) {
			return -1;
		}
		
		return descriptor.eventData.version;
	}

	@Override
	public List<Event> getEventsForAggregateSince(final UUID aggregateId, final int sinceVersion) {
		List<Event> eventList = new ArrayList<Event>();
		
		if (eventCollection.count() < 1) {
			return eventList;
		}
		
		Query query = DBQuery.and(DBQuery.is("eventData.aggregateId", aggregateId.toString()), DBQuery.greaterThan("eventData.version", sinceVersion));
		
		DBCursor<EventDescriptor> cursor = eventCollection.find(query).sort(new BasicDBObject("eventData.version" , 1));
		
		while (cursor.hasNext()) {
			EventDescriptor descriptor = cursor.next();
			eventList.add(descriptor.eventData);
		}
		
		cursor.close();
		
		return eventList;
	}

	@Override
	public List<Event> getEventsForAggregate(final UUID aggregateId) {
		return getEventsForAggregateSince(aggregateId, -1);
	}
	
	@Override
	public void republishAll() {
		DBCursor<EventDescriptor> eventDescriptors = eventCollection.find();
		
		for (EventDescriptor descriptor : eventDescriptors) {
			publisher.send(descriptor.eventData);
		}
	}

    static class EventDescriptor {
    	public ObjectId _id;
		public Event eventData;
    }
}
