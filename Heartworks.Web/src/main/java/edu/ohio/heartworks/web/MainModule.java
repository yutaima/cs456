package edu.ohio.heartworks.web;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;

import net.vz.mongodb.jackson.JacksonDBCollection;

import org.codehaus.jackson.map.ObjectMapper;

import com.google.inject.AbstractModule;
import com.google.inject.BindingAnnotation;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.mongodb.DB;
import com.mongodb.MongoException;
import com.mongodb.MongoURI;

import edu.ohio.heartworks.api.HeartWorksMessages;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryCommandHandler;
import edu.ohio.heartworks.domain.form.FormCommandHandler;
import edu.ohio.heartworks.domain.formsession.FormSessionCommandHandler;
import edu.ohio.heartworks.domain.lesson.LessonCommandHandler;
import edu.ohio.heartworks.domain.media.MediaItemCommandHandler;
import edu.ohio.heartworks.infrastructure.AggregateRoot;
import edu.ohio.heartworks.infrastructure.BusBootstrapper;
import edu.ohio.heartworks.infrastructure.CommandHandler;
import edu.ohio.heartworks.infrastructure.CommandSender;
import edu.ohio.heartworks.infrastructure.DefaultRepository;
import edu.ohio.heartworks.infrastructure.EventHandler;
import edu.ohio.heartworks.infrastructure.EventPublisher;
import edu.ohio.heartworks.infrastructure.EventStore;
import edu.ohio.heartworks.infrastructure.InProcessBus;
import edu.ohio.heartworks.infrastructure.ReadModel;
import edu.ohio.heartworks.infrastructure.ReadModelFactory;
import edu.ohio.heartworks.infrastructure.Repository;
import edu.ohio.heartworks.infrastructure.RepositoryFactory;
import edu.ohio.heartworks.infrastructure.UniqueObject;
import edu.ohio.heartworks.web.eventhandlers.FoodDiaryEventHandler;
import edu.ohio.heartworks.web.eventhandlers.PublicAggregateEventHandler;
import edu.ohio.heartworks.web.filestore.FileStore;
import edu.ohio.heartworks.web.filestore.GridFSFileStore;

public class MainModule extends AbstractModule {
	
	@BindingAnnotation @Target({ FIELD, PARAMETER, METHOD }) @Retention(RUNTIME)
	@interface DatabaseMapper {}

	@Override
	protected void configure() {
		bind(String.class).annotatedWith(Names.named("MongoDB URI"))
			.toInstance("mongodb://heartworks:heartworks@ds045297.mongolab.com:45297/heartworks");
	    
		Multibinder<EventHandler> eventBinder = Multibinder.newSetBinder(binder(), EventHandler.class);
		
		eventBinder.addBinding().to(FoodDiaryEventHandler.class);
		eventBinder.addBinding().to(PublicAggregateEventHandler.class);
	}
	
	@Provides @Singleton
	Set<CommandHandler> provideCommandHandlers(RepositoryFactory repositoryFactory) {
		Set<CommandHandler> handlers = new HashSet<CommandHandler>();
		
		handlers.add(new FormCommandHandler(repositoryFactory));
		handlers.add(new FormSessionCommandHandler(repositoryFactory));
		handlers.add(new FoodDiaryCommandHandler(repositoryFactory));
		handlers.add(new MediaItemCommandHandler(repositoryFactory));
		handlers.add(new LessonCommandHandler(repositoryFactory));
		
		return handlers;
	}
	
	@Provides
	FileStore provideFileStore(DB db) {
		return new GridFSFileStore(db);		
	}
	
	@Provides
	CollectionFactory provideCollectionFactory(final DB mongoDb, final ObjectMapper objectMapper) {
		return new CollectionFactory() {
			@Override
			public <V, K> JacksonDBCollection<V, K> getCollection(Class<V> valueType, Class<K> keyType) {
				return JacksonDBCollection.wrap(mongoDb.getCollection(valueType.getSimpleName()), valueType, keyType, objectMapper);
			}
		};
	}
	
	@Provides
	EventStore provideEventStore(DB mongoDb, EventPublisher eventPublisher, @DatabaseMapper ObjectMapper objectMapper) {
		return new MongoEventStore(mongoDb, eventPublisher, objectMapper);
	}
	
	@Provides
	ReadModelFactory provideReadModelFactory(final CollectionFactory collectionFactory) {
		return new ReadModelFactory() {
			@Override
			public <T extends UniqueObject> ReadModel<T> getReadModel(Class<T> modelType) {
				return new MongoDBReadModel<T>(collectionFactory.getCollection(modelType, String.class));
			}
		};
	}
	
	@Provides
	RepositoryFactory provideRepositoryFactory(final EventStore storage) {
		return new RepositoryFactory() {
			@Override
			public <T extends AggregateRoot> Repository<T> getRepository(Class<T> type) {
				return new DefaultRepository<T>(type, storage);
			}
		};
	}
	
	@Provides @DatabaseMapper
	ObjectMapper provideDatabaseObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new HeartWorksMessages());
        mapper.registerModule(net.vz.mongodb.jackson.internal.MongoJacksonMapperModule.INSTANCE);
        
        return mapper;
	}

	@Provides
	ObjectMapper provideGenericObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new HeartWorksMessages());
        
        return mapper;
	}
	
	@Provides @Singleton
	DB provideMongoDB(@Named("MongoDB URI") String uriString) {
		
		// We opt to use the MongoURI class to access MongoDB connection methods.
		MongoURI uri = new MongoURI(uriString);
		DB database = null;

		try {
			// The MongoURI class can connect and return a database given the URI above.
			database = uri.connectDB();
			
			// If you are running in auth mode and have provided user info in your URI, you can use this line.
			database.authenticate(uri.getUsername(), uri.getPassword());
		} catch (UnknownHostException uhe) {
			System.out.println("UnknownHostException: " + uhe);
		} catch (MongoException me) {
			System.out.println("MongoException: " + me);
		}

		return database;
	}
	
	@Provides
	BusBootstrapper busBootstrapperProvider(
			Set<CommandHandler> commandHandlers,
			Set<EventHandler> eventHandlers,
			CommandSender commandBus,
			EventPublisher eventBus) {
		
		return new BusBootstrapper(commandHandlers, eventHandlers, commandBus, eventBus);
	}
	
	@Provides @Singleton
	CommandSender provideCommandSender() {
		return new InProcessBus();
	}
	
	@Provides @Singleton
	EventPublisher provideEventPublisher() {
		return new InProcessBus();
	}
	
	@Provides
	DatabaseBootstrapper dbBootstrapperProvider(CommandSender commandBus, EventStore eventStore) {
		
		return new DatabaseBootstrapper(commandBus, eventStore);
	}
}
