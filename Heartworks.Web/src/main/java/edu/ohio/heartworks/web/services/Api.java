package edu.ohio.heartworks.web.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import com.google.inject.Inject;
import com.google.sitebricks.At;
import com.google.sitebricks.client.transport.Json;
import com.google.sitebricks.headless.Reply;
import com.google.sitebricks.headless.Request;
import com.google.sitebricks.headless.Service;
import com.google.sitebricks.http.Post;

import edu.ohio.heartworks.api.AggregateEventList;
import edu.ohio.heartworks.api.EventList;
import edu.ohio.heartworks.api.ServerAggregateDescriptor;
import edu.ohio.heartworks.api.SyncRequest;
import edu.ohio.heartworks.infrastructure.Command;
import edu.ohio.heartworks.infrastructure.CommandSender;
import edu.ohio.heartworks.infrastructure.EventStore;
import edu.ohio.heartworks.infrastructure.ReadModel;
import edu.ohio.heartworks.infrastructure.ReadModelFactory;
import edu.ohio.heartworks.web.models.PublicAggregateModel;

@Service @At("/api") 
public class Api {
	
	@Inject CommandSender commandBus;
	@Inject EventStore eventStore;
	@Inject ReadModelFactory readModelFactory;
	
	@Post
	public Reply<?> post(Request request) {

        ArrayList<AggregateEventList> eventList = new ArrayList<AggregateEventList>();

		SyncRequest syncRequest = request.read(SyncRequest.class).as(Json.class);
		
		if (syncRequest == null) {
			return Reply.saying().error();
		}
		
		for (Command command : syncRequest.getCommands()) {
			commandBus.send(command);
		}
		
		List<ServerAggregateDescriptor> descriptors = syncRequest.getAggregateDescriptors();
		
		HashSet<UUID> descriptorIds = new HashSet<UUID>();
		
		for (ServerAggregateDescriptor descriptor : descriptors) {
			descriptorIds.add(descriptor.getId());
		}
		
		// Add descriptors for public aggs. if the client doesn't know about them already
		ReadModel<PublicAggregateModel> readModel = readModelFactory.getReadModel(PublicAggregateModel.class);
		List<PublicAggregateModel> aggregates = readModel.findAll();
		
		for (PublicAggregateModel aggregate : aggregates) {
			UUID aggregateId = UUID.fromString(aggregate.aggregateId);
			
			if (!descriptorIds.contains(aggregateId)) {
				descriptors.add(new ServerAggregateDescriptor(aggregateId, -1));
			}
		}
		
		for (ServerAggregateDescriptor descriptor : descriptors) {

			EventList newEvents = new EventList();
			newEvents.addAll(eventStore.getEventsForAggregateSince(descriptor.getId(), descriptor.version));
			
            if (!newEvents.isEmpty()) {
            	eventList.add(new AggregateEventList(descriptor.getId(), newEvents));
            }
		}
		
		return Reply.with(eventList).as(Json.class);
	}
}
