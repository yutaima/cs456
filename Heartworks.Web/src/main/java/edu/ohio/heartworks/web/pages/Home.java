package edu.ohio.heartworks.web.pages;

import java.io.File;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.sitebricks.At;
import com.google.sitebricks.http.Get;
import com.google.sitebricks.http.Post;

import edu.ohio.heartworks.domain.media.MediaItemCommands.CreateMediaItem;
import edu.ohio.heartworks.infrastructure.CommandSender;
import edu.ohio.heartworks.infrastructure.ReadModel;
import edu.ohio.heartworks.infrastructure.ReadModelFactory;
import edu.ohio.heartworks.web.models.FoodDiaryModel;
import edu.ohio.heartworks.domain.media.MediaTypes;

@At("/")
public class Home {
	
	@Inject private ReadModelFactory readModelFactory;
	@Inject private CommandSender commandBus;
	@Inject private Provider<ServletContext> servletContextProvider;
	
	private List<FoodDiaryModel> foodDiaries;
	
	public List<FoodDiaryModel> getFoodDiaries() {
		return foodDiaries;
	}

	@Get
	public void get() {
		ReadModel<FoodDiaryModel> readModel = readModelFactory.getReadModel(FoodDiaryModel.class);
		foodDiaries = readModel.findAll();
	}
	
	@Post
	public void post(HttpServletRequest request) throws Exception {
				
		if (!ServletFileUpload.isMultipartContent(request)) {
			return;// Reply.saying().status(400);
		}
		
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload fileUpload = new ServletFileUpload(factory);
		
		@SuppressWarnings("unchecked")
		List<FileItem> items = fileUpload.parseRequest(request);
		
	    for (FileItem item : items) {
	        if (!item.isFormField()) {
	        	
                UUID mediaId = UUID.randomUUID();
	            
	            String fileName = item.getName();
	        	
	        	File root = new File(servletContextProvider.get().getRealPath("/files"));
	        	File fileDir = new File(root, mediaId.toString());
                
                if (!fileDir.exists()) {
                	fileDir.mkdirs();
                }

                File file = new File(fileDir, fileName);
                item.write(file);
                
                CreateMediaItem command = new CreateMediaItem();
                command.aggregateId = mediaId;
                command.fileName = fileName;
                command.name = fileName;
                
                // TODO: this is hard coded for now
                command.mediaType = new MediaTypes.Audio();
                		
                commandBus.send(command);
                
                break;
	        }
	    }
	}
}