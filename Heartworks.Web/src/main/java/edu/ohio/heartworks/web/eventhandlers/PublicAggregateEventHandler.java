package edu.ohio.heartworks.web.eventhandlers;

import com.google.inject.Inject;

import edu.ohio.heartworks.domain.form.FormEvents.FormCreated;
import edu.ohio.heartworks.domain.lesson.LessonEvents.LessonCreated;
import edu.ohio.heartworks.domain.media.MediaItemEvents.MediaItemCreated;
import edu.ohio.heartworks.infrastructure.Event;
import edu.ohio.heartworks.infrastructure.EventHandler;
import edu.ohio.heartworks.infrastructure.Handler;
import edu.ohio.heartworks.infrastructure.ReadModel;
import edu.ohio.heartworks.infrastructure.ReadModelFactory;
import edu.ohio.heartworks.web.models.PublicAggregateModel;

public class PublicAggregateEventHandler implements EventHandler {
	
	private ReadModel<PublicAggregateModel> readModel;

	@Inject
	public PublicAggregateEventHandler(ReadModelFactory factory) {
		this.readModel = factory.getReadModel(PublicAggregateModel.class);
	}
	
	@Handler
	public void handle(FormCreated event) {
		saveModel(event);
	}
	
	@Handler
	public void handle(LessonCreated event) {
		saveModel(event);
	}
	
	@Handler
	public void handle(MediaItemCreated event) {
		saveModel(event);
	}
	
	@Override
	public void clear() {
		readModel.deleteAll();
	}
	
	private void saveModel(Event event) {
		PublicAggregateModel model = new PublicAggregateModel();
		model.aggregateId = event.aggregateId.toString();
		
		readModel.store(model);
	}
}