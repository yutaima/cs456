package edu.ohio.heartworks.web.eventhandlers;

import com.google.inject.Inject;

import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.DescriptionChanged;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodDiaryCreated;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodPictureAdded;
import edu.ohio.heartworks.domain.fooddiary.FoodDiaryEvents.FoodTypeChanged;
import edu.ohio.heartworks.infrastructure.EventHandler;
import edu.ohio.heartworks.infrastructure.Handler;
import edu.ohio.heartworks.infrastructure.ReadModel;
import edu.ohio.heartworks.infrastructure.ReadModelFactory;
import edu.ohio.heartworks.web.models.FoodDiaryModel;

public class FoodDiaryEventHandler implements EventHandler {
	
	private ReadModel<FoodDiaryModel> readModel;

	@Inject
	public FoodDiaryEventHandler(ReadModelFactory readModelFactory) {
		this.readModel = readModelFactory.getReadModel(FoodDiaryModel.class);
	}
	
	@Handler
	public void handle(FoodDiaryCreated event) {
		FoodDiaryModel diary = new FoodDiaryModel(event.aggregateId.toString(), event.dateCreated);
		
		readModel.store(diary);
	}
	
	@Handler
	public void handle(DescriptionChanged event) {
		FoodDiaryModel diary = readModel.findById(event.aggregateId);
		diary.description = event.description;
		diary.version++;
		
		readModel.store(diary);
	}
	
	@Handler
	public void handle(FoodPictureAdded event) {
		FoodDiaryModel diary = readModel.findById(event.aggregateId);
		diary.imageFileNames.add(event.aggregateId + "/" + event.fileName);
		diary.version++;
		
		readModel.store(diary);
	}
	
	@Handler
	public void handle(FoodTypeChanged event) {
		FoodDiaryModel diary = readModel.findById(event.aggregateId);
		diary.foodType = event.foodType;
		diary.version++;
		
		readModel.store(diary);
	}

	@Override
	public void clear() {
		readModel.deleteAll();
	}
}
