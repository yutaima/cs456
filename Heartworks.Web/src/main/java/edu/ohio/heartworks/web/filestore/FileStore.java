package edu.ohio.heartworks.web.filestore;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.apache.commons.io.output.ByteArrayOutputStream;

public interface FileStore {
	void storeFile(UUID aggregateId, String fileName, InputStream inputFileStream) throws IOException;
	ByteArrayOutputStream getFile(UUID aggregateId, String fileName) throws IOException;
}