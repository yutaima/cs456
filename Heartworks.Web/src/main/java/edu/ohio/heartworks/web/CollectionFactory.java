package edu.ohio.heartworks.web;

import net.vz.mongodb.jackson.JacksonDBCollection;

public interface CollectionFactory {
	<V, K> JacksonDBCollection<V, K> getCollection(Class<V> valueType, Class<K> keyType);
}