package edu.ohio.heartworks.web.services;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.ByteArrayOutputStream;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.google.sitebricks.At;
import com.google.sitebricks.client.transport.Raw;
import com.google.sitebricks.headless.Reply;
import com.google.sitebricks.headless.Service;
import com.google.sitebricks.http.Get;
import com.google.sitebricks.http.Post;

import edu.ohio.heartworks.web.filestore.FileStore;

@Service @At("/file")
public class File {
	
	@Inject Provider<ServletContext> servletContextProvider;
	@Inject FileStore fileStore;
	
	@Get @At("/:id/:fileName")
	public Reply<?> get(@Named("id") String id, @Named("fileName") String fileName) throws IOException {
		ByteArrayOutputStream outStream = fileStore.getFile(UUID.fromString(id), fileName);
		
		return Reply.with(outStream.toByteArray()).as(Raw.class).ok();
	}

	@Post @At("/:id")
	public Reply<?> post(HttpServletRequest request, @Named("id") String id) throws Exception {
		
		if (!ServletFileUpload.isMultipartContent(request)) {
			return Reply.saying().status(400);
		}
		
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload fileUpload = new ServletFileUpload(factory);
		
		@SuppressWarnings("unchecked")
		List<FileItem> items = fileUpload.parseRequest(request);
	    
	    for (FileItem item : items) {
	        if (!item.isFormField()) {

	        	String fileName = item.getName();
	        	
	        	fileStore.storeFile(UUID.fromString(id), fileName, item.getInputStream());
                
                break;
	        }
	    }

		return Reply.saying().status(201);
	}
}
