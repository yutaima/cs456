package edu.ohio.heartworks.web;

import java.util.Set;

import com.google.inject.Inject;

import edu.ohio.heartworks.infrastructure.EventHandler;
import edu.ohio.heartworks.infrastructure.EventStore;

public class StateRebuilder {
	
	@Inject private Set<EventHandler> eventHandlers;
	@Inject private EventStore eventStore;
	
	public void rebuild() {
		for (EventHandler handler : eventHandlers) {
			handler.clear();
		}
		
		eventStore.republishAll();
	}
}