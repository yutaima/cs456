package edu.ohio.heartworks.web.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import edu.ohio.heartworks.domain.fooddiary.FoodTypes.FoodType;
import edu.ohio.heartworks.infrastructure.UniqueObject;

import net.vz.mongodb.jackson.ObjectId;

public class FoodDiaryModel implements UniqueObject {
	
	@ObjectId
	@JsonProperty("_id")
	public String foodDiaryId;
	
	public Date dateCreated;
	public int version;
	public String description;
	public List<String> imageFileNames;
	public FoodType foodType;

	public FoodDiaryModel() {
		this.imageFileNames = new ArrayList<String>();
	}

	public FoodDiaryModel(String foodDiaryId, Date dateCreated) {
		this.foodDiaryId = foodDiaryId;
		this.dateCreated = dateCreated;
		this.imageFileNames = new ArrayList<String>();
		this.version = 0;
	}

	@Override
	@JsonIgnore
	public UUID getId() {
		return UUID.fromString(this.foodDiaryId);
	}
	
	public List<String> getImageFileNames() {
		return imageFileNames;
	}
}