package edu.ohio.heartworks.web.filestore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;

public class LocalFileStore implements FileStore {

	private File root;

	public LocalFileStore(ServletContext context) {
    	root = new File(context.getRealPath("/file"));
	}

	@Override
	public void storeFile(UUID aggregateId, String fileName, InputStream inputFileStream) throws IOException {
        File fileDir = new File(root, aggregateId.toString());
        
        if (!fileDir.exists()) {
        	fileDir.mkdirs();
        }
        
        File file = new File(fileDir, fileName);
        FileOutputStream outputFileStream = null;
        
        try {
        	outputFileStream = new FileOutputStream(file);
	        IOUtils.copy(inputFileStream, outputFileStream);
        } finally {
	        IOUtils.closeQuietly(outputFileStream);
        }
	}

	@Override
	public ByteArrayOutputStream getFile(UUID aggregateId, String fileName) throws IOException {
        File fileDir = new File(root, aggregateId.toString());
        File file = new File(fileDir, fileName);
        
        ByteArrayOutputStream fileOutputStream = new ByteArrayOutputStream();

        FileInputStream fileInputStream = null;
        
        try {
        	fileInputStream = new FileInputStream(file);
            IOUtils.copy(fileInputStream, fileOutputStream);
        } finally {
            IOUtils.closeQuietly(fileInputStream);
        }
        
        return fileOutputStream;
	}

}
