package edu.ohio.heartworks.web.filestore;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.apache.commons.io.output.ByteArrayOutputStream;

import com.mongodb.DB;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

public class GridFSFileStore implements FileStore {
	
	private DB db;

	public GridFSFileStore(DB db) {
		this.db = db;
	}

	@Override
	public void storeFile(UUID aggregateId, String fileName, InputStream fileStream) throws IOException {
		GridFS bucket = new GridFS(db);
		GridFSInputFile gfsFile = bucket.createFile(fileStream);
		
		gfsFile.setFilename(aggregateId.toString() + "_" + fileName);
		gfsFile.save();
	}

	@Override
	public ByteArrayOutputStream getFile(UUID aggregateId, String fileName) throws IOException {
		GridFS gfsBucket = new GridFS(db);
		GridFSDBFile gfsFile = gfsBucket.findOne(aggregateId.toString() + "_" + fileName);
		
        ByteArrayOutputStream fileOutputStream = new ByteArrayOutputStream();
        
        gfsFile.writeTo(fileOutputStream);
        
        return fileOutputStream;
	}
}