package edu.ohio.heartworks.web;


import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.sitebricks.SitebricksModule;

import edu.ohio.heartworks.infrastructure.BusBootstrapper;
import edu.ohio.heartworks.web.pages.Home;
import edu.ohio.heartworks.web.services.Api;

public class AppConfig extends GuiceServletContextListener {

	@Override
	public Injector getInjector() {
	    Injector injector = Guice.createInjector(new SitebricksModule() {
	        @Override
	        protected void configureSitebricks() {
	            scan(Home.class.getPackage());
	            scan(Api.class.getPackage());
	            
	            install(new MainModule());
	        }
	    });
	    
		BusBootstrapper busBootstrapper = injector.getInstance(BusBootstrapper.class);
		DatabaseBootstrapper dbBootstrapper = injector.getInstance(DatabaseBootstrapper.class);
		
		busBootstrapper.Bootstrap();
		dbBootstrapper.Bootstrap();
	    
	    return injector;
	}
}
