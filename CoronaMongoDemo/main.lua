local widget = require("widget")
local json = require("json")

-- set own api key and dbname
local apiKey = "?apiKey=50f86f0be4b09c5e82a8469d"
local dbName = "demo"

local baseUrl = "https://api.mongolab.com/api/1/databases/" .. dbName .. "/"

local t = display.newText("Waiting for event...", 0, 120, 300, 500, "AmericanTypewriter-Bold", 18)

local function networkListener(event)
 
  if (event.isError) then
    t.text = "Network error!"
  else
    myNewData = event.response
    t.text = "From server: "..myNewData

    local decodedData = (json.decode(myNewData))
    -- do something with data
  end
 
end

local function insertDocument(collection, data)
  headers = {}
   
  headers["Content-Type"] = "application/json"
  headers["Accept-Language"] = "en-US"
   
  body = json.encode(data)
   
  local params = {}
  params.headers = headers
  params.body = body
   
  network.request(baseUrl .. "collections/" .. collection .. apiKey, "POST", networkListener,  params)
end

local function getCollection(collection)
  network.request(baseUrl .. "collections/" .. collection .. apiKey, "GET", networkListener, {})
end

local button1Press = function(event)
  insertDocument("test", { key = "key", value = "value" })
end

local button2Press = function(event)
  getCollection("test")
end

local button1 = widget.newButton {
	default = "buttonRed.png",
	over = "buttonRedOver.png",
	onPress = button1Press,
	label = "Insert Document",
	emboss = true
}

button1.x = 160; button1.y = 0

local button2 = widget.newButton{
	default = "buttonRed.png",
	over = "buttonRedOver.png",
	onPress = button2Press,
	label = "Get Documents",
	emboss = true
}

button2.x = 160; button2.y = 60

