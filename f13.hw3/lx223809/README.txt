Name: Lijie Xia
Date: 9/11/2013
HW:   Homework 3
CS 4560

Homework choosed: Android Activity

Code is from http://www.mkyong.com/android/android-activity-from-one-screen-to-another-screen/

Description: 
	This is a simple app including two activities, which are screen 1 and screen 2.
	The first activity has a title and button which link to the second one.
	The second activity is a screen with simple text.
	Users can return to the first activity by press the android back button.

	Activity file is under MyAndroidAppp\src\com\mkyong\android named AppActivity.java and App2Activity.java
	-In AppActivity.java, we declare a Button.
		Then we initial the activity, set the UI in activity 1 and add on click listener event on button.
	-In App2Activity.java, we simply initialize the activity and the UI of it.