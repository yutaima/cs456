Joshua Shelton
js416211

The project source code is an expansion of the "Hello World" project from homework #2.

I chose the android development because it allows me to visually setup a user interface with its
collection of different widgets without having to repeatedly go to the source to change the layout.