package com.helloworld;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity {
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    findViewById(R.id.button).setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        startActivity(new Intent(MainActivity.this, NotMainActivity.class));
      }
    });
  }
}
