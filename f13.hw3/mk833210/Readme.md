I chose to work on vanilla Android/Activities because it's the most basic and
it's the native way to develop on Android. The original code is from running

    android create project --target 1 --name HellowWorld \
      --path HelloWorld --activity MainActivity --package com.helloworld

Then I modified it based on the
[given](http://developer.android.com/training/basics/activity-lifecycle/index.html)
[links](http://developer.android.com/guide/components/activities.html)
so it has two activities.

Android gives each activity its own screen, and moving from one activity to
another pushes the current activity onto a stack. Hitting the back button then
destroys the current activity and retrieves the previous one from the stack.

Intents describe how an app may be activated. This is used to put the app in the
launcher but can also be used to have Android launch an appropriate app to
perform some action.

A fragment is a reusable piece of UI that can be used in multiple activities.
Multiple fragments can be used to build up a GUI, and they can be combined in
different ways, for example if running on a tablet versus a phone.
