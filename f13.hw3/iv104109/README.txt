Title:		Homework 3 - Android Activities
Author:		Isaac N. Van Houten

Class:		CS 4560 -- Fall 2013-2014
Date:		Sept 12, 2013
Instructor: 	Chang Liu

Source:		http://goo.gl/EGNKWy

   In my app, the user is first presented with a screen with two lines of text, and then a button. When
the button is pressed, the activity switches to another screen with some more text and a picture of 
myself.   

   The original source code is from www.mkyong.com, a very helpful website that specializes in android 
and java tutorials. The tutorial in question is very simple, it walks through the process of creating 
a button and two activities, with some explanation about the layout system and how that works. Although
this tutorial was very helpful in getting started, I had to look through the android development links
that Dr. Liu posted on the homework page, in order to learn more about the layout system and creating
new objects, such as an image, in the layouts.