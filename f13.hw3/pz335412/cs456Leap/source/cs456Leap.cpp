// cs456Leap.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "Leap.h"
using namespace Leap;


class SampleListener : public Listener {
  public:
    virtual void onConnect(const Controller&);
    virtual void onFrame(const Controller&);
};

void SampleListener::onConnect(const Controller& controller) {
  std::cout << "Connected" << std::endl;
}

void SampleListener::onFrame(const Controller& controller) {
  const Frame frame = controller.frame();
  if ((frame.hands().count()!=0)) {
    std::cout << frame.hands().count() << " hands  " << frame.fingers().count()<< " fingers" << std::endl;
  }
  
}

int main() {
  //Create a sample listener and controller
  SampleListener listener;
  Controller controller;

  // Have the sample listener receive events from the controller
  controller.addListener(listener);

  // Keep this process running until Enter is pressed
  std::cout << "Press Enter to quit..." << std::endl;
  std::cout << "Now show me your hands and fingers, I will count them!" << std::endl;
  std::cin.get();

  // Remove the sample listener when done
  controller.removeListener(listener);

  return 0;
}


