--
-- Abstract: Storyboard Sample
--
-- Version: 1.0
-- 
-- Sample code is MIT licensed, see http://www.coronalabs.com/links/code/license
-- Copyright (C) 2011 Corona Labs Inc. All Rights Reserved.
--
-- Demonstrates use of the Storyboard API (scene events, transitioning, etc.)
--

-- hide device status bar
display.setStatusBar( display.HiddenStatusBar )

-- require controller module
local storyboard = require "storyboard"
local widget = require "widget"

--variable for which scene is active
local scene1Active = true

-- load first scene
storyboard.gotoScene( "scene1", "fade", 400 )

--
-- Display objects added below will not respond to storyboard transitions
--
local function secondScene( event )
	if scene1Active == false then
		storyboard.gotoScene( "scene1", "fade", 400)
		scene1Active = true
	end
end

local function firstScene( evenet )
	if scene1Active == true then
		storyboard.gotoScene( "scene2", "fade", 400)
		scene1Active = false
	end
end

-- table to setup tabBar buttons
local tabButtons = {
	{ label="Scene 1", up="icon1.png", down="icon1-down.png", width = 32, height = 32, selected=true, onPress = secondScene },
	{ label="Scene 2", up="icon2.png", down="icon2-down.png", width = 32, height = 32, onPress = firstScene},
}

-- create the actual tabBar widget
local tabBar = widget.newTabBar{
	top = display.contentHeight - 50,	-- 50 is default height for tabBar widget
	buttons = tabButtons
}
