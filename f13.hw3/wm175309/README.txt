Weston Martin
9/12/2013
cs4560
Homework 3

I chose to edit an existing application in corona SDK. The application can be found in Corona SDK sample programs under /SampleCode/Interface/Storyboard. The code creates and app that has 4 scenes and 2 useless widget buttons at the bottom of the screen. The app allows you to switch the scene by tapping on the scene. I altered the app to only have 2 scenes that you can switch back and forth using the two buttons (that previously had no function) at the bottom of the screen rather than tapping the scene.