Homework 3
CS 4560 Fall 2013

All code was taken from developer.android.com and was then modified by me, Abdulrahman Kurdi.
I developed a very simple "Course Finder" application. In the first activity (the main acivity),
the user is prompted to enter a course number. After the user enters the course number and clicks
"Accept", he is sent to the 2nd activity. The second activity shows details of the course
such as the course name and its current meeting times. For the sake of simplicity, I only show 
one course information(this course), and it was all hard coded in. I chose this subject because 
it's very relevant to us, since we are all students and teachers. 

-Abdulrahman Kurdi
-ak109609
