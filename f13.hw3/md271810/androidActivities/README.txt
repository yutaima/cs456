Author: Michael Dixon
Date: 9/10/2013
Used beginner's tutorial at developer.android.com/basics/firstapp

**** REASONS FOR CHOOSING ANDROID ****
I chose to use android because I'm interested in learning how to develop for the phone. Furthermore, I am interested
in learning a very popular language that a lot of people use.


**** METHOD ****
This is a very simple program that contains two activities. The first activity has a textbox and a button, and the second
activity shows whatever the user typed into the textbox after clicking the button from the first activity. To do this, I
first created the views for the activities using some xml. This is what is displayed on the screen. The views are contained
in the res/layout folder. activity_display_message.xml is for the activity that displays the text, and main.xml is for the
first view that takes in the input.

The controllers for the view are located in the src folder. To move to the next activity, I added an onClick attribute to
the send button in the first view. This attribute called a sendMessage function that is located in the corresponding controller
for the activity. The sendMessage function creates what is called an "intent", and then starts the next activity with the intent.
The intent basically says "hey, I am going to start another activity. I *intend* on starting another activity." The intent
contains the view that calls the sendMessage function and the activity that it intends on calling. The intent also gets the
text that you typed in the first activity added to it, so the next activity has access to the text. Once the intent is created,
you call "startActivity(intent);" to start the activity you specified in the intent. The next activity's view is mostly blank;
the text gets displayed via the controller. So, the controller extracts the text from the intent, and then puts the text into
the view using the setContentView function.