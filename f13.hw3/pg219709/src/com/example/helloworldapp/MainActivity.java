package com.example.helloworldapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	private Intent emailIntent;
	private String feedback;
	private EditText feedbackBox;
	
	public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button next = (Button) findViewById(R.id.next_activity);
		next.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View v){
				Intent myIntent = new Intent(v.getContext(), NewActivity.class);
				v.getContext().startActivity(myIntent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	

	/*Responds to send button click*/
	public void sendMessage(View view){
		emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.setType("plain/text");
		
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"pg219709@ohio.edu"});
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Testing");
		
		EditText editText = (EditText) findViewById(R.id.edit_message);
		feedback = editText.getText().toString();
		
		
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, feedback);
		
		startActivity(Intent.createChooser(emailIntent, "Test Title"));
		
		/*
		Intent intent = new Intent(this, DisplayMessageActivity.class);
		EditText editText = (EditText) findViewById(R.id.edit_message);
		String message = editText.getText().toString();
		intent.putExtra(EXTRA_MESSAGE, message);
		startActivity(intent);*/
	}

}
