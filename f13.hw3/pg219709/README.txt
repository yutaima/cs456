I decided to work on developing two activities for my Android application.
I want to become more accustomed to java because I am certain I will focus on
a java project this year. As you can see from the pictures, I have an 
activity that has an email button and another button that leads to a second 
activity, which simply displays a message, demonstrating that the main activity
was simply paused in its current state. I left inline comments to show you the
vital parts of my code. 



RESOURCES:
  This is the tutorial I watched to create the second activity apart from the main activity
      https://www.youtube.com/watch?v=0wz0bM-xy3M

  This is the form I used to create an email link on my main activity page
      http://stackoverflow.com/questions/8994488/android-button-onclick-submit-to-email
