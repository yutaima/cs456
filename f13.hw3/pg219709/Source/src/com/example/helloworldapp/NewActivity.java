package com.example.helloworldapp;

import android.app.Activity;
import android.os.Bundle;

public class NewActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_activity);
		/*The above statement places the new_activity
		 *screen into the screen's view hierarchy.
		 *The parameter is our activity_main.xml
		 *file which is the content we wish to
		 *display*/
	}

}
