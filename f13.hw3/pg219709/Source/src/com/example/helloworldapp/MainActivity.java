package com.example.helloworldapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	private Intent emailIntent;
	private String feedback;
	private EditText feedbackBox;
	
	public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button next = (Button) findViewById(R.id.next_activity);  /*We call the Activity button by its id*/
		next.setOnClickListener(new OnClickListener(){ /*And capture its onclick event, which means when the button is pressed by the user*/
			
			@Override
			public void onClick(View v){ /*We trigger this newly created onClick function*/
				Intent myIntent = new Intent(v.getContext(), NewActivity.class); /*This function creates a new intent to display another page*/
				v.getContext().startActivity(myIntent); /*And then starts that new activity, meaning the user will now see the next_activity page*/
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	

	/*Responds to send button click*/
	public void sendMessage(View view){
		emailIntent = new Intent(android.content.Intent.ACTION_SEND); /*We create a new intent to send an email*/
		emailIntent.setType("plain/text"); /*Here we set the style of the email*/
		
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"pg219709@ohio.edu"}); /*We declare the address we would like to send the email to*/
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Testing"); /*And give the message a subject*/
		
		EditText editText = (EditText) findViewById(R.id.edit_message); /*The body of the message comes from an EditText on the main activity page.
		                                                                  We retrieve it by the elements id.*/
		feedback = editText.getText().toString(); /*Then we set our feedback variable to the text that was typed in the EditText element.*/
		
		
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, feedback); /*And send that text as the body of the email.*/
		
		startActivity(Intent.createChooser(emailIntent, "Test Title")); 
		
		/*
		Intent intent = new Intent(this, DisplayMessageActivity.class);
		EditText editText = (EditText) findViewById(R.id.edit_message);
		String message = editText.getText().toString();
		intent.putExtra(EXTRA_MESSAGE, message);
		startActivity(intent);*/
	}

}
