For homework 3 I chose the android option with Activities. 
I chose to use source code to create an app with multiple activities.

Basic output represents the first activity. User can read the output and choose
to press the button to see what else appears.

After the button is pressed, a final output text appears and the user can exit the app.

Below is a link to the sourcecode package and the website that had a tutorial:
http://www.mkyong.com/android/android-activity-from-one-screen-to-another-screen/ 
