package com.example.helloworld;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NewActivity extends Activity {
	
	Button button;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setContentView(R.layout.activity_main2);
	}
	
	public void onActivityBtnClick(View view){
		Intent myIntent = new Intent(view.getContext(), MainActivity.class);
		startActivity(myIntent);
		finish();
	
	}

}
