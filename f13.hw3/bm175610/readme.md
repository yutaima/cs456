Brian Mcclaugherty

Assignment 3

Source code origination: http://developer.android.com/shareables/training/ActivityLifecycle.zip

I chose to look into android activities. Activities navigate through different 'states' of an application.
When another activity is created or resumed, the current activity is saved and moved into a background state. 
Once the user quits the new activity, the old activity is restored from background and resumed. Activities can be
quit using the back button, or by using a physical button.

In my screen shots, I launch the application which starts activity A. Next I start acitvity B, which pauses A. I then pause
activity B to return to Activity A. 