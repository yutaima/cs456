Bryan Schneider
bs149808

The assignment I chose to do is the Android Activities App.

Because I just had to be different, I decided to to follow the instructions at:

http://docs.xamarin.com/guides/android/getting_started/hello%2C_multi-screen_applications

Mainly because I wanted to avoid having to use Java. A button is created in the Main activity,
and a hello world message is displayed. If this button is pressed, the app transitions to the
Second Activity, where a button is created that goes back to the Main Activity and a hello world
message is displayed. I'm only making use of onCreate right now. 