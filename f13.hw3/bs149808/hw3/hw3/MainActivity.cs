using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace hw3
{
	[Activity (Label = "MainActivity", MainLauncher = true)]
	public class MainActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			var showSecond = FindViewById<Button> (Resource.Id.showSecond);
			showSecond.Click += (sender, e) => {
				StartActivity (typeof(SecondActivity));
			};
		}
	}
}


