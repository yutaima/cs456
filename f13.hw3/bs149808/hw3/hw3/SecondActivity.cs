using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace hw3
{
	[Activity (Label = "SecondActivity")]			
	public class SecondActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "second" layout resource
			SetContentView (Resource.Layout.Second);

			// Get our button from the layout resource,
			// and attach an event to it
			var showMain = FindViewById<Button> (Resource.Id.showMain);
			showMain.Click += (sender, e) => {
				StartActivity (typeof(MainActivity));
			};
		}
	}
}

