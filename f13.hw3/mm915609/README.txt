Name: Matthew Mensch
Oak ID: mm915609@ohio.edu
Date: 9/12/2013
Class: CS 4560

For homework assignment 3, I chose to do the Android activites. To complete this assignment I used the
tutorial on Android.development's website which can be found at:
http://developer.android.com/training/basics/firstapp/building-ui.html.

Using this tutorial, an application was created that will allow the user to input text and click the
"send" button. Once this button is pressed, activity 2 is done which changes to a screen with the text
that was previously typed in. I used the virtual device on Eclipse to do this.