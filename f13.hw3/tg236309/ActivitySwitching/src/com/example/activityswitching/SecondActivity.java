package com.example.activityswitching;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
        TextView v = (TextView)findViewById(R.id.Text2);
        v.setText(R.string.act2);
	}
    public void Switch_to_1(View view){
    	Intent myIntent=new Intent(this,FirstActivity.class);
    	startActivity(myIntent);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.second, menu);
		return true;
	}

}
