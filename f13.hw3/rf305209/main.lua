-- 
-- Abstract: List View sample app
--  
-- Version: 2.0
-- 
-- Sample code is MIT licensed, see http://www.coronalabs.com/links/code/license
-- Copyright (C) 2010 Corona Labs Inc. All Rights Reserved.
--
-- Demonstrates how to create a list view using the widget's Table View Library.
-- A list view is a collection of content organized in rows that the user
-- can scroll up or down on touch. Tapping on each row can execute a 
-- custom function.

display.setStatusBar( display.HiddenStatusBar ) 

--import the widget library
local widget = require("widget")
widget.setTheme( "theme_ios" )

--Set the background to white
display.setDefault( "background", 255, 255, 255 )

--Create a group to hold our widgets & images
local widgetGroup = display.newGroup()

-- create a gradient for the top-half of the toolbar
local toolbarGradient = graphics.newGradient( {168, 181, 198, 255 }, {139, 157, 180, 255}, "down" )

-- create toolbar to go at the top of the screen
local titleBar = widget.newTabBar{
	gradient = toolbarGradient,
	bottomFill = { 117, 139, 168, 255 },
	height = 44
}
titleBar.y = display.screenOriginY + titleBar.contentHeight * 0.5

-- create embossed text to go on toolbar
local titleText = display.newEmbossedText( "Jokes", 0, 0, native.systemFontBold, 20 )
titleText:setReferencePoint( display.CenterReferencePoint )
titleText:setTextColor( 255 )
titleText.x = 160
titleText.y = titleBar.y

-- create a shadow underneath the titlebar (for a nice touch)
local shadow = display.newImage( "shadow.png" )
shadow:setReferencePoint( display.TopLeftReferencePoint )
shadow.x, shadow.y = 0, titleBar.y + titleBar.contentHeight * 0.5
shadow.xScale = 320 / shadow.contentWidth
shadow.alpha = 0.45

--Text to show which item we selected
local itemSelected = display.newText( "You selected item ", 0, 0, 205, 120, native.systemFontBold, 28 )
itemSelected:setTextColor( 0 )
itemSelected.x = display.contentWidth + itemSelected.contentWidth * 0.5
itemSelected.y = display.contentCenterY
widgetGroup:insert( itemSelected )

--Forward reference for our back button
local backButton

--Create Table view
local list = widget.newTableView{
	width = 320, 
	height = 448,
	bottomPadding = 8,
	maskFile = "mask-320x448.png"
}
list.y = titleBar.y + titleBar.contentHeight * 0.5


--Insert widgets/images into a group
widgetGroup:insert( list )
widgetGroup:insert( titleBar )
widgetGroup:insert( titleText )
widgetGroup:insert( shadow )


--Handle row rendering
local function onRowRender( event )
	local row = event.row
	local rowGroup = event.view
	local label = "Why did the chicken cross the road?"
	local color = 0
	
	--Create the row's text
	row.textObj = display.newRetinaText( rowGroup, label, 0, 0, 220, 70, native.systemFontBold, 16 )
	row.textObj:setTextColor( color )
	row.textObj:setReferencePoint( display.CenterLeftReferencePoint )
	row.textObj.x, row.textObj.y = 20, rowGroup.contentHeight * 0.5
	rowGroup:insert( row.textObj )
	
	--Create the row's arrow
	row.arrow = display.newImage( "rowArrow.png", false )
	row.arrow.x = rowGroup.contentWidth - row.arrow.contentWidth * 2
	row.arrow.y = rowGroup.contentHeight * 0.5
	rowGroup:insert( row.arrow )
end

--Handle the back button release event
local function onBackRelease()
	--Transition in the list, transition out the item selected text and the back button
	transition.to( list, { x = 0, time = 400, transition = easing.outExpo } )
	transition.to( itemSelected, { x = display.contentWidth + itemSelected.contentWidth * 0.5, time = 400, transition = easing.outExpo } )
	transition.to( backButton, { x = 60, alpha = 0, time = 400, transition = easing.outQuad } )
end

--Create the back button
backButton = widget.newButton{
	style = "backSmall",
	label = "Back", 
	yOffset = - 3,
	onRelease = onBackRelease
}
backButton.alpha = 0
backButton.x = 60
backButton.y = titleBar.y
widgetGroup:insert( backButton )


--Hande row touch events
local function onRowTouch( event )
	local row = event.row
	local background = event.background
	
	if event.phase == "press" then
		print( "Pressed row: " .. row.index )
		background:setFillColor( 0, 110, 233, 255 )

	elseif event.phase == "release" or event.phase == "tap" then
		--Update the item selected text
		
		itemSelected.text = "To get to the other side "
		--Transition out the list, transition in the item selected text and the back button
		transition.to( list, { x = - list.contentWidth, time = 400, transition = easing.outExpo } )
		transition.to( itemSelected, { x = display.contentCenterX, time = 400, transition = easing.outExpo } )
		transition.to( backButton, { x = 40, alpha = 1, time = 400, transition = easing.outQuad } )
		
		print( "Tapped and/or Released row: " .. row.index )
		background:setFillColor( 0, 110, 233, 255 )
		row.reRender = true
	end
end


-- insert rows into list (tableView widget)

	list:insertRow{
		height = 172,
		onRender = onRowRender,
		listener = onRowTouch
	}

