package cs4560.activitydemo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;

public class AnotherActivity extends Activity {

	Button button;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.another_activity);
	}

}
