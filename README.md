This is the class repository for CS4560 and CS4561.

LinkedIn Group: http://www.linkedin.com/groups?gid=6535339

Public LinkedIn Software Engineering Tools Group: https://www.linkedin.com/groups/Software-Engineering-SaaS-Tools-6534987

Always provide a detailed log message when committing to this repository.

Check your .gitignore file. Always do "git status" before "git commit -m ''". Do not check in .class, .o, .DS_Store files.

Do not check in files in the root folder.